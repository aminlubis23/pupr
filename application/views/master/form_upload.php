<html>
<head>
	<title>Form Import</title>

	<!-- Load File jquery.min.js yang ada difolder js -->
	<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

	<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
	</script>
</head>
<body>
	<h3>Form Import</h3>
	<hr>

	<a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
	<br>
	<br>

	<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
	<form method="post" action="<?php echo base_url("admin/Uploadscnbkn/form"); ?>" enctype="multipart/form-data">
		<!--
		-- Buat sebuah input type file
		-- class pull-left berfungsi agar file input berada di sebelah kiri
		-->
		<input type="file" name="file">

		<!--
		-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
		-->
		<input type="submit" name="preview" value="Preview">
	</form>

	<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("admin/Uploadscnbkn/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "<table border='1' cellpadding='8'>
		<tr>
			<th colspan='5'>Preview Data</th>
		</tr>
		<tr>
		<th>ID</th>
			<th>NIK</th>
			<th>No KK</th>
			<th>NIK KK</th>
			<th>Password</th>
			<th>Nama</th>
			<th>Nama Ijazah</th>
			<th>Tempat Lahir</th>
			<th>Tempat Lahir Ijazah</th>
			<th>Tanggal Lahir</th>
			<th>Tanggal Lahir Ijazah</th>
			<th>Jenis Kelamin</th>
			<th>Email</th>
			<th>Pertanyaan 1</th>
			<th>Jawaban 1</th>
			<th>Pertanyaan 2</th>
			<th>Jawaban 2</th>
			<th>Agama</th>
			<th>Alamat KTP</th>
			<th>Alamat Domisili</th>
			<th>Tinggi Badan</th>
			<th>Status Kawin</th>
			<th>No Telp</th>
			<th>No HP</th>
			<th>Nama Ibu</th>
			<th>Nama Ayah</th>
			<th>Kodepos</th>
			<th>Lokasi Kabkota</th>
			<th>Tanggal Daftar</th>
			<th>Remarks</th>
		</tr>";

		$numrow = 1;
		$kosong = 0;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($sheet as $row){
			// Ambil data pada excel sesuai Kolom
			$id = $row['A']; // Ambil data NIS
			$nik = $row['B']; // Ambil data nama
			$no_kk = $row['C']; // Ambil data jenis kelamin
			$nik_kk = $row['D']; // Ambil data alamat
			$password = $row['E']; // Ambil data alamat
			$nama = $row['F']; // Ambil data alamat
			$nama_ijazah = $row['G']; // Ambil data alamat
			$tempat_lahir = $row['H']; // Ambil data alamat
			$tempat_lahir_ijazah = $row['I']; // Ambil data alamat
			$tanggal_lahir = $row['J']; // Ambil data alamat
			$tanggal_lahir_ijazah = $row['K']; // Ambil data alamat
			$jenis_kelamin = $row['L']; // Ambil data alamat
			$email = $row['M']; // Ambil data alamat
			$pertanyaan_pengaman_1 = $row['N']; // Ambil data alamat
			$jawaban_1 = $row['O']; // Ambil data alamat
			$pertanyaan_pengaman_2 = $row['P']; // Ambil data alamat
			$jawaban_2 = $row['Q']; // Ambil data alamat
			$agama = $row['R']; // Ambil data alamat
			$alamat_ktp = $row['S']; // Ambil data alamat
			$alamat_domisili = $row['T']; // Ambil data alamat
			$tinggi_badan = $row['U']; // Ambil data alamat
			$status_kawin = $row['V']; // Ambil data alamat
			$no_telp = $row['W']; // Ambil data alamat
			$no_hp = $row['X']; // Ambil data alamat
			$nama_ibu = $row['Y']; // Ambil data alamat
			$nama_ayah = $row['Z']; // Ambil data alamat
			$kodepos = $row['AA']; // Ambil data alamat
			$lokasi_kabkota = $row['AB']; // Ambil data alamat
			$tgl_daftar = $row['AC']; // Ambil data alamat
			$remarks = $row['AD']; // Ambil data alamat

			// Cek jika semua data tidak diisi
			if (empty($id) && empty($nik) && empty($no_kk) && empty($nik_kk) && empty($password) && empty($nama) && empty($nama_ijazah) && empty($tempat_lahir) && empty($tempat_lahir_ijazah) && empty($tanggal_lahir) && empty($tanggal_lahir_ijazah) && empty($jenis_kelamin) && empty($email) && empty($pertanyaan_pengaman_1) && empty($jawaban_1) && empty($pertanyaan_pengaman_2) && empty($jawaban_2) && empty($agama) && empty($alamat_ktp) && empty($alamat_domisili) && empty($tinggi_badan) && empty($status_kawin) && empty($no_telp) && empty($no_hp) && empty($nama_ibu) && empty($nama_ayah) && empty($kodepos) && empty($lokasi_kabkota) && empty($tgl_daftar) && empty($remarks))
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$id_td = ( ! empty($id))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$nik_td = ( ! empty($nik))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
				$no_kk_td = ( ! empty($no_kk))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
				$nik_kk_td = ( ! empty($nik_kk))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$password_td = ( ! empty($password))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$nama_ijazah_td = ( ! empty($nama_ijazah))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tempat_lahir_td = ( ! empty($tempat_lahir))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tempat_lahir_ijazah_td = ( ! empty($tempat_lahir_ijazah))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tanggal_lahir_td = ( ! empty($tanggal_lahir))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tanggal_lahir_ijazah_td = ( ! empty($tanggal_lahir_ijazah))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$jenis_kelamin_td = ( ! empty($jenis_kelamin))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$email_td = ( ! empty($email))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$pertanyaan_pengaman_1_td = ( ! empty($pertanyaan_pengaman_1))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$jawaban_1_td = ( ! empty($jawaban_1))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$pertanyaan_pengaman_2_td = ( ! empty($pertanyaan_pengaman_2))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$jawaban_2_td = ( ! empty($jawaban_2))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$agama_td = ( ! empty($agama))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$alamat_ktp_td = ( ! empty($alamat_ktp))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$alamat_domisili_td = ( ! empty($alamat_domisili))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tinggi_badan_td = ( ! empty($tinggi_badan))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$status_kawin_td = ( ! empty($status_kawin))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$no_telp_td = ( ! empty($no_telp))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$no_hp_td = ( ! empty($no_hp))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$nama_ibu_td = ( ! empty($nama_ibu))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$nama_ayah_td = ( ! empty($nama_ayah))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$kodepos_td = ( ! empty($kodepos))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$lokasi_kabkota_td = ( ! empty($lokasi_kabkota))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tgl_daftar_td = ( ! empty($tgl_daftar))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$remarks_td = ( ! empty($remarks))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah

				// Jika salah satu data ada yang kosong
				if (empty($id) && empty($nik) && empty($no_kk) && empty($nik_kk) && empty($password) && empty($nama) && empty($nama_ijazah) && empty($tempat_lahir) && empty($tempat_lahir_ijazah) && empty($tanggal_lahir) && empty($tanggal_lahir_ijazah) && empty($jenis_kelamin) && empty($email) && empty($pertanyaan_pengaman_1) && empty($jawaban_1) && empty($pertanyaan_pengaman_2) && empty($agama) && empty($alamat_ktp) && empty($alamat_domisili) && empty($tinggi_badan) && empty($status_kawin) && empty($no_telp) && empty($no_hp) && empty($nama_ibu) && empty($nama_ayah) && empty($kodepos) && empty($lokasi_kabkota) && empty($tgl_daftar) && empty($remarks)){
					$kosong++; // Tambah 1 variabel $kosong
				}

				echo "<tr>";
				echo "<td".$id_td.">".$id."</td>";
				echo "<td".$nik_td.">".$nik."</td>";
				echo "<td".$no_kk_td.">".$no_kk."</td>";
				echo "<td".$password_td.">".$password."</td>";
				echo "<td".$nama_td.">".$nama."</td>";
				echo "<td".$nama_ijazah_td.">".$nama_ijazah."</td>";
				echo "<td".$tempat_lahir_td.">".$tempat_lahir."</td>";
				echo "<td".$tempat_lahir_ijazah_td.">".$tempat_lahir_ijazah."</td>";
				echo "<td".$tanggal_lahir_td.">".$tanggal_lahir."</td>";
				echo "<td".$tanggal_lahir_ijazah_td.">".$tanggal_lahir_ijazah."</td>";
				echo "<td".$jenis_kelamin_td.">".$jenis_kelamin."</td>";
				echo "<td".$email_td.">".$email."</td>";
				echo "<td".$pertanyaan_pengaman_1_td.">".$pertanyaan_pengaman_1."</td>";
				echo "<td".$jawaban_1_td.">".$jawaban_1."</td>";
				echo "<td".$pertanyaan_pengaman_2_td.">".$pertanyaan_pengaman_2."</td>";
				echo "<td".$jawaban_2_td.">".$jawaban_2."</td>";
				echo "<td".$agama_td.">".$agama."</td>";
				echo "<td".$alamat_ktp_td.">".$alamat_ktp."</td>";
				echo "<td".$alamat_domisili_td.">".$alamat_domisili."</td>";
				echo "<td".$tinggi_badan_td.">".$tinggi_badan."</td>";
				echo "<td".$status_kawin_td.">".$status_kawin."</td>";
				echo "<td".$no_telp_td.">".$no_telp."</td>";
				echo "<td".$no_hp_td.">".$no_hp."</td>";
				echo "<td".$nama_ibu_td.">".$nama_ibu."</td>";
				echo "<td".$nama_ayah_td.">".$nama_ayah."</td>";
				echo "<td".$kodepos_td.">".$kodepos."</td>";
				echo "<td".$lokasi_kabkota_td.">".$lokasi_kabkota."</td>";
				echo "<td".$tgl_daftar_td.">".$tgl_daftar."</td>";
				echo "<td".$remarks_td.">".$remarks."</td>";
				//echo "<td".$fil30_td.">".$fil30."</td>";
				echo "</tr>";
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		echo "</table>";

		// Cek apakah variabel kosong lebih dari 1
		// Jika lebih dari 1, berarti ada data yang masih kosong
		if($kosong > 1){
		?>
			<script>
			$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

				$("#kosong").show(); // Munculkan alert validasi kosong
			});
			</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' name='import'>Import</button>";
			echo "<a href='".base_url("admin/Uploadscnbkn")."'>Cancel</a>";
		}

		echo "</form>";
	}
	?>
</body>
</html>
