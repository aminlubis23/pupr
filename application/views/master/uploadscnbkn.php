<?php
$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');
?>

<?php
$site	= $this->konfigurasi_model->listing();
$uploadscnbkn = $this->uploadscnbkn_model->listing();
// Notifikasi
if($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

// Error
if(isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

// Validasi
echo validation_errors('<div class="alert alert-success">','</div>');

?>

<!--main-->
<br />
<div role="main" class="main">
    <div class="container">
        <h3>Upload File</h3>
        <div class="row" style="background-color: white">
            <!--<form method="post" action="<?php echo base_url("index.php/Siswa/form"); ?>" enctype="multipart/form-data">-->
            <form method="post" action="<?php echo base_url("index.php/upload_bkn/form"); ?>" enctype="multipart/form-data">
            <div class="col-md-3">
                <br />
                <label>&nbsp;Input File:</label>
                <input id="file" name="file" type="file" /><br />
            </div>
            <div class="col-md-3">
                <div class="pull-Left">
                    <br />
                    <button type="submit" name="preview" value="Preview" class="mb-xs mt-xs mr-xs btn btn-primary" style="width: 200px"><i class="fa fa-upload"></i>&nbsp;Upload File</button>
                </div>
            </div>
            <div class="col-md-6"></div>
            </form>
        </div>
        <br />
        <br />

        <div style="width: 1150px; background-color: white; border: 1px">
            <br />
            <div class="col-md-12" style="background-color: white; overflow-x: auto; overflow-y: auto">
                <table id="tableview" class="table table-striped table-bordered" style="width: 5000px; background-color: white">
                    <thead>
                    <tr>
                        <th style="color: #242424; width:100px; text-align:center">No</th>
                        <th style="color: #242424; width:100px; text-align:center">ID</th>
                        <th style="color: #242424; width:100px; text-align:center">NIK</th>
                        <th style="color: #242424; width:100px; text-align:center">No KK</th>
                        <th style="color: #242424; width:100px; text-align:center">NIK KK</th>
                        <th style="color: #242424; width:100px; text-align:center">Password</th>
                        <th style="color: #242424; width:100px; text-align:center">Nama</th>
                        <th style="color: #242424; width:100px; text-align:center">Nama Ijazah</th>
                        <th style="color: #242424; width:100px; text-align:center">Tempat Lahir</th>
                        <th style="color: #242424; width:100px; text-align:center">Tempat Lahir Ijazah</th>
                        <th style="color: #242424; width:100px; text-align:center">Tanggal Lahir</th>
                        <th style="color: #242424; width:100px; text-align:center">Tanggal Lahir Ijazah</th>
                        <th style="color: #242424; width:100px; text-align:center">Jenis Kelamin</th>
                        <th style="color: #242424; width:100px; text-align:center">Email</th>
                        <th style="color: #242424; width:100px; text-align:center">Pertanyaan 1</th>
                        <th style="color: #242424; width:100px; text-align:center">Jawaban 1</th>
                        <th style="color: #242424; width:100px; text-align:center">Pertanyaan 2</th>
                        <th style="color: #242424; width:100px; text-align:center">Jawaban 2</th>
                        <th style="color: #242424; width:100px; text-align:center">Agama</th>
                        <th style="color: #242424; width:100px; text-align:center">Alamat KTP</th>
                        <th style="color: #242424; width:100px; text-align:center">Alamat Domisili</th>
                        <th style="color: #242424; width:100px; text-align:center">Tinggi Badan</th>
                        <th style="color: #242424; width:100px; text-align:center">Status Kawin</th>
                        <th style="color: #242424; width:100px; text-align:center">No Telp</th>
                        <th style="color: #242424; width:100px; text-align:center">No HP</th>
                        <th style="color: #242424; width:100px; text-align:center">Nama Ibu</th>
                        <th style="color: #242424; width:100px; text-align:center">Nama Ayah</th>
                        <th style="color: #242424; width:100px; text-align:center">Kodepos</th>
                        <th style="color: #242424; width:100px; text-align:center">Lokasi Kabkota</th>
                        <th style="color: #242424; width:100px; text-align:center">Tanggal Daftar</th>
                        <th style="color: #242424; width:100px; text-align:center">Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; foreach($uploadscnbkn as $uploadscnbkn) { ?>
                        <tr>
                            <td>
                                <label style="color: #242424" id="lblno"><?php echo $uploadscnbkn->id_exportdatascnbkn ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblid"><?php echo $uploadscnbkn->id ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnik"><?php echo $uploadscnbkn->nik ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnokk"><?php echo $uploadscnbkn->no_kk ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnikkk"><?php echo $uploadscnbkn->nik_kk ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpassword"><?php echo $uploadscnbkn->password ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnama"><?php echo $uploadscnbkn->nama ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnamaijazah"><?php echo $uploadscnbkn->nama_ijazah ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbltempatlahir"><?php echo $uploadscnbkn->tempat_lahir ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbltempatlahirijazah"><?php echo $uploadscnbkn->tempat_lahir_ijazah ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbltgllahir"><?php echo $uploadscnbkn->tanggal_lahir ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbltgllahirijazah"><?php echo $uploadscnbkn->tanggal_lahir_ijazah ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbljengkel"><?php echo $uploadscnbkn->jenis_kelamin ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblemail"><?php echo $uploadscnbkn->email ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpertanyaan1"><?php echo $uploadscnbkn->pertanyaan_pengaman_1 ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbljawaban1"><?php echo $uploadscnbkn->jawaban_1 ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpertanyaan2"><?php echo $uploadscnbkn->pertanyaan_pengaman_2 ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbljawaban2"><?php echo $uploadscnbkn->jawaban_2 ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblagama"><?php echo $uploadscnbkn->agama ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblalamatktp"><?php echo $uploadscnbkn->alamat_ktp ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblalamatdomisili"><?php echo $uploadscnbkn->alamat_domisili ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbltinggibadan"><?php echo $uploadscnbkn->tinggi_badan ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblstatuskawin"><?php echo $uploadscnbkn->status_kawin ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnotelp"><?php echo $uploadscnbkn->no_telp ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnohp"><?php echo $uploadscnbkn->no_hp ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnamaibu"><?php echo $uploadscnbkn->nama_ibu ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblnamaayah"><?php echo $uploadscnbkn->nama_ayah ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblkodepos"><?php echo $uploadscnbkn->kodepos ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbllokasikabkota"><?php echo $uploadscnbkn->lokasi_kabkota ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbltanggaldaftar"><?php echo $uploadscnbkn->tgl_daftar ?></label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblremarks"><?php echo $uploadscnbkn->remarks ?></label>
                            </td>
                        </tr>
                        <?php $i++; } ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>

        </div>
    </div>

</footer>
</div>
<!-- End OF Main -->
            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

            <!-- datatable-->
            <!--<script src="Datatable/jquery-3.3.1.js"></script>-->
            <!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
            <script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#tableview').DataTable({
                        "scrollX": true
                    });
                });
            </script>
<?php

$variable_inject_akses_level = 'member';

if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
    if(isset($upload_error)){ // Jika proses upload gagal
        echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
        die; // stop skrip
    }

    // Buat sebuah tag form untuk proses import data ke database
    echo "<form method='post' action='".base_url("index.php/upload_bkn/import")."'>";

    // Buat sebuah div untuk alert validasi kosong
    echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

    echo "<table border='1' cellpadding='8'>
		<tr>
			<th colspan='5'>Preview Data</th>
		</tr>
		<tr>
			<th style=\"color: #242424; width:100px; text-align:center\">No</th>
            <th style=\"color: #242424; width:100px; text-align:center\">ID</th>
            <th style=\"color: #242424; width:100px; text-align:center\">NIK</th>
            <th style=\"color: #242424; width:100px; text-align:center\">No KK</th>
            <th style=\"color: #242424; width:100px; text-align:center\">NIK KK</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Password</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Nama</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Nama Ijazah</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Tempat Lahir</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Tempat Lahir Ijazah</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Tanggal Lahir</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Tanggal Lahir Ijazah</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Jenis Kelamin</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Email</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Pertanyaan 1</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Jawaban 1</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Pertanyaan 2</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Jawaban 2</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Agama</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Alamat KTP</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Alamat Domisili</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Tinggi Badan</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Status Kawin</th>
            <th style=\"color: #242424; width:100px; text-align:center\">No Telp</th>
            <th style=\"color: #242424; width:100px; text-align:center\">No HP</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Nama Ibu</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Nama Ayah</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Kodepos</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Lokasi Kabkota</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Tanggal Daftar</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Akses Level</th>
            <th style=\"color: #242424; width:100px; text-align:center\">Remarks</th>
		</tr>";

    $numrow = 1;
    $kosong = 0;

    // Lakukan perulangan dari data yang ada di excel
    // $sheet adalah variabel yang dikirim dari controller
    foreach($sheet as $row){
        // Ambil data pada excel sesuai Kolom
        $id_exportdatascnbkn = $row['A']; // Ambil data no
        $id = $row['B']; // Ambil data id
        $nik = $row['C']; // Ambil data nik
        $no_kk = $row['D']; // Ambil data no_kk
        $nik_kk = $row['E']; // Ambil data nik_kk
        $password = $row['F']; // Ambil data password
        $nama = $row['G']; // Ambil data nama
        $nama_ijazah = $row['H']; // Ambil data nama_ijazah
        $tempat_lahir = $row['I']; // Ambil data tempat_lahir
        $tempat_lahir_ijazah = $row['J']; // Ambil data tempat_lahir_ijazah
        $tanggal_lahir = $row['K']; // Ambil data tanggal_lahir
        $tanggal_lahir_ijazah = $row['L']; // Ambil data tanggal_lahir_ijazah
        $jenis_kelamin = $row['M']; // Ambil data jenis_kelamin
        $email = $row['N']; // Ambil data email
        $pertanyaan_pengaman_1 = $row['O']; // Ambil data pertanyaan_pengaman_1
        $jawaban_1 = $row['P']; // Ambil data jawaban_1
        $pertanyaan_pengaman_2 = $row['Q']; // Ambil data pertanyaan_pengaman_2
        $jawaban_2 = $row['R']; // Ambil data jawaban_2
        $agama = $row['S']; // Ambil data agama
        $alamat_ktp = $row['T']; // Ambil data alamat_ktp
        $alamat_domisili = $row['U']; // Ambil data alamat_domisili
        $tinggi_badan = $row['V']; // Ambil data tinggi_badan
        $status_kawin = $row['W']; // Ambil data status_kawin
        $no_telp = $row['X']; // Ambil data no_telp
        $no_hp = $row['Y']; // Ambil data no_tehp
        $nama_ibu = $row['Z']; // Ambil data nama_ibu
        $nama_ayah = $row['AA']; // Ambil data nama_ayah
        $kodepos = $row['AB']; // Ambil data kodepos
        $lokasi_kabkota = $row['AC']; // Ambil data lokasi_kabkota
        $tgl_daftar = $row['AD']; // Ambil data tgl_daftar
        //$akses_level = $variable_inject_akses_level; // ambil data dari injector temporary
        $remarks = $row['AE']; // Ambil data remarks

        // Cek jika semua data tidak diisi
        if(empty($id_exportdatascnbkn) && empty($id) && empty($nik) && empty($no_kk) && empty($nik_kk) && empty($password) && empty($nama) && empty($nama_ijazah) && empty($tempat_lahir) && empty($tempat_lahir_ijazah) && empty($tanggal_lahir) && empty($tanggal_lahir_ijazah) && empty($jenis_kelamin) && empty($email) && empty($pertanyaan_pengaman_1) && empty($jawaban_1) && empty($pertanyaan_pengaman_2) && empty($jawaban_2) && empty($agama) && empty($alamat_ktp) && empty($alamat_domisili) && empty($tinggi_badan) && empty($status_kawin) && empty($no_telp) && empty($no_hp) && empty($nama_ibu) && empty($nama_ayah) && empty($kodepos) && empty($lokasi_kabkota) && empty($tgl_daftar) && empty($remarks))
        //if(empty($id_exportdatascnbkn) && empty($id) && empty($nik) && empty($no_kk) && empty($nik_kk) && empty($password) && empty($nama) && empty($nama_ijazah) && empty($tempat_lahir) && empty($tempat_lahir_ijazah) && empty($tanggal_lahir) && empty($tanggal_lahir_ijazah) && empty($jenis_kelamin) && empty($email) && empty($pertanyaan_pengaman_1) && empty($jawaban_1) && empty($pertanyaan_pengaman_2) && empty($jawaban_2) && empty($agama) && empty($alamat_ktp) && empty($alamat_domisili) && empty($tinggi_badan) && empty($status_kawin) && empty($no_telp) && empty($no_hp) && empty($nama_ibu) && empty($nama_ayah) && empty($kodepos) && empty($lokasi_kabkota) && empty($tgl_daftar) && empty($akses_level) && empty($remarks))
            continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

        // Cek $numrow apakah lebih dari 1
        // Artinya karena baris pertama adalah nama-nama kolom
        // Jadi dilewat saja, tidak usah diimport
        if($numrow > 1){
            // Validasi apakah semua data telah diisi
            $id_exportdatascnbkn_td = ( ! empty($id_exportdatascnbkn))? "" : " style='background: #E07171;'"; // Jika No kosong, beri warna merah
            $id_td = ( ! empty($id))? "" : " style='background: #E07171;'"; // Jika Id kosong, beri warna merah
            $nik_td = ( ! empty($nik))? "" : " style='background: #E07171;'"; // Jika Nik kosong, beri warna merah
            $no_kk_td = ( ! empty($no_kk))? "" : " style='background: #E07171;'"; // Jika No KK kosong, beri warna merah
            $nik_kk_td = ( ! empty($nik_kk))? "" : " style='background: #E07171;'"; // Jika Nik KK kosong, beri warna merah
            $password_td = ( ! empty($password))? "" : " style='background: #E07171;'"; // Jika Password kosong, beri warna merah
            $nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
            $nama_ijazah_td = ( ! empty($nama_ijazah))? "" : " style='background: #E07171;'"; // Jika Nama Ijazah kosong, beri warna merah
            $tempat_lahir_td = ( ! empty($tempat_lahir))? "" : " style='background: #E07171;'"; // Jika Tempat Lahir kosong, beri warna merah
            $tempat_lahir_ijazah_td = ( ! empty($tempat_lahir_ijazah))? "" : " style='background: #E07171;'"; // Jika Tempat Lahir Ijazah kosong, beri warna merah
            $tanggal_lahir_td = ( ! empty($tanggal_lahir))? "" : " style='background: #E07171;'"; // Jika Tanggal Lahir kosong, beri warna merah
            $tanggal_lahir_ijazah_td = ( ! empty($tanggal_lahir_ijazah))? "" : " style='background: #E07171;'"; // Jika Tanggal Lahir pada Ijazah kosong, beri warna merah
            $jenis_kelamin_td = ( ! empty($jenis_kelamin))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
            $email_td = ( ! empty($email))? "" : " style='background: #E07171;'"; // Jika Email kosong, beri warna merah
            $pertanyaan_pengaman_1_td = ( ! empty($pertanyaan_pengaman_1))? "" : " style='background: #E07171;'"; // Jika Pertanyaan Pengaman_1 kosong, beri warna merah
            $jawaban_1_td = ( ! empty($jawaban_1))? "" : " style='background: #E07171;'"; // Jika Jawaban 1 kosong, beri warna merah
            $pertanyaan_pengaman_2_td = ( ! empty($pertanyaan_pengaman_2))? "" : " style='background: #E07171;'"; // Jika Pertanyaan Pengaman 2 kosong, beri warna merah
            $jawaban_2_td = ( ! empty($jawaban_2))? "" : " style='background: #E07171;'"; // Jika Jawaban 2 kosong, beri warna merah
            $agama_td = ( ! empty($agama))? "" : " style='background: #E07171;'"; // Jika Agama kosong, beri warna merah
            $alamat_ktp_td = ( ! empty($alamat_ktp))? "" : " style='background: #E07171;'"; // Jika Alamat ktp kosong, beri warna merah
            $alamat_domisili_td = ( ! empty($alamat_domisili))? "" : " style='background: #E07171;'"; // Jika Alamat Domisili kosong, beri warna merah
            $tinggi_badan_td = ( ! empty($tinggi_badan))? "" : " style='background: #E07171;'"; // Jika Tinggi Badan kosong, beri warna merah
            $status_kawin_td = ( ! empty($status_kawin))? "" : " style='background: #E07171;'"; // Jika Status Nikah kosong, beri warna merah
            $no_telp_td = ( ! empty($no_telp))? "" : " style='background: #E07171;'"; // Jika Nomer Telephone kosong, beri warna merah
            $no_hp_td = ( ! empty($no_hp))? "" : " style='background: #E07171;'"; // Jika Nomer Handphone kosong, beri warna merah
            $nama_ibu_td = ( ! empty($nama_ibu))? "" : " style='background: #E07171;'"; // Jika Nama Ibu kosong, beri warna merah
            $nama_ayah_td = ( ! empty($nama_ayah))? "" : " style='background: #E07171;'"; // Jika Nama Ayah kosong, beri warna merah
            $kodepos_td = ( ! empty($kodepos))? "" : " style='background: #E07171;'"; // Jika Kodepos kosong, beri warna merah
            $lokasi_kabkota_td = ( ! empty($lokasi_kabkota))? "" : " style='background: #E07171;'"; // Jika Lokasi Kabupaten Kota kosong, beri warna merah
            $tgl_daftar_td = ( ! empty($tgl_daftar))? "" : " style='background: #E07171;'"; // Jika Tanggal Daftar kosong, beri warna merah
            //$akses_level_td = ( ! empty($akses_level))? "" : " style='background: #E07171;'"; // Jika Tanggal Daftar kosong, beri warna merah
            $remarks_td = ( ! empty($remarks))? "" : " style='background: #E07171;'"; // Jika Remarks kosong, beri warna merah

            // Jika salah satu data ada yang kosong
            if(empty($id_exportdatascnbkn) && empty($id) && empty($nik) && empty($no_kk) && empty($nik_kk) && empty($password) && empty($nama) && empty($nama_ijazah) && empty($tempat_lahir) && empty($tempat_lahir_ijazah) && empty($tanggal_lahir) && empty($tanggal_lahir_ijazah) && empty($jenis_kelamin) && empty($email) && empty($pertanyaan_pengaman_1) && empty($jawaban_1) && empty($pertanyaan_pengaman_2) && empty($jawaban_2) && empty($agama) && empty($alamat_ktp) && empty($alamat_domisili) && empty($tinggi_badan) && empty($status_kawin) && empty($no_telp) && empty($no_hp) && empty($nama_ibu) && empty($nama_ayah) && empty($kodepos) && empty($lokasi_kabkota) && empty($tgl_daftar) && empty($remarks)){
            //if(empty($id_exportdatascnbkn) && empty($id) && empty($nik) && empty($no_kk) && empty($nik_kk) && empty($password) && empty($nama) && empty($nama_ijazah) && empty($tempat_lahir) && empty($tempat_lahir_ijazah) && empty($tanggal_lahir) && empty($tanggal_lahir_ijazah) && empty($jenis_kelamin) && empty($email) && empty($pertanyaan_pengaman_1) && empty($jawaban_1) && empty($pertanyaan_pengaman_2) && empty($jawaban_2) && empty($agama) && empty($alamat_ktp) && empty($alamat_domisili) && empty($tinggi_badan) && empty($status_kawin) && empty($no_telp) && empty($no_hp) && empty($nama_ibu) && empty($nama_ayah) && empty($kodepos) && empty($lokasi_kabkota) && empty($tgl_daftar) && empty($akses_level) && empty($remarks)){
                $kosong++; // Tambah 1 variabel $kosong
            }

            /*echo "<tr>";
            echo "<td".$nis_td.">".$nis."</td>";
            echo "<td".$nama_td.">".$nama."</td>";
            echo "<td".$jk_td.">".$jenis_kelamin."</td>";
            echo "<td".$alamat_td.">".$alamat."</td>";
            echo "</tr>";*/
            echo "<tr>";
            echo "<td".$id_exportdatascnbkn_td.">".$id_exportdatascnbkn."</td>";
            echo "<td".$id_td.">".$id."</td>";
            echo "<td".$nik_td.">".$nik."</td>";
            echo "<td".$no_kk_td.">".$no_kk."</td>";
            echo "<td".$nik_kk_td.">".$nik_kk."</td>";
            echo "<td".$password_td.">".$password."</td>";
            echo "<td".$nama_td.">".$nama."</td>";
            echo "<td".$nama_ijazah_td.">".$nama_ijazah."</td>";
            echo "<td".$tempat_lahir_td.">".$tempat_lahir."</td>";
            echo "<td".$tempat_lahir_ijazah_td.">".$tempat_lahir_ijazah."</td>";
            echo "<td".$tanggal_lahir_td.">".$tanggal_lahir_ijazah."</td>";
            echo "<td".$tanggal_lahir_ijazah_td.">".$tanggal_lahir_ijazah."</td>";
            echo "<td".$jenis_kelamin_td.">".$jenis_kelamin."</td>";
            echo "<td".$email_td.">".$email."</td>";
            echo "<td".$pertanyaan_pengaman_1_td.">".$pertanyaan_pengaman_1."</td>";
            echo "<td".$jawaban_1_td.">".$jawaban_1."</td>";
            echo "<td".$pertanyaan_pengaman_2_td.">".$pertanyaan_pengaman_2."</td>";
            echo "<td".$jawaban_2_td.">".$jawaban_2."</td>";
            echo "<td".$agama_td.">".$agama."</td>";
            echo "<td".$alamat_ktp_td.">".$alamat_ktp."</td>";
            echo "<td".$alamat_domisili_td.">".$alamat_domisili."</td>";
            echo "<td".$tinggi_badan_td.">".$tinggi_badan."</td>";
            echo "<td".$status_kawin_td.">".$status_kawin."</td>";
            echo "<td".$no_telp_td.">".$no_telp."</td>";
            echo "<td".$no_hp_td.">".$no_hp."</td>";
            echo "<td".$nama_ibu_td.">".$nama_ibu."</td>";
            echo "<td".$nama_ayah_td.">".$nama_ayah."</td>";
            echo "<td".$kodepos_td.">".$kodepos."</td>";
            echo "<td".$lokasi_kabkota_td.">".$lokasi_kabkota."</td>";
            echo "<td".$tgl_daftar_td.">".$tgl_daftar."</td>";
            //echo "<td>".$akses_level_td."".$akses_level."</td>";
            echo "<td".$remarks_td.">".$remarks."</td>";
            echo "</tr>";
        }

        $numrow++; // Tambah 1 setiap kali looping
    }

    echo "</table>";

    // Cek apakah variabel kosong lebih dari 1
    // Jika lebih dari 1, berarti ada data yang masih kosong
    if($kosong > 1){
        ?>
        <script>
            $(document).ready(function(){
                // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                $("#kosong").show(); // Munculkan alert validasi kosong
            });
        </script>
        <?php
    }else{ // Jika semua data sudah diisi
        echo "<hr>";

        // Buat sebuah tombol untuk mengimport data ke database
        echo "<button type='submit' name='import'>Import</button>";
        echo "<a href='".base_url("index.php/Upload_bkn")."'>Cancel</a>";
    }

    echo "</form>";
}
?>