
<html>
<head>
	<title>Form Summary IMPORT Data BKN</title>
</head>
<body>
	<h1>Data Uploader Summary BKN Lattest</h1><hr>
	<a href="<?php echo base_url("index.php/admin/Uploadscnbkn/form"); ?>">Import Data BKN</a><br><br>
	<a href="<?php echo base_url("index.php/admin/Dasbor"); ?>">Kembali Ke Dashboard</a><br><br>
	<table border="1" cellpadding="8">
	<tr>
		<th>ID</th>
		<th>NIK</th>
		<th>No KK</th>
		<th>NIK KK</th>
		<th>Password</th>
		<th>Nama</th>
		<th>Nama Ijazah</th>
		<th>Tempat Lahir</th>
		<th>Tempat Lahir Ijazah</th>
		<th>Tanggal Lahir</th>
		<th>Tanggal Lahir Ijazah</th>
		<th>Jenis Kelamin</th>
		<th>Email</th>
		<th>Pertanyaan 1</th>
		<th>Jawaban 1</th>
		<th>Pertanyaan 2</th>
		<th>Jawaban 2</th>
		<th>Agama</th>
		<th>Alamat KTP</th>
		<th>Alamat Domisili</th>
		<th>Tinggi Badan</th>
		<th>Status Kawin</th>
		<th>No Telp</th>
		<th>No HP</th>
		<th>Nama Ibu</th>
		<th>Nama Ayah</th>
		<th>Kodepos</th>
		<th>Lokasi Kabkota</th>
		<th>Tanggal Daftar</th>
		<th>Remarks</th>
	</tr>

	<?php
	if( ! empty($exportdatascnbkn)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
		foreach($exportdatascnbkn as $data){ // Lakukan looping pada variabel exportdatascnbkn dari controller
			echo "<tr>";
			echo "<td>".$data->id."</td>";
			echo "<td>".$data->nik."</td>";
			echo "<td>".$data->no_kk."</td>";
			echo "<td>".$data->nik_kk."</td>";
			echo "<td>".$data->password."</td>";
			echo "<td>".$data->nama."</td>";
			echo "<td>".$data->nama_ijazah."</td>";
			echo "<td>".$data->tempat_lahir."</td>";
			echo "<td>".$data->tempat_lahir_ijazah."</td>";
			echo "<td>".$data->tanggal_lahir."</td>";
			echo "<td>".$data->tanggal_lahir_ijazah."</td>";
			echo "<td>".$data->jenis_kelamin."</td>";
			echo "<td>".$data->email."</td>";
			echo "<td>".$data->pertanyaan_pengaman_1."</td>";
			echo "<td>".$data->jawaban_1."</td>";
			echo "<td>".$data->pertanyaan_pengaman_2."</td>";
			echo "<td>".$data->jawaban_2."</td>";
			echo "<td>".$data->agama."</td>";
			echo "<td>".$data->alamat_ktp."</td>";
			echo "<td>".$data->alamat_domisili."</td>";
			echo "<td>".$data->tinggi_badan."</td>";
			echo "<td>".$data->status_kawin."</td>";
			echo "<td>".$data->no_telp."</td>";
			echo "<td>".$data->no_hp."</td>";
			echo "<td>".$data->nama_ibu."</td>";
			echo "<td>".$data->nama_ayah."</td>";
			echo "<td>".$data->kodepos."</td>";
			echo "<td>".$data->lokasi_kabkota."</td>";
			echo "<td>".$data->tgl_daftar."</td>";
			//echo "<td>".$data->create_date."</td>";
			//echo "<td>".$data->create_by."</td>";
			echo "<td>".$data->remarks."</td>";
			echo "</tr>";
		}
	}else{ // Jika data tidak ada
		echo "<tr><td colspan='4'>Data tidak ada</td></tr>";
	}
	?>
	</table>
</body>
</html>
