<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_pengumuman_admin" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        // Attach file

        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  

        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            window.location.href=jsonResponse.redirect;
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();

        }

        });

        event.preventDefault();
    });


});

</script>
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'pengumuman_admin'?>">PENGUMUMAN</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Modul Pengumuman) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">
                    <form role="form" method="post" id="form_pengumuman_admin" action="<?php echo base_url('pengumuman_admin/process') ?>" enctype="multipart/form-data">


                        <label>Nomor SK Pengumuman</label><br />
                        <input type="text" class="form-control" id="no_pengumuman" name="no_pengumuman" value="<?php echo isset($value->no_pengumuman)?$value->no_pengumuman:''?>"/>
                        <br />

                        <label>Tanggal Pengumuman</label><br />
                        <input type="date" name="tanggal_pengumuman" id="tanggal_pengumuman" class="form-control" value="<?php echo isset($value->tanggal_pengumuman)?$this->tanggal->formatDateForm($value->tanggal_pengumuman):''?>"/>
                        <br />

                        <label>Judul</label><br />
                        <input type="text" class="form-control" id="judul_pengumuman" name="judul_pengumuman" value="<?php echo isset($value->judul_pengumuman)?$value->judul_pengumuman:''?>"/>
                        <br />

                        <label>Kategori</label><br />
                        <?php echo $this->master->custom_selection(array('table' => 'kategori_pengumuman', 'id' => 'id_kategori_pengumuman', 'name' => 'nama_kategori_pengumuman', 'where' => array() ), isset($value->id_kategori_pengumuman)?$value->id_kategori_pengumuman:'','id_kategori_pengumuman','id_kategori_pengumuman','form-control','','')?>
                        <br />

                        <label>File Lampiran</label><br />
                        <input type="file" id="uploadfile" name="file_pengumuman" class="form-control"/>
                        <br />
                        <?php 
                        if(isset($value)){
                            echo 'Klik <a href="'.base_url().'assets/upload/image/'.$value->file_pengumuman.'" target="_blank"><i>disini</i></a> untuk melihat lampiran';
                        }

                        ?>
                        <br>
                        <br>

                        <label>Status Pengumuman</label><br />
                        <div class="radio">
                              <label>
                                <input name="status_pengumuman" type="radio" class="ace" value="NEW" <?php echo isset($value) ? ($value->status_pengumuman == 'NEW') ? 'checked="checked"' : '' : 'checked="checked"'; ?> />
                                <span class="lbl"> Terbaru</span>
                              </label>
                              <label>
                                <input name="status_pengumuman" type="radio" class="ace" value="EXP" <?php echo isset($value) ? ($value->status_pengumuman == 'EXP') ? 'checked="checked"' : '' : ''; ?>/>
                                <span class="lbl"> Expired</span>
                              </label>
                        </div>
                        <br />

                        <label>Deskripsi</label><br />
                        <textarea class="ckeditor" id="ckeditor" name="deskripsi_pengumuman"><?php echo isset($value->deskripsi_pengumuman)?$value->deskripsi_pengumuman:''?></textarea>
                        <br />
                        <!-- btn submit -->
                        <center>
                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($value->id_pengumuman)?$value->id_pengumuman:''?>"/>
                        <a href="<?php echo base_url().'pengumuman_admin'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>

                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->
