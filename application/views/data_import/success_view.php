<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">

<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'data_import'?>">KELENGKAPAN DATA CPNS</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Silahkan lengkapi data dibawah ini) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">

                    <br>
                    <div class="alert alert-success">
                        <center>
                            <h2 style="color:green; margin-bottom:-5px">Terima Kasih</h2>
                            Data anda akan diverifikasi oleh Verifikator Kementerian PUPR<br>
                            Informasi selanjutnya akan kami kirim via email yang anda masukan
                        </center>
                    </div>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

