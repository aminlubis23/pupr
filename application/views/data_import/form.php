<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_data_cpns" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        // Attach file
        /*formData.append('file', $('input[type=file]').files[0]); */
        
        /*$.each($("input[type='file']")[0].files, function(i, file) {
            alert(file); return false;
            console.log(file);
            formData.append('file', file);
        });*/


        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  

        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            window.location.href=jsonResponse.redirect;
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();

        }

        });

        event.preventDefault();
    });

    $('select[name="pend_universitas"]').change(function () {
        if ($(this).val()) {
            $.getJSON("<?php echo site_url('mst_university/get_detail_univ') ?>/" + $(this).val(), '', function (data) {
                $('#detail_university').html('<span style="color:green;"><i>Perguruan Tinggi "'+data.univ_status+'"</i> Akreditasi <i>"'+data.univ_akreditasi+'"</i></span>');
            });

            

        } else {
            $('#detail_university').html('Tidak Terkareditasi');
            $('#pend_jurusan option').remove()
            $('#detail_majors').html('');
        }
    });

    /*$('select[name="pend_jurusan"]').change(function () {
        var univ_id = $('#univIdHidden').val();
        if ($(this).val()) {
            $.getJSON("<?php echo site_url('mst_majors/get_detail_majors') ?>/" + $(this).val() + '/'+ univ_id, '', function (data) {
                $('#detail_majors').html('<span style="color:green;"> Akreditasi Prodi <i>"'+data.akreditasi+'"</i></span>');
            });

        } else{
             $('#detail_majors').html('<span style="color:red;"> Tidak Terakreditasi </span>');
        }
    });*/

    /*$('select[name="formasi_jabatan"]').change(function () {
        if ($(this).val()) {
            $.getJSON("<?php echo site_url('verifikasi_data_cpns/getKualifikasiPendByFormasiJabatan') ?>/" + $(this).val(), '', function (data) {
                $('#formasi_jurusan_pendidikan option').remove();
                $('<option value="">-Pilih Kualifikasi Pendidikan-</option>').appendTo($('#formasi_jurusan_pendidikan'));
                $.each(data, function (i, o) {
                    $('<option value="' + o.kp_id + '">' + o.kp_name + '</option>').appendTo($('#formasi_jurusan_pendidikan'));
                });

            });

            $.getJSON("<?php echo site_url('verifikasi_data_cpns/getPenempatanByFormasiJabatan') ?>/" + $(this).val(), '', function (data) {
                $('#formasi_penempatan option').remove();
                $('<option value="">-Pilih Penempatan-</option>').appendTo($('#formasi_penempatan'));
                $.each(data, function (i, o) {
                    $('<option value="' + o.up_id + '">' + o.up_name + '</option>').appendTo($('#formasi_penempatan'));
                });

            });

        } else {
            $('#formasi_jabatan option').remove()
            $('#formasi_penempatan option').remove()
        }
    });*/



});

</script>

<script src="<?php echo base_url()?>js/typeahead.jquery.js"></script>
<script src="<?php echo base_url()?>js/typeahead.js"></script>

<script>
  $(document).ready(function () {

      $('#inputKeyUniversitas').typeahead({
          source: function (query, result) {
              $.ajax({
                  url: "<?php echo base_url()?>mst_university/getUniversityByKeyword",
                  data: 'keyword=' + query,              
                  dataType: "json",
                  type: "POST",
                  success: function (response) {
                    result($.map(response, function (item) {
                        return item;
                    }));
                  }
              });
          },
          afterSelect: function (item) {
            // do what is needed with item
            $('#table_detail_akreditasi_majors').hide();
            $('#majorsIdHidden').val('');
            $('#pend_jurusan').val('');
            $('#inputKeyJurusan').val('');
            var val_item=item.split(':')[0];
            console.log(val_item);
            $('#univIdHidden').val(val_item);
            /*show detail akreditasi*/
            $.getJSON("<?php echo base_url()?>mst_university/getDetailAkreditasiUniv/"+val_item, '', function (data) {
                $('#table_detail_akreditasi').show();
                if(data.univ_akreditasi==null){
                    $('#table_body_content').html('<tr><td>'+data.univ_id+'</td><td>'+data.univ_name+'</td><td colspan="4" style="color:red"><i><b>Belum Terakreditasi</b> atau Data Akreditasi tidak ada dalam Database Kami</i></td></tr>');

                }else{
                    $('#table_body_content').html('<tr><td>'+data.univ_id+'</td><td>'+data.univ_name+'</td><td>'+data.univ_akreditasi+'</td><td>'+data.univ_tahun_sk+'</td><td>'+data.univ_tgl_sk+'</td><td>'+data.univ_tgl_expired_sk+'</td></tr>');
                }

                /*$.getJSON("<?php echo site_url('mst_university/getMajorsByUniversity') ?>/" + val_item, '', function (data) {
                    $('#pend_jurusan option').remove();
                    $('<option value="">-Pilih Jurusan-</option>').appendTo($('#pend_jurusan'));
                    $.each(data, function (i, o) {
                        $('<option value="' + o.majors_id + '">' + o.majors_name + '</option>').appendTo($('#pend_jurusan'));
                    });

                });*/

            });


          }
      });

      $('#inputKeyJurusan').typeahead({

          source: function (query, result) {
              univ_id = $('#univIdHidden').val();
              if(univ_id==''){
                alert('Silahkan Pilih Universitas'); return false;
              }
              $.ajax({
                  url: "<?php echo base_url()?>mst_majors/getMajorsByKeyword",
                  data: 'keyword=' + query + '&univ_id=' +univ_id,              
                  dataType: "json",
                  type: "POST",
                  success: function (response) {
                    result($.map(response, function (item) {
                        return item;
                    }));
                  }
              });
          },
          afterSelect: function (item) {
            // do what is needed with item
            

            if(univ_id==''){
                alert('Silahkan Pilih Jurusan'); return false;
            }else{
                var majors_id=item.split(':')[0];
                console.log(majors_id);
                $('#majorsIdHidden').val(majors_id);
                /*show detail akreditasi*/
                $.getJSON("<?php echo base_url()?>mst_majors/get_detail_majors/"+majors_id+"/"+univ_id, '', function (data) {
                    $('#table_detail_akreditasi_majors').show();
                    if(data==null){
                        $('#table_body_content_majors').html('<tr><td>'+data.univ_id+'</td><td>'+data.univ_name+'</td><td colspan="4" style="color:red"><i><b>Belum Terakreditasi</b> atau Data Akreditasi tidak ada dalam Database Kami</i></td></tr>');

                    }else{
                        $('#table_body_content_majors').html('<tr><td>'+data.majors_id+'</td><td>'+data.majors_name+'</td><td>'+data.akreditasi+'</td><td>'+data.tahun+'</td><td>'+data.tgl_sk+'</td><td>'+data.tgl_kadaluarsa+'</td></tr>');
                    }

                });
            }
            


          }
      });

      $('input:radio[name=verifikasi_is_ptt]:checked').change(function () {
            if ($("input[name='verifikasi_is_ptt']:checked").val() == 'Y') {
                $.getJSON("<?php echo base_url()?>set_parameter/getById", '', function (data) {
                    if(data.max_usia_ptt>0){
                       $('#WarningPtt').html('Batas usia maximum PTT adalah '+data.max_usia_ptt+' Tahun');
                    }
                });
            }

        });

      
  });
</script>


<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'data_import'?>">KELENGKAPAN DATA CPNS</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Silahkan lengkapi data dibawah ini) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

            <?php echo validation_errors(); ?>

                <!-- content data table -->
                <div class="col-md-12">
                    <form id="form_data_cpns" method="post" action="<?php echo base_url('data_import/process') ?>" enctype="multipart/form-data">
                        <?php if(isset($value->verifikasi_status) || $value->verifikasi_status==1): ?>
                            <div class="alert alert-success">
                                <b>Terima Kasih !</b> Data anda berhasil diproses<br>
                                <small style="font-size:11px !important">
                                Proses Verifikasi Data akan dilakukan mulai tanggal <b>31 Oktober 2018</b><br>
                                Silahkan lengkapi data anda kembali sebelum diverifikasi oleh Admin Kementerian PUPR. <br>
                                </small>
                            </div>
                        <?php endif;?>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-user"></i> Biodata Diri</h4></div>
                                <br>
                                <!-- end block title -->

                                <!-- foto profile -->
                                <div class="col-md-2">
                                    <img src="<?php echo isset($file[2])?base_url().'uploaded_files/data_pelamar/'.$file[2].'':base_url().'assets/admin/images/no_photo.jpg'?>" class="img-responsive" alt="" style="width: 100%" /><br>
                                    Pas Foto 3x4
                                    <br />
                                    <input type="file" id="pas_foto" name="file[2]" class="form-control"/>
                                </div>
                                <!-- end foto profile -->

                                <!-- block biodata diri -->
                                <div class="col-md-5">
                                    
                                    <label>Nama Lengkap</label><br />
                                    <input type="text" id="nama_lengkap" name="dp_nama_lengkap" value="<?php echo isset($value->dp_nama_lengkap)?$value->dp_nama_lengkap:isset($value)?$value->nama:set_value('dp_nama_lengkap')?>" class="form-control" placeholder="" /><?php echo form_error('dp_nama_lengkap'); ?><br />

                                    <label>No KK</label><br />
                                    <input type="text" id="no_kk" name="dp_no_kk" value="<?php echo isset($value->dp_no_kk)?$value->dp_no_kk:isset($value)?$value->no_kk:set_value('dp_no_kk')?>" class="form-control" placeholder="" /><?php echo form_error('dp_no_kk'); ?><br />

                                    <label>Jenis Kelamin</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_gender', 'id' => 'gender_id', 'name' => 'gender_name', 'where' => array() ), isset($value->dp_jk)?$value->dp_jk:set_value('dp_jk'),'dp_jk','dp_jk','form-control','','')?><?php echo form_error('dp_jk'); ?><br />

                                    <label>KTP</label><br />
                                    <input type="file" id="ktp" class="form-control" name="file[1]"/><br />

                                    

                                    
                                    
                                </div>
                                
                                <div class="col-md-5">

                                    <label>NIK</label><br />
                                    <input type="text" id="nik" name="dp_nik" value="<?php echo isset($value->dp_nik)?$value->dp_nik:isset($value)?$value->nik:set_value('dp_nik')?>" class="form-control" /><?php echo form_error('dp_nik'); ?><br />

                                    <div class="col-md-6">
                                    <label>Tempat Lahir</label><br />
                                        <input type="text" id="dp_tempat_lahir" name="dp_tempat_lahir" value="<?php echo isset($value->dp_tempat_lahir)?$value->dp_tempat_lahir:isset($value)?$value->tempat_lahir:set_value('dp_tempat_lahir')?>" class="form-control" /><?php echo form_error('dp_tempat_lahir'); ?><br />
                                    </div>

                                    <div class="col-md-6">
                                    <label>Tanggal Lahir</label><br />
                                    <input type="date" id="tanggal_lahir" name="dp_tanggal_lahir" value="<?php echo isset($value->dp_tanggal_lahir)?$value->dp_tanggal_lahir:set_value('dp_tanggal_lahir')?>" class="form-control" /><?php echo form_error('dp_tanggal_lahir'); ?><br />
                                    </div>

                                    <div class="col-md-6">
                                    <label>Agama</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_religion', 'id' => 'religion_id', 'name' => 'religion_name', 'where' => array() ), isset($value->dp_agama)?$value->dp_agama:set_value('dp_agama'),'dp_agama','dp_agama','form-control','','')?><?php echo form_error('dp_agama'); ?>
                                    <br>
                                    </div>
                                    <div class="col-md-6">
                                    <label>Status Nikah</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_marital_status', 'id' => 'ms_id', 'name' => 'ms_name', 'where' => array() ), isset($value->dp_status_nikah)?$value->dp_status_nikah:set_value('dp_status_nikah'),'dp_status_nikah','dp_status_nikah','form-control','','')?><?php echo form_error('dp_status_nikah'); ?>
                                    <br />
                                    </div>

                                    <div class="col-md-6">
                                        <label>Surat Pernyataan</label><br />
                                        <input type="file" id="surat_pernyataan" name="file[3]" class="form-control"/><br />
                                    </div>
                                    <div class="col-md-6">
                                        <label>KK</label><br />
                                        <input type="file" id="kk" name="file[4]" class="form-control"/>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-info"></i> Kontak Informasi</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-6">

                                    <label>Alamat Lengkap</label><br />
                                    <input type="text" id="alamat_lengkap" name="dp_alamat_lengkap" class="form-control" value="<?php echo isset($value->dp_alamat_lengkap)?$value->dp_alamat_lengkap:isset($value)?$value->alamat_ktp:set_value('dp_alamat_lengkap')?>" /><?php echo form_error('dp_alamat_lengkap'); ?><br />

                                    <label>Alamat Domisili <small style="font-size:11px !important"><i> (Tempat tinggal saat ini) </i> </small> </label><br />
                                    <input type="radio" name="dp_domisili_negara" value="DN" <?php echo isset($value)?($value->dp_domisili_negara=='DN')?'checked':'':''?>> Dalam Negeri &nbsp;&nbsp;&nbsp;

                                    <input type="radio" name="dp_domisili_negara" value="LN" <?php echo isset($value)?($value->dp_domisili_negara=='LN')?'checked':'':''?>> Luar Negeri 
                                    <br>
                                    <input type="text" id="alamat_org_tua" name="dp_alamat_org_tua" value="<?php echo isset($value->dp_alamat_org_tua)?$value->dp_alamat_org_tua:set_value('dp_alamat_org_tua')?>" class="form-control" /><?php echo form_error('dp_alamat_org_tua'); ?><br />

                                </div>
                                
                                <div class="col-md-3">
                                    <label>Nomor Telp</label><br />
                                    <input type="text" id="dp_no_telp" name="dp_no_telp" value="<?php echo isset($value->dp_no_telp)?$value->dp_no_telp:set_value('dp_no_telp')?>" class="form-control" /><?php echo form_error('dp_no_telp'); ?><br />
                                </div>
                                <div class="col-md-3">
                                    <label>Nomor Handphone</label><br />
                                    <input type="text" id="dp_no_hp" name="dp_no_hp" value="<?php echo isset($value->dp_no_hp)?$value->dp_no_hp:set_value('dp_no_hp')?>" class="form-control" /><?php echo form_error('dp_no_hp'); ?><br />
                                </div>
                                <div class="col-md-6">

                                    <label>Email</label><br />
                                    <input type="text" id="dp_email" name="dp_email" value="<?php echo isset($value->dp_email)?$value->dp_email:set_value('dp_email')?>" class="form-control" /><?php echo form_error('dp_email'); ?><br />

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-list"></i> Formasi</h4></div>
                                <br>
                                <!-- end block title -->

                                <div class="col-md-3">
                                    <label>Jenis Formasi</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_formasi_jenis', 'id' => 'formasi_jenis_id', 'name' => 'formasi_jenis_name', 'where' => array() ), isset($value->formasi_jenis)?$value->formasi_jenis:set_value('formasi_jenis'),'formasi_jenis','formasi_jenis','form-control','','')?><?php echo form_error('formasi_jenis'); ?><br />
                                </div> 

                                <div class="col-md-6">
                                    <label>Formasi Jabatan</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_formasi_jabatan', 'id' => 'fj_id', 'name' => 'fj_name', 'where' => array() ), isset($value->formasi_jabatan)?$value->formasi_jabatan:set_value('formasi_jabatan'),'formasi_jabatan','formasi_jabatan','form-control','','')?><?php echo form_error('formasi_jabatan'); ?><br />
                                </div>

                                <div class="col-md-3">&nbsp;</div>
                                <div class="col-md-6">
                                    <label>Lokasi Penempatan</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_unit_penempatan', 'id' => 'up_id', 'name' => 'up_name', 'where' => array() ), isset($value->formasi_penempatan)?$value->formasi_penempatan:set_value('formasi_penempatan'),'formasi_penempatan','formasi_penempatan','form-control','','')?><?php echo form_error('formasi_penempatan'); ?><br />
                                </div>

                                <div class="col-md-6">
                                    <label>Kualifikasi Pendidikan</label><br />
                                    <?php echo $this->master->custom_selection_with_concat_join(array('table' => 'mst_kualifikasi_pendidikan', 'id' => 'kp_id', 'name' => 'kp_name', 'where' => array(), 'concat' => 'CONCAT(jenjang,"-",majors_name) as kp_name', 'join' => array(array('relation_table' => 'mst_majors','relation_ref_id' => 'majors_id','relation_id' => 'majors_id','relation_type' => 'left')) ), isset($value->formasi_jurusan_pendidikan)?$value->formasi_jurusan_pendidikan:'' ,'formasi_jurusan_pendidikan','formasi_jurusan_pendidikan','form-control','','')?><br />
                                </div>

                                <!-- <div class="col-md-6">
                                    <label>Kualifikasi Pendidikan</label><br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_kualifikasi_pendidikan', 'id' => 'kp_id', 'name' => 'kp_name', 'where' => array() ), isset($value->formasi_jurusan_pendidikan)?$value->formasi_jurusan_pendidikan:set_value('formasi_jurusan_pendidikan'),'formasi_jurusan_pendidikan','formasi_jurusan_pendidikan','form-control','','')?><?php echo form_error('formasi_jurusan_pendidikan'); ?><br />
                                </div> -->

                                <div class="col-md-4">
                                    <label>PTT? <small style="font-size:11px !important"><i> (Apakah anda Pegawai Tidak Tetap pada Kementerian PUPR?) </i> </small> </label><br />
                                    <input type="radio" name="verifikasi_is_ptt" value="Y" <?php echo isset($value)?($value->verifikasi_is_ptt=='Y')?'checked':'':''?> > Ya &nbsp;&nbsp;&nbsp;

                                    <input type="radio" name="verifikasi_is_ptt" value="N" <?php echo isset($value)?($value->verifikasi_is_ptt=='N')?'checked':'':''?> > Tidak
                                    <br>
                                    <span id="WarningPtt" style="color:red;font-style:italic"></span>
                                </div>
                                <br>                              

                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-bolt"></i> Lokasi Ujian</h4></div>
                                <br>

                                <div class="col-md-6">
                                    <label>Lokasi Ujian SKD</label><br />
                                    <?php echo $this->master->custom_selection_with_join(array('table' => 'mst_lokasi_ujian', 'id' => 'lokasi_ujian_id', 'name' => 'name', 'where' => array('ujian_skd' => 'Y'), 'join' =>  array( array('relation_table' => 'regencies', 'relation_id' => 'id', 'relation_ref_id' => 'regency_id', 'relation_type' => 'left') ) ), isset($value->loku_skd_id)?$value->loku_skd_id:set_value('formasi_lokasi_ujian_skd'),'formasi_lokasi_ujian_skd','formasi_lokasi_ujian_skd','form-control','','')?><?php echo form_error('formasi_lokasi_ujian_skd'); ?><br />
                                </div>

                                <div class="col-md-6">
                                    <label>Lokasi Ujian SKB</label><br />
                                    <?php echo $this->master->custom_selection_with_join(array('table' => 'mst_lokasi_ujian', 'id' => 'lokasi_ujian_id', 'name' => 'name', 'where' => array('ujian_skb' => 'Y'), 'join' =>  array( array('relation_table' => 'regencies', 'relation_id' => 'id', 'relation_ref_id' => 'regency_id', 'relation_type' => 'left') ) ), isset($value->loku_skb_id)?$value->loku_skb_id:set_value('formasi_lokasi_ujian_skb'),'formasi_lokasi_ujian_skb','formasi_lokasi_ujian_skb','form-control','','')?><?php echo form_error('formasi_lokasi_ujian_skb'); ?><br />
                                </div>

                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-graduation-cap"></i> Pendidikan</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-12">
                                    <label>Universitas</label><br />
                                    <input id="inputKeyUniversitas" class="form-control"  type="text" placeholder="Masukan keyword minimal 3 karakter" value="<?php echo isset($value->concat_univ)?$value->concat_univ:''?>"/>
                                    <input type="hidden" name="pend_universitas" id="univIdHidden" value="<?php echo isset($value)?$value->pend_universitas:''?>"><br>
                                </div>

                                <div class="col-md-12" id="table_detail_akreditasi" <?php echo isset($value->univ_akreditasi)?'':'style="display:none"'?> >
                                    <label><b>Akreditasi Universitas</b></label><br />
                                    <table class="table table-striped">
                                        <tr>
                                            <th style="background-color:#30d83d; color:white">ID</th>
                                            <th style="background-color:#30d83d; color:white">Nama Universitas</th>
                                            <th style="background-color:#30d83d; color:white">Akreditasi</th>
                                            <th style="background-color:#30d83d; color:white">Tahun</th>
                                            <th style="background-color:#30d83d; color:white">Tanggal SK</th>
                                            <th style="background-color:#30d83d; color:white">Tanggal Expired</th>
                                        </tr>
                                        <tbody id="table_body_content">
                                            <?php if(isset($value->univ_akreditasi)) :?>
                                            <tr>
                                                <th><?php echo $value->univ_id?></th>
                                                <th><?php echo $value->univ_name?></th>
                                                <th><?php echo $value->univ_akreditasi?></th>
                                                <th><?php echo $value->univ_tahun_sk?></th>
                                                <th><?php echo $this->tanggal->formatDate($value->univ_tgl_sk)?></th>
                                                <th><?php echo $this->tanggal->formatDate($value->univ_tgl_expired_sk)?></th>
                                            </tr>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                    <label>Jurusan</label><br />
                                    <input id="inputKeyJurusan" class="form-control"  type="text" placeholder="Masukan keyword minimal 3 karakter" value="<?php echo isset($value->concat_majors)?$value->concat_majors:''?>" />
                                    <input type="hidden" name="pend_jurusan" id="majorsIdHidden" value="<?php echo isset($value)?$value->pend_jurusan:''?>"><br>
                                </div>

                                <div class="col-md-12" id="table_detail_akreditasi_majors" <?php echo isset($akreditasi_jurusan->akreditasi)?'':'style="display:none"'?>>
                                    <label><b>Akreditasi Jurusan</b></label><br />
                                    <table class="table table-striped">
                                        <tr>
                                            <th style="background-color:#30d83d; color:white">ID</th>
                                            <th style="background-color:#30d83d; color:white">Nama Jurusan</th>
                                            <th style="background-color:#30d83d; color:white">Akreditasi</th>
                                            <th style="background-color:#30d83d; color:white">Tahun</th>
                                            <th style="background-color:#30d83d; color:white">Tanggal SK</th>
                                            <th style="background-color:#30d83d; color:white">Tanggal Expired</th>
                                        </tr>
                                        <tbody id="table_body_content_majors">
                                            <?php if(isset($akreditasi_jurusan->akreditasi)) :?>
                                            <tr>
                                                <th><?php echo $akreditasi_jurusan->majors_id?></th>
                                                <th><?php echo $akreditasi_jurusan->majors_name?></th>
                                                <th><?php echo $akreditasi_jurusan->akreditasi?></th>
                                                <th><?php echo $akreditasi_jurusan->tahun?></th>
                                                <th><?php echo $this->tanggal->formatDate($akreditasi_jurusan->tgl_sk)?></th>
                                                <th><?php echo $this->tanggal->formatDate($akreditasi_jurusan->tgl_kadaluarsa)?></th>
                                            </tr>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-3">
                                    <label>Tanggal Ijasah</label><br />
                                    <input type="date" id="pend_tgl_ijasah" name="pend_tgl_ijasah" class="form-control" placeholder="2017-02-01 / 2017-01-05" value="<?php echo isset($value->pend_tgl_ijasah)?$value->pend_tgl_ijasah:set_value('pend_tgl_ijasah')?>" /> <?php echo form_error('pend_tgl_ijasah'); ?><br />
                                </div>
                                <div class="col-md-3">
                                    <label>Nomor Ijasah</label><br />
                                        <input type="text" id="pend_no_ijasah" name="pend_no_ijasah" class="form-control" placeholder="SK/01/VII/2018" value="<?php echo isset($value->pend_no_ijasah)?$value->pend_no_ijasah:set_value('pend_no_ijasah')?>" /> <?php echo form_error('pend_no_ijasah'); ?>

                                        <br />
                                </div>
                                
                                <div class="col-md-3">
                                    <label>IPK </label><br />
                                    <input type="text" id="pend_ipk" name="pend_ipk" class="form-control" placeholder="3.50" value="<?php echo isset($value->pend_ipk)?$value->pend_ipk:set_value('pend_ipk')?>" /> <?php echo form_error('pend_ipk'); ?><i style="font-size:11px">(Gunakan titik ( . ) untuk angka decimal)</i>
                                    <br />
                                </div>

                                <div class="col-md-3">
                                    <label>Gelar Sarjana</label><br />
                                    <input type="text" id="pend_gelar_sarjana" name="pend_gelar_sarjana" class="form-control" placeholder="S.Si" value="<?php echo isset($value->pend_gelar_sarjana)?$value->pend_gelar_sarjana:set_value('pend_gelar_sarjana')?>" /> <?php echo form_error('pend_gelar_sarjana'); ?><br />

                                    
                                </div>
                                

                                <div class="col-md-6">
                                    <label>Ijasah</label><br />
                                    <input type="file" id="ijasah" name="file[5]" class="form-control"/>
                                    <br />
                                </div>
                                
                                <div class="col-md-6">
                                    <label>Transkrip  Nilai</label><br />
                                    <input type="file" id="transkrip" name="file[6]" class="form-control"/><br>
                                </div>


                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Kemampuan Bahasa Inggris</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-6">
                                    Jenis TOEFL<br />
                                    <?php echo $this->master->custom_selection(array('table' => 'mst_jenis_toefl', 'id' => 'jt_id', 'name' => 'jt_name', 'where' => array() ), isset($value->bhs_jenis_toefl)?$value->bhs_jenis_toefl:set_value('bhs_jenis_toefl'),'bhs_jenis_toefl','bhs_jenis_toefl','form-control','','')?><?php echo form_error('bhs_jenis_toefl'); ?>

                                    <br />
                                    Lembaga Penerbit TOEFL<br />
                                    <input type="text" id="bhs_lembaga_toefl" name="bhs_lembaga_toefl" class="form-control" placeholder="Kanguru International Education Service" value="<?php echo isset($value->bhs_lembaga_toefl)?$value->bhs_lembaga_toefl:set_value('bhs_lembaga_toefl')?>" /> <?php echo form_error('bhs_lembaga_toefl'); ?>

                                    <br />
                                    Tanggal Sertifikat<br />
                                    <input type="date" id="bhs_tanggal_sertifikat" name="bhs_tanggal_sertifikat" class="form-control" value="<?php echo isset($value->bhs_tanggal_sertifikat)?$value->bhs_tanggal_sertifikat:set_value('bhs_tanggal_sertifikat')?>" /> <?php echo form_error('bhs_tanggal_sertifikat'); ?>

                                    <br />
                                    Lokasi Tes TOEFL
                                    <br />
                                    <input type="text" id="bhs_lokasi_tes" name="bhs_lokasi_tes" class="form-control" placeholder="Banda Aceh" value="<?php echo isset($value->bhs_lokasi_tes)?$value->bhs_lokasi_tes:set_value('bhs_lokasi_tes')?>" /><?php echo form_error('bhs_lokasi_tes'); ?>

                                </div>
                                <div class="col-md-6">
                                    Nilai TOEFL
                                    <br />
                                    <input type="text" id="bhs_nilai_toefl" name="bhs_nilai_toefl" class="form-control" placeholder="477" value="<?php echo isset($value->bhs_nilai_toefl)?$value->bhs_nilai_toefl:set_value('bhs_nilai_toefl')?>" /> <?php echo form_error('bhs_nilai_toefl'); ?>

                                    <br />
                                    Nomor Sertifikat<br />
                                    <input type="text" id="bhs_no_sertifikat" name="bhs_no_sertifikat" class="form-control" placeholder="01/VI/2018" value="<?php echo isset($value->bhs_no_sertifikat)?$value->bhs_no_sertifikat:set_value('bhs_no_sertifikat')?>" /> <?php echo form_error('bhs_no_sertifikat'); ?>

                                    <br />
                                    Tanggal Tes TOEFL<br />
                                    <input type="date" id="bhs_tgl_tes_toefl" name="bhs_tgl_tes_toefl" class="form-control" value="<?php echo isset($value->bhs_tgl_tes_toefl)?$value->bhs_tgl_tes_toefl:set_value('bhs_tgl_tes_toefl')?>" /> <?php echo form_error('bhs_tgl_tes_toefl'); ?>

                                    <br />
                                    Sertifikat TOEFL<br />
                                    <input type="file" id="sertifikat_toefl" name="file[7]" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- btn submit -->
                        <center>
                        <input type="hidden" name="dp_id" value="<?php echo isset($value->dp_id)?$value->dp_id:0?>">
                        <?php if( in_array($this->session->userdata('user')->level, array(0,2))) :?>
                        <a href="<?php echo base_url().'data_import'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <?php endif;?>
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>
                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

