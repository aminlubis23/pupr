﻿<!DOCTYPE html>
<?php
/*title*/
$title_card_header = ($tipe=='skd')?'SKD':'SKB';
$long_text_title_card_header = ($tipe=='skd')?'SELEKSI KOMPETENSI DASAR':'SELEKSI KOMPETENSI BIDANG';
$field_sesi = ($tipe=='skd')?'loku_sesi_skd':'loku_sesi_skb';
$field_urut = ($tipe=='skd')?'loku_urut_skd':'loku_urut_skb';
$field_tanggal = ($tipe=='skd')?'tanggal_ujian_skd':'tanggal_ujian_skb';
$field_alamat = ($tipe=='skd')?'alamat_skd':'alamat_skb';
$field_kota = ($tipe=='skd')?'kota_skd_name':'kota_skb_name';
?>
<html>
<head>

    <title>Kartu Peserta Ujian <?php echo $title_card_header?></title>
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/vendor/bootstrap/css/bootstrap.min.css">
    <script src="<?php echo base_url().'assets/barcode-master/prototype/sample/prototype.js'?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/barcode-master/prototype/prototype-barcode.js'?>" type="text/javascript"></script>
<script type="text/javascript">

window.onload = generateBarcode;

  function generateBarcode(){
    $("barcodeTargetPESERTA").update();
    $("barcodeTargetPANITIA").update();
    var value = "<?php echo $value->dp_no_peserta?>";
    var btype = "code128";
    
    var settings = {
      output:"css",
      bgColor: "#FFFFFF",
      color: "#000000",
      barWidth: 1,
      barHeight: 80,
      moduleSize: 5,
      posX: 10,
      posY: 20,
      addQuietZone: false
    };

    $("barcodeTargetPESERTA").update().show().barcode(value, btype, settings);
    $("barcodeTargetPANITIA").update().show().barcode(value, btype, settings);

  }
    
</script> 
</head>
<style type="text/css">
    .sub-title{
        margin-top: -30px !important;
        font-size: 12px;
    }
    .content-card{
        font-size: 12px !important;
        width : 100%;
    }
    .table-content-card{
        font-size: 12px;
    }
    .table{
        margin-bottom: 0px !important;
    }
    .img-logo-card{
        margin: 0px 0px 0px 0px !important;
    }
</style>
<body>
    
    <?php 
        for($i=0;$i<2;$i++) :
            $title_card = ($i==0)?'PANITIA':'PESERTA';
            $color_card = ($i==0)?'red':'blue';

    ?>
    <div class="content-card">
        <table class="table" border="0" width="100%">
            <tr>
                <td width="80px"> <img class="img-logo-card" src="<?php echo base_url().'assets/upload/image//baru/logo_ori.jpg'?>" width="80px"> </td>
                <td valign="top"> <h4 style="margin-bottom:-0px;margin-top:10px;font-size:16px"><b>KARTU PESERTA <?php echo $long_text_title_card_header?></b></h4><small class="sub-title"> PENGADAAN CPNS TAHUN <?php echo date('Y')?> <br> KEMENTERIAN PEKERJAAN UMUM DAN PERUMAHAN RAKYAT</small></td>
                <td style="text-align: center; background-color:<?php echo $color_card?>;color:white;padding-top:28px">LEMBAR <?php echo $title_card?></td>
            </tr>
        </table>

        <table class="table table-content-card" border="0">
            <tr>
                <td width="150px"> <img src="<?php echo base_url().'assets/contoh_pas_foto.jpg'?>" width="150px"> </td>
                <td>
                    No. Peserta / NIK <br>
                    <b><?php echo $value->dp_no_peserta?> / <?php echo $value->dp_nik?> </b><br>
                    Nama (JK) <br>
                    <?php echo $value->dp_nama_lengkap?> (<?php echo $value->jk?>)<br>
                    Formasi <br>
                    <b>(<?php echo $value->formasi_jenis_name?>) <?php echo $value->fj_name ?> <br><?php echo $value->kp_name?></b>
                    <br>
                    Jadwal Ujian<br>
                    <b><?php echo ucwords(strtolower($value->$field_kota)) ?>, <?php echo $this->tanggal->formatDate($value->$field_tanggal) ?> - Sesi <?php echo $value->$field_sesi ?> - No.Urut <?php echo $value->$field_urut ?></b><br>
                    Alamat : <?php echo $value->$field_alamat ?>
                    <br>
                </td>
                <td align="right" style="padding-top:30px;padding-right:8px" width="100px">
                    <div style="margin-top:-10px">
                        <div id="barcodeTarget<?php echo $title_card?>" class="barcodeTarget<?php echo $title_card?>"></div>
                    </div>
                </td>
            </tr>
        </table>

        <table class="table table-content-card" border="0">
            <tr>
                <td width="50%" class="center" style="text-align: center;">
                   <br>Peserta <br><br><br><br><br> <?php echo $value->dp_nama_lengkap ?>
                </td>
                <td width="50%" class="center" style="text-align: center;">
                   Jakarta, <?php echo $this->tanggal->formatDate(date('Y-m-d'))?><br>
                   <b>Kepala Biro Kepegawaian, Organisasi dan Tata Kelola</b> <br><br><br><br><br> Ir. Asep Arofah Permana, M.M.,M.T<br>NIP.19602308260001
                </td>
            </tr>
        </table>
    </div>

    <hr class="doted">
    <br><br>
    <?php endfor;?>

</body>

</html>
