<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url().'assets/barcode-master/prototype/sample/prototype.js'?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/barcode-master/prototype/prototype-barcode.js'?>" type="text/javascript"></script>
<script type="text/javascript">

window.onload = generateBarcode;

  function generateBarcode(){
    $("barcodeTarget").update();
    var value = "30211131030004";
    var btype = "code128";
    
    var settings = {
      output:"css",
      bgColor: "#FFFFFF",
      color: "#000000",
      barWidth: 1.5,
      barHeight: 100,
      moduleSize: 5,
      posX: 10,
      posY: 20,
      addQuietZone: false
    };

    $("barcodeTarget").update().show().barcode(value, btype, settings);

  }
    
</script> 
<style type="text/css">
    .sub-title{
        margin-top: -30px !important;
        font-size: 11px;
    }
    .content-card{
        font-size: 11px !important;
        width : 100%;
    }
    .table-content-card{
        font-size: 12px;
    }
    .table{
        margin-bottom: 0px !important;
    }
    .img-logo-card{
        margin: 0px 0px 0px 0px !important;
    }
</style>
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'data_import'?>">HASIL AKHIR SELEKSI CPNS</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Inforamasi Akhir Seleksi CPNS Kementerian PUPR Tahun 2018) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">

                <br>

                    <div class="content-card">
                        <center><h4 style="font-size:16px;line-height:20px;font-weight:bold">Hasil Akhir Seleksi Pengadaan CPNS<br> Kementerian PUPR Tahun Anggaran 2018</h4></center>
                        <table class="table table-content-card" border="0">
                            <tr>
                                <td width="150px"> <img src="<?php echo base_url().'assets/contoh_pas_foto.jpg'?>" width="150px"> </td>
                                <td width="500px">
                                    No. Peserta / NIK <br>
                                    <b>3021-113-1030004 / 3603122311900004</b><br>
                                    Nama (JK) <br>
                                    Muhammad Amin Lubis (L)<br>
                                    Formasi <br>
                                    <b>(UMUM) Tekhnik Pengairan Ahli Pratama<br>S.1/D4 Tekhnik Geodesi</b>
                                    <br>
                                    Jadwal Ujian<br>
                                    <b>Jakarta, 23 September 2018 - Sesi 5 - No.Urut 4</b><br>
                                    Alamat : Jl. Kerinci Blok E 36 No 10 Pondok Indah Kuta Bumi Tangerang
                                    <br><br>
                                </td>
                                <td align="center">
                                    <img src="<?php echo base_url().'assets/medali.jpg'?>" width="100px"><br>
                                    <h3 style="color:green">CONGRATULATION</h3>
                                    <p style="font-size:16px;margin-top:-20px">Selamat Anda Dinyatakan <b>Lulus</b><br> Seleksi Pengadaan CPNS Kementerian PUPR Tahun 2018</p>
                                    

                                </td>
                            </tr>
                        </table>

                        <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Hasil Ujian SKD/SKB</h4></div>

                        <table class="table table-content-card" border="0">
                            <tr>
                                <td width="30%">
                                   <table class="table">
                                       <tr>
                                           <td colspan="2"><b>I. SELEKSI KOMPETENSI DASAR (SKD)</b></td>
                                       </tr>
                                       <tr>
                                           <td width="250px">&nbsp;&nbsp;&nbsp; a. TWK</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="250px">&nbsp;&nbsp;&nbsp; a. TIU</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="250px">&nbsp;&nbsp;&nbsp; a. TKP</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="250px">TOTAL NILAI SKD</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="250px">NILAI SKD SKALA 100</td>
                                           <td>: 110</td>
                                       </tr>
                                       
                                       
                                    </table>
                                </td>
                                <td width="30%">
                                   <table class="table">
                                       <tr>
                                           <td colspan="2"><b>II. SELEKSI KOMPETENSI BIDANG (SKB)</b></td>
                                       </tr>
                                       <tr>
                                           <td width="200px">&nbsp;&nbsp;&nbsp; a. NILAI SKB CAT</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI SKB CAT SKALA 100</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI BOBOT (70%)</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="230px">&nbsp;&nbsp;&nbsp; b. NILAI PSIKOTES LANJUTAN</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI PSIKOTES SKALA 100</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI BOBOT (30%)</td>
                                           <td>: 110</td>
                                       </tr>
                                       
                                    </table>
                                </td>
                                <td width="40%">
                                    <table class="table">
                                       <tr>
                                           <td colspan="2"><b>III. NILAI AKHIR</b></td>
                                       </tr>
                                       <tr>
                                           <td width="230px">NILAI BOBOT SKD (40%)</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="230px">NILAI BOBOT SKB (60%)</td>
                                           <td>: 110</td>
                                       </tr>
                                       <tr>
                                           <td width="230px" colspan="2" class="center"><b>HASIL NILAI AKHIR</b><br><h2>596</h2></td>
                                       </tr>
                                       
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

