<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>DATA LENGKAP PELAMAR</strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul View Data Lengkap Pelamar) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- add btn -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="row" style="background-color: white;padding-top:10px">

                            <div class="row" style="padding-left:30px; padding-right:30px">

                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#informasi_umum_tabs">Informasi Umum</a></li>
                                <li><a data-toggle="tab" href="#data_umum_tabs">Biodata Diri</a></li>
                                <li><a data-toggle="tab" href="#hasil_verifikasi_tabs">Hasil Verifikasi</a></li>
                                <?php if( in_array($value->verifikasi_status, array(3,4))) : ?>
                                <li><a data-toggle="tab" href="#cetak_kartu_skd_tabs">Cetak Kartu SKD</a></li>
                                
                                <?php if($value->verifikasi_ujian_skd=='L') :?>
                                      <li><a data-toggle="tab" href="#cetak_kartu_skb_tabs">Cetak Kartu SKB</a></li>
                                <?php endif;?>

                                <?php endif;?>

                              </ul>

                              <div class="tab-content">

                                <div id="informasi_umum_tabs" class="tab-pane fade in active">
                                  <strong><h4>INFORMASI UMUM</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Informasi Umum) </i></small>
                                  </div>
                                  <br>
                                  <?php echo isset($profile)?$profile->informasi_umum:'';?>
                                </div>

                                <div id="data_umum_tabs" class="tab-pane fade">
                                  <strong><h4>DATA LENGKAP</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Data Lengkap Pelamar) </i></small>
                                    </div>
                                  <p>
                                    <!-- content data table -->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-user"></i> Biodata Diri</h4></div>
                                                <br>
                                                <!-- end block title -->

                                                <!-- foto profile -->
                                                <div class="col-md-2">
                                                    <img src="<?php echo isset($file[2])?base_url().'uploaded_files/data_pelamar/'.$file[2].'':base_url().'assets/admin/images/no_photo.jpg'?>" class="img-responsive" alt="" style="width: 100%" /><br>
                                                </div>
                                                <!-- end foto profile -->

                                                <!-- block biodata diri -->
                                                <div class="col-md-5">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Nama Lengkap</th>
                                                            <td><?php echo $value->dp_nama_lengkap?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">No KK</th>
                                                            <td><?php echo $value->dp_no_kk?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Jenis Kelamin</th>
                                                            <td><?php echo $value->jk?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Status Nikah</th>
                                                            <td><?php echo $value->nama_status_kawin?></td>
                                                        </tr>

                                                    </table>

                                                    <table width="300px">
                                                        <tr>
                                                            <td width="100px"> <a class="center" href="<?php echo isset($file[1])?base_url().'uploaded_files/data_pelamar/'.$file[1].'':'#'?>" target="_blank" style="color:red"><img src="<?php echo base_url().'assets/upload/file.png'?>" width="50px"><br>KTP</a></td>
                                                            <td width="100px"><a href="<?php echo isset($file[4])?base_url().'uploaded_files/data_pelamar/'.$file[4].'':'#'?>" target="_blank" style="color:red"><img src="<?php echo base_url().'assets/upload/file.png'?>" width="50px"><br>KK</a></td>
                                                            <td width="150px"><a href="<?php echo isset($file[3])?base_url().'uploaded_files/data_pelamar/'.$file[3].'':'#'?>" target="_blank" style="color:red"><img src="<?php echo base_url().'assets/upload/file.png'?>" width="50px"><br>Surat Pernyataan</a></td>
                                                        </tr>
                                                    </table>

                                                </div>
                                                
                                                <div class="col-md-5">

                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">NIK</th>
                                                            <td><?php echo $value->dp_nik?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Tempat Lahir</th>
                                                            <td><?php echo $value->dp_tempat_lahir?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Lahir</th>
                                                            <td><?php echo $this->tanggal->formatDate($value->dp_tanggal_lahir)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Agama</th>
                                                            <td><?php echo $value->nama_agama?></td>
                                                        </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-info"></i> Kontak Informasi</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">

                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Alamat Lengkap</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Alamat Domisili</th>
                                                        <th style="background-color:#f1ab0de0;color:black">No Telp</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Email</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $value->dp_alamat_lengkap?></td>
                                                        <td><?php echo $value->dp_alamat_org_tua?></td>
                                                        <td><?php echo $value->dp_no_telp?></td>
                                                        <td><?php echo $value->dp_email?></td>
                                                    </tr>
                                                </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-list"></i> Formasi</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">

                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Kualifikasi Pendidikan</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Jenis Formasi</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Jabatan</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Penempatan</th>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo $value->kp_name?></td>
                                                            <td><?php echo $value->formasi_jenis_name?></td>
                                                            <td><?php echo $value->fj_name?></td>
                                                            <td><?php echo $value->up_name?></td>
                                                        </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-graduation-cap"></i> Pendidikan</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">

                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Universitas</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Jurusan</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Ijasah</th>
                                                            <th style="background-color:#f1ab0de0;color:black">IPK (Grade)</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Nomor Ijasah</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Gelar Sarjana</th>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo $value->univ_name?></td>
                                                            <td><?php echo $value->majors_name?></td>
                                                            <td><?php echo $this->tanggal->formatDate($value->pend_tgl_ijasah)?></td>
                                                            <td><?php echo $value->pend_ipk?></td>
                                                            <td><?php echo $value->pend_no_ijasah?></td>
                                                            <td><?php echo $value->pend_gelar_sarjana?></td>
                                                        </tr>
                                                    </table>
                                                    <table width="160px">
                                                        <td width="80px">
                                                            <a href="<?php echo isset($file[5])?base_url().'uploaded_files/data_pelamar/'.$file[5].'':'#'?>" target="_blank" style="color:red"><img src="<?php echo base_url().'assets/upload/file.png'?>" width="50px"><br>Ijasah</a>
                                                        </td>
                                                        <td width="80px">
                                                            <a href="<?php echo isset($file[6])?base_url().'uploaded_files/data_pelamar/'.$file[6].'':'#'?>" target="_blank" style="color:red"><img src="<?php echo base_url().'assets/upload/file.png'?>" width="50px"><br>Transkrip</a>
                                                        </td>
                                                    </table>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Kemampuan Bahasa Inggris</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Jenis TOEFL</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Sertifikat</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Lokasi Tes TOEFL</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Nilai TOEFL</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Nomor Sertifikat</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Tes</th>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo $value->jt_name?></td>
                                                            <td><?php echo $this->tanggal->formatDate($value->bhs_tanggal_sertifikat)?></td>
                                                            <td><?php echo $value->bhs_lokasi_tes?></td>
                                                            <td><?php echo $value->bhs_nilai_toefl?></td>
                                                            <td><?php echo $value->bhs_no_sertifikat?></td>
                                                            <td><?php echo $this->tanggal->formatDate($value->bhs_tgl_tes_toefl)?></td>
                                                        </tr>
                                                    </table>

                                                    <table width="80px">
                                                        <td width="80px">
                                                            <a href="<?php echo isset($file[7])?base_url().'uploaded_files/data_pelamar/'.$file[7].'':'#'?>" target="_blank" style="color:red"><img src="<?php echo base_url().'assets/upload/file.png'?>" width="50px"><br>TOEFL</a>
                                                        </td>
                                                    </table>


                                                </div>

                                            </div>
                                        </div>
                                        <br>


                                    <!-- end content data table -->
                                  </p>
                                </div>

                                <div id="hasil_verifikasi_tabs" class="tab-pane fade">
                                  <strong><h4>HASIL VERIFIKASI</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Hasil Verifikasi Data Pelamar) </i></small>
                                  </div>

                                  <p>
                                    <?php 
                                        $ver_ipk = $this->konfigurasi_model->verifikasi_ipk($value->pend_ipk);
                                         $ipk = ( $ver_ipk )?'<div class="alert alert-success">
                                                    <b style="color:green">Memenuhi Syarat </b>
                                                    Nilai IPK '.$value->pend_ipk.' (Memenuhi nilai minimum)<br>
                                                </div>':'<div class="alert alert-danger">
                                                <b style="color:red">Tidak Memenuhi Syarat </b>
                                                Nilai IPK '.$value->pend_ipk.' (Kurang dari nilai minimum)<br>
                                            </div>';

                                    echo $ipk?>
                                    <?php 
                                        $ver_toefl_2 = $this->konfigurasi_model->verifikasi_toefl($value->bhs_nilai_toefl);
                                         $show_toefl = ( $ver_toefl_2 )?'<div class="alert alert-success">
                                                    <b style="color:green">Memenuhi Syarat </b>
                                                    Nilai TOEFL '.$value->bhs_nilai_toefl.' (Memenuhi nilai minimum)<br>
                                                </div>':'<div class="alert alert-danger">
                                                <b style="color:red">Tidak Memenuhi Syarat </b>
                                                Nilai TOEFL '.$value->bhs_nilai_toefl.' (Kurang dari nilai minimum)<br>
                                            </div>';

                                    echo $show_toefl?>

                                  </p>
                                </div>

                                <div id="cetak_kartu_skd_tabs" class="tab-pane fade">
                                  <strong><h4>CETAK KARTU SKD</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Silahkan Cetak Kartu Ujian Anda Disini) </i></small>
                                  </div>
                                  <br>
                                  Silahkan download kartu peserta ujian SKD anda disini <i><a href="<?php echo base_url().'data_import/cetak_kartu_peserta/skd/'.$value->dp_nik.''?>" target="_blank" style="color:red" >Download</a></i>
                                </div>

                                <div id="cetak_kartu_skb_tabs" class="tab-pane fade">
                                  <strong><h4>CETAK KARTU SKB</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Silahkan Cetak Kartu Ujian Anda Disini) </i></small>
                                  </div>
                                  <br>
                                  Silahkan download kartu peserta ujian SKB anda disini <i><a href="<?php echo base_url().'data_import/cetak_kartu_peserta/skb/'.$value->dp_nik.''?>" target="_blank" style="color:red" >Download</a></i>

                                </div>


                              </div>

                            </div>

                            <br>
                            


                               
                        </div>

                    </div>
                    
                </div>

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- End Main -->


