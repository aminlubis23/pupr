<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_upload_data_bkn" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        // Attach file
        formData.append('file', $('input[type=file]').files[0]); 
        
        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  
        },
        uploadProgress: function(event, position, total, percentComplete) {
        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            reload_table();
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();
        }
        });
        event.preventDefault();
    });
});

</script>

<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>IMPORT DATA CPNS </strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul Import Data CPNS Dari BKN) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- content data table -->
                <?php if(in_array($this->session->userdata('user')->level, array('0'))) :?>
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" id="form_upload_data_bkn" action="<?php echo base_url().'data_import/process_import'?>" method="POST" enctype="multipart/form-data">

                                <div class="panel-group" id="accordion">

                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion" href="#5_coll"> <i class="fa fa-download"></i> FORM IMPORT DATA BKN <i class="fa fa-angle-double-right"></i> <small style="color:white; font-style: italic">Silahkan upload data BKN dan import</small></a>
                                        </h4>
                                      </div>
                                      <div id="5_coll" class="panel-collapse collapse">
                                        <div class="panel-body">

                                            <div class="form-group">
                                              <label class="control-label col-md-2">Browse File Excel</label>
                                              <div class="col-md-3">
                                                <input type="file" name="file" class="form-control">
                                              </div>
                                            </div>

                                            <!-- <div class="form-group">
                                              <label class="control-label col-md-2">Fungsi Aksi</label>
                                              <div class="col-md-3">
                                                <select name="action" class="form-control">
                                                    <option value="">-Silahkan Pilih-</option>
                                                    <option value="ido">Insert Data Only</option>
                                                    <option value="udo">Update Data Only</option>
                                                    <option value="iud">Insert and Update Data</option>
                                                </select>
                                              </div>
                                            </div> -->

                                            <div class="form-group">
                                              <label class="control-label col-md-2">&nbsp;</label>
                                              <div class="col-md-10">
                                                <button type="submit" id="btn_submit" class="mb-xs mt-xs mr-xs btn btn-sm btn-success"><i class="fa fa-download"></i> Import to Database </button>
                                              </div>
                                            </div>

                                            


                                        </div>
                                      </div>
                                    </div>
                                
                                </div> 

                            </form>    
                        </div>
                        
                    </div>
                <?php endif;?>

                <div class="col-md-12">

                    <table id="dynamic-table" base-url="data_import" class="table table-striped table-bordered" >
                        <thead>
                        <tr>
                            <th style="color: #242424; text-align: center; width:30px; ">No</th>
                             <th width="50px">&nbsp;</th>
                            <th width="50px">&nbsp;</th>
                            <th></th>
                            <th style="color: #242424;width:150px">NIK</th>
                            <th style="color: #242424;width:150px">Nama</th>
                            <th style="color: #242424;width:150px">Email</th>
                            <th style="color: #242424;width:150px">No Telp</th>
                            <th style="color: #242424;width:100%">Alamat KTP</th>
                            <!-- <th style="color: #242424; text-align: center; width:150px">Action</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>js/als_datatable_with_detail.js"></script>
<!-- End Main -->