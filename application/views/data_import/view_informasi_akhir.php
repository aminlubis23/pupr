<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>DATA LENGKAP PELAMAR</strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul View Data Lengkap Pelamar) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- add btn -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="row" style="background-color: white;padding-top:10px">

                            <div class="row" style="padding-left:30px; padding-right:30px">

                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#informasi_umum_tabs">Pengumuman Akhir</a></li>
                                <li><a data-toggle="tab" href="#data_umum_tabs">Biodata Diri</a></li>
                              </ul>

                              <div class="tab-content">

                                <div id="informasi_umum_tabs" class="tab-pane fade in active">
                                  <strong><h4>PENGUMUMAN AKHIR</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Pengumuman Akhir Proses Rekrutmen Kementerian PUPR) </i></small>
                                  </div>
                                  <br>
                                  <div class="content-card">
                                    <center><h4 style="font-size:16px;line-height:20px;font-weight:bold">Hasil Akhir Seleksi Pengadaan CPNS<br> Kementerian PUPR Tahun Anggaran 2018</h4></center>
                                    <table class="table table-content-card" border="0">
                                        <tr>
                                            <td width="150px"> <img src="<?php echo base_url().'assets/contoh_pas_foto.jpg'?>" width="150px"> </td>
                                            <td width="500px">
                                                No. Peserta / NIK <br>
                                                <b style="font-size:16px"><?php echo $value->dp_no_peserta?> / <?php echo $value->dp_nik?></b><br>
                                                Nama (JK) <br>
                                                <b><?php echo $value->dp_nama_lengkap?></b> (<?php echo $value->jk?>)<br>
                                                Formasi <br>
                                                <b>(<?php echo $value->formasi_jenis_name?>) <?php echo $value->fj_name?><br><?php echo $value->kp_name?></b>
                                                <br>
                                                Tempat, Tanggal Lahir<br>
                                                <b><?php echo $value->dp_tempat_lahir?>, <?php echo $this->tanggal->formatDate($value->dp_tanggal_lahir)?></b><br>
                                                Alamat : <?php echo $value->dp_alamat_lengkap?>
                                                <br><br>
                                            </td>
                                            <td align="center">

                                              <?php 
                                                if(isset($nilai->status_akhir)) :
                                                  if ($nilai->status_akhir=='L') :
                                              ?>
                                                <img src="<?php echo base_url().'assets/medali.jpg'?>" width="100px"><br>
                                                <h3 style="color:green">CONGRATULATION</h3>
                                                <p style="font-size:16px;margin-top:-20px">Selamat Anda Dinyatakan <b>Lulus</b><br> Seleksi Pengadaan CPNS Kementerian PUPR Tahun 2018</p>
                                                <?php else: ?>
                                                  <img src="<?php echo base_url().'assets/sory.jpg'?>" width="250px"><br>
                                                  <!-- <h3 style="color:green">SORRY</h3> -->
                                                  <p style="font-size:16px;margin-top:-20px">Maaf Anda Dinyatakan <b>Tidak Lulus</b><br> Seleksi Pengadaan CPNS Kementerian PUPR Tahun 2018</p>
                                                <?php endif; ?>

                                              <?php else : echo 'Belum ada Nilai';endif;?>
                                                

                                            </td>
                                        </tr>
                                    </table>

                                    <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Hasil Ujian SKD/SKB</h4></div>

                                    <table class="table table-content-card" border="0">
                                        <tr>
                                            <td width="30%">
                                               <table class="table">
                                                   <tr>
                                                       <td colspan="2"><b>I. SELEKSI KOMPETENSI DASAR (SKD)</b></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="250px">&nbsp;&nbsp;&nbsp; a. TWK</td>
                                                       <td>: <?php echo isset($nilai->twk)?$nilai->twk:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="250px">&nbsp;&nbsp;&nbsp; a. TIU</td>
                                                       <td>: <?php echo isset($nilai->tiu)?$nilai->tiu:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="250px">&nbsp;&nbsp;&nbsp; a. TKP</td>
                                                       <td>: <?php echo isset($nilai->tkp)?$nilai->tkp:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="250px">TOTAL NILAI SKD</td>
                                                       <td>: <?php echo isset($nilai->total_skd)?$nilai->total_skd:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="250px">NILAI SKD SKALA 100</td>
                                                       <td>: <?php echo isset($nilai->total_skd_skala)?$nilai->total_skd_skala:0?></td>
                                                   </tr>
                                                   
                                                   
                                                </table>
                                            </td>
                                            <td width="30%">
                                               <table class="table">
                                                   <tr>
                                                       <td colspan="2"><b>II. SELEKSI KOMPETENSI BIDANG (SKB)</b></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="200px">&nbsp;&nbsp;&nbsp; a. NILAI SKB CAT</td>
                                                       <td>: <?php echo isset($nilai->cat)?$nilai->cat:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI SKB CAT SKALA 100</td>
                                                       <td>: <?php echo isset($nilai->total_cat_skala)?$nilai->total_cat_skala:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI BOBOT (70%)</td>
                                                       <td>: <?php echo isset($nilai->total_cat_bobot)?$nilai->total_cat_bobot:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="230px">&nbsp;&nbsp;&nbsp; b. NILAI PSIKOTES LANJUTAN</td>
                                                       <td>: <?php echo isset($nilai->psikotes)?$nilai->psikotes:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI PSIKOTES SKALA 100</td>
                                                       <td>: <?php echo isset($nilai->total_psikotes_skala)?$nilai->total_psikotes_skala:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NILAI BOBOT (30%)</td>
                                                       <td>: <?php echo isset($nilai->total_psikotes_bobot)?$nilai->total_psikotes_bobot:0?></td>
                                                   </tr>
                                                   
                                                </table>
                                            </td>
                                            <td width="40%">
                                                <table class="table">
                                                   <tr>
                                                       <td colspan="2"><b>III. NILAI AKHIR</b></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="230px">NILAI BOBOT SKD (40%)</td>
                                                       <td>: <?php echo isset($nilai->total_bobot_skd)?$nilai->total_bobot_skd:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="230px">NILAI BOBOT SKB (60%)</td>
                                                       <td>: <?php echo isset($nilai->total_bobot_skb)?$nilai->total_bobot_skb:0?></td>
                                                   </tr>
                                                   <tr>
                                                       <td width="230px" colspan="2" class="center"><b>HASIL NILAI AKHIR</b><br><h2><?php echo isset($nilai->total_nilai_akhir)?$nilai->total_nilai_akhir:0?></h2></td>
                                                   </tr>
                                                   
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <center style="margin-top:-30px !important">
                                        Selanjutnya Saudara/Saudari diharuskan melakukan pemberkasan usulan penetapan Nomor Induk Pegawai (NIP) <br> mulai tanggal <b>15 Agustus 2018</b> di <b>Ruang Pendopo Kementerian Pekerjaan Umum dan Perumahan Rakyat, <br>Jl Pattimura No 20, Kebayoran Baru, Jakarta Selatan.</b><br><br><a href="#" class="btn btn-sm btn-primary">Download Format Kelengkapan Berkas<br>Usulan Penetapan NIP</a><br><br>
                                        <a href="#" class="btn btn-sm btn-primary">Cetak Checklist Dokumen Pemberkasan</a><br><br>
                                        Pelamar yang ingin mengundurkan diri wajib menyerahkan Surat Pengunduran Diri<br> kepada Panitia Pengadaan CPNS Kementerian PUPR<br><br>
                                        <a href="#" class="btn btn-sm btn-danger">Download Format Surat Pengunduran Diri</a> <br><br>

                                        <h4>Hasil Integrasi Nilai SKD dan SKB Seleksi CPNS Tahun 2018</h4>
                                        <iframe src="<?php echo base_url().'uploaded_files/nilai.pdf'?>" width="100%" height="1000px"></iframe>
                                    </center>
                                    </div>
                                </div>

                                <div id="data_umum_tabs" class="tab-pane fade">
                                  <strong><h4>DATA LENGKAP</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Data Lengkap Pelamar) </i></small>
                                    </div>
                                  <p>
                                    <!-- content data table -->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-user"></i> Biodata Diri</h4></div>
                                                <br>
                                                <!-- end block title -->

                                                <!-- foto profile -->
                                                <div class="col-md-2">
                                                    <img src="<?php echo isset($file[2])?base_url().'uploaded_files/data_pelamar/'.$file[2].'':base_url().'assets/admin/images/no_photo.jpg'?>" class="img-responsive" alt="" style="width: 100%" /><br>
                                                </div>
                                                <!-- end foto profile -->

                                                <!-- block biodata diri -->
                                                <div class="col-md-5">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Nama Lengkap</th>
                                                            <td><?php echo $value->dp_nama_lengkap?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">No KK</th>
                                                            <td><?php echo $value->dp_no_kk?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Jenis Kelamin</th>
                                                            <td><?php echo $value->jk?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Status Nikah</th>
                                                            <td><?php echo $value->nama_status_kawin?></td>
                                                        </tr>

                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Lampiran KTP</th>
                                                            <td><a href="<?php echo isset($file[1])?base_url().'uploaded_files/data_pelamar/'.$file[1].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Lampiran KK</th>
                                                            <td><a href="<?php echo isset($file[4])?base_url().'uploaded_files/data_pelamar/'.$file[4].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                                        </tr>

                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Surat Pernyataan</th>
                                                            <td><a href="<?php echo isset($file[3])?base_url().'uploaded_files/data_pelamar/'.$file[3].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                                        </tr>

                                                    </table>

                                                </div>
                                                
                                                <div class="col-md-5">

                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">NIK</th>
                                                            <td><?php echo $value->dp_nik?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Tempat Lahir</th>
                                                            <td><?php echo $value->dp_tempat_lahir?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Lahir</th>
                                                            <td><?php echo $this->tanggal->formatDate($value->dp_tanggal_lahir)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Agama</th>
                                                            <td><?php echo $value->nama_agama?></td>
                                                        </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-info"></i> Kontak Informasi</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">

                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Alamat Lengkap</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Alamat Domisili</th>
                                                        <th style="background-color:#f1ab0de0;color:black">No Telp</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Email</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $value->dp_alamat_lengkap?></td>
                                                        <td><?php echo $value->dp_alamat_org_tua?></td>
                                                        <td><?php echo $value->dp_no_telp?></td>
                                                        <td><?php echo $value->dp_email?></td>
                                                    </tr>
                                                </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-list"></i> Formasi</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">

                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Kode Formasi</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Kualifikasi Pendidikan</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Jenis Formasi</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Jabatan</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Penempatan</th>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo $value->formasi_kode?></td>
                                                            <td><?php echo $value->kp_name?></td>
                                                            <td><?php echo $value->formasi_jenis_name?></td>
                                                            <td><?php echo $value->fj_name?></td>
                                                            <td><?php echo $value->up_name?></td>
                                                        </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-graduation-cap"></i> Pendidikan</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">

                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Universitas</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Jurusan</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Ijasah</th>
                                                            <th style="background-color:#f1ab0de0;color:black">IPK (Grade)</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Nomor Ijasah</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Gelar Sarjana</th>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo $value->univ_name?></td>
                                                            <td><?php echo $value->majors_name?></td>
                                                            <td><?php echo $this->tanggal->formatDate($value->pend_tgl_ijasah)?></td>
                                                            <td><?php echo $value->pend_ipk?></td>
                                                            <td><?php echo $value->pend_no_ijasah?></td>
                                                            <td><?php echo $value->pend_gelar_sarjana?></td>
                                                        </tr>
                                                    </table>

                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- block title -->
                                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Kemampuan Bahasa Inggris</h4></div>
                                                <br>
                                                <!-- end block title -->
                                                <div class="col-md-12">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="background-color:#f1ab0de0;color:black">Jenis TOEFL</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Sertifikat</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Lokasi Tes TOEFL</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Nilai TOEFL</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Nomor Sertifikat</th>
                                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Tes</th>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo $value->bhs_jenis_toefl?></td>
                                                            <td><?php echo $this->tanggal->formatDate($value->bhs_tanggal_sertifikat)?></td>
                                                            <td><?php echo $value->bhs_lokasi_tes?></td>
                                                            <td><?php echo $value->bhs_nilai_toefl?></td>
                                                            <td><?php echo $value->bhs_no_sertifikat?></td>
                                                            <td><?php echo $this->tanggal->formatDate($value->bhs_tgl_tes_toefl)?></td>
                                                        </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                        <br>


                                    <!-- end content data table -->
                                  </p>
                                </div>

                              </div>

                            </div>

                            <br>
                            


                               
                        </div>

                    </div>
                    
                </div>

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- End Main -->


