<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>STATISTIK DATA CPNS </strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul Statistik Data CPNS) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- add btn -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="row" style="background-color: white;padding-top:10px">
                            <div class="container">
                                <div class="counters">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-primary">
                                            <strong id="valuepelamar"><?php echo $count['total_all']?></strong>
                                            <label class="title-counter">Total Data Peserta<br>Hasil Import Data Dari BKN</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-secondary">
                                            <strong id="valueadm"><?php echo $count['ttl_pelamar']?></strong>
                                            <label class="title-counter">Total Pelamar Yang Sudah <br>Melengkapi Data Pada Aplikasi Kementerian</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-tertiary">
                                            <strong id="valueSKD"><?php echo $count['memenuhi_syarat']?></strong>
                                            <label class="title-counter">Total Pelamar Yang Telah<br> Memenuhi Syarat</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-quaternary">
                                            <strong id="valueSKB"><?php echo $count['lulus']?></strong>
                                            <label class="title-counter">Total Pelamar Yang Lulus<br>Pada Seleksi Akhir</label>
                                        </div>
                                    </div>
                                </div>
                                <hr class="tall" />
                            </div>

                            <div class="row" style="padding-left:30px; padding-right:30px">
                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#statistik_pendaftaran_tabs">Data Umum</a></li>
                                <li><a data-toggle="tab" href="#persentase_tabs">Persentase Data Pelamar</a></li>
                                <li><a data-toggle="tab" href="#persentase_hasil_verif_tabs">Grafik Hasil Verifikasi</a></li>
                              </ul>

                              <div class="tab-content">

                                <div id="statistik_pendaftaran_tabs" class="tab-pane fade in active">
                                  <strong><h4>STATISTIK PENDAFTARAN</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Statistik Pendaftaran CPNS Tahun 2018) </i></small>
                                    </div>
                                  <p>
                                    <!-- content data table -->
                                    <table id="dynamic-table-2" base-url="verifikasi_data_cpns" class="table table-striped table-bordered" >
                                            <thead>
                                            <tr>
                                                <th valign="middle" rowspan="2" style="color: #242424; text-align: center; width:30px; ">No</th>
                                                <th class="center" rowspan="2">NAMA JABATAN / FORMASI PENDIDIKAN</th>
                                                <!-- <th class="center" rowspan="2">JUMLAH FORMASI JABATAN</th> -->
                                                <!-- <th class="center" rowspan="2">KUALIFIKASI PENDIDIKAN</th> -->
                                                <th class="center" rowspan="2">JUMLAH FORMASI</th>
                                                <th class="center" colspan="<?php echo count($jenis_formasi)?>">JENIS FORMASI</th>
                                                <th rowspan="2" class="center">UNIT ORG PENEMPATAN</th>
                                            </tr>
                                            <tr>
                                                <?php 
                                                    foreach($jenis_formasi as $row){
                                                        echo '<th class="center" width="80px">'.$row->formasi_jenis_name.'</th>';
                                                }?>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $alphabet='A'; foreach($data as $key=>$val) : ?>

                                                <tr>
                                                    <td class="center"><?php echo $alphabet?></td>
                                                    <td  colspan="9"><b><?php echo $key?></b></td>
                                                </tr>

                                                <?php $no=0; foreach($val as $key_val=>$row_val) : $no++; 
                                                      $rowspan = count($val[$key_val]);
                                                ?>

                                                    <tr>
                                                        <td class="center"><?php echo $no?></td>
                                                        <td colspan="9"><?php echo $key_val?></td>
                                                    </tr>
                                                    <?php 
                                                      foreach($val[$key_val] as $key_vals=>$row_vals) : 
                                                        $arr_jmf[$key_val][] = $row_vals['kp_jumlah_formasi'];
                                                    ?>
                                                    <tr>
                                                        <td></td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - <?php echo $row_vals['kp_name']?></td>
                                                        <td class="center"><?php echo $row_vals['kp_jumlah_formasi']?></td>
                                                        <?php 
                                                            foreach($jenis_formasi as $row_jf){
                                                                $total_data = isset( $row_vals['statistik_data'][$row_vals['kfj_id']][$row_vals['fj_id']][$row_vals['kp_id']][$row_jf->formasi_jenis_id] ) ? $row_vals['statistik_data'][$row_vals['kfj_id']][$row_vals['fj_id']][$row_vals['kp_id']][$row_jf->formasi_jenis_id] : 0;

                                                                $arr_total_data[$row_vals['kfj_id']][$row_vals['fj_id']][$row_vals['kp_id']][$row_jf->formasi_jenis_id][] = $total_data;
                                                                
                                                                echo '<td class="center">'.$total_data.' </td>';
                                                        }?>
                                                        <td><?php echo $row_vals['penempatan']?></td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                     <tr>
                                                      <td></td>
                                                      <td align="center">TOTAL</td>
                                                      <td class="center"><b style="font-size:14px"><?php echo array_sum($arr_jmf[$key_val])?></b></td>
                                                      <?php 
                                                            foreach($jenis_formasi as $row_jf){
                                                                echo '<td class="center" style="font-size:14px"><b>'.array_sum($arr_total_data[$row_vals['kfj_id']][$row_vals['fj_id']][$row_vals['kp_id']][$row_jf->formasi_jenis_id]).'</b></td>';
                                                        }?>
                                                      <td></td>
                                                    </tr>

                                                    

                                                <?php endforeach;?>

                                            <?php $alphabet++; endforeach;?>
                                            </tbody>
                                        </table>
                                    <!-- end content data table -->
                                  </p>
                                </div>

                                <div id="persentase_tabs" class="tab-pane fade">
                                  <strong><h4>PERSENTASE DATA PELAMAR</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Persentase Data Pelamar Dengan Diagram Pie) </i></small>
                                  </div>

                                  <p>
                                    <div class="row">
                                        <div class="col-md-12">
                                           <div id="graifk_by_kualifikasi_pendidikan" style="min-width: 100%; height: 500px; max-width: 600px; margin: 0 auto"></div>
                                        </div>

                                        <div class="col-md-6">
                                           <div id="grafik_by_jk" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>

                                        <div class="col-md-6">
                                           <div id="grafik_by_formasi" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>
                                        <hr>
                                        <div class="col-md-6">
                                           <div id="grafik_by_status_marital" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>
                                        <div class="col-md-6">
                                           <div id="grafik_by_agama" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>
                                    </div>

                                  </p>
                                </div>

                                <div id="persentase_hasil_verif_tabs" class="tab-pane fade">
                                  <strong><h4>GRAFIK HASIL VERIFIKASI</h4></strong>
                                  <div style="margin-top:-15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i>
                                    <small> <i>(Persentase Hasil Verifikasi Data Pelamar) </i></small>
                                  </div>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <div id="grafik_by_status_verifikasi" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>

                                        <div class="col-md-6">
                                            <div id="grafik_by_lulus_ipk" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>

                                        <div class="col-md-6">
                                            <div id="grafik_by_lulus_toefl" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                        </div>

                                    </div>

                                </div>

                              </div>
                            </div>
                            <br>                            

                        </div>

                    </div>
                    
                </div>

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- End Main -->

<script type="text/javascript">

    // Build the chart
    Highcharts.chart('grafik_by_jk', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Jenis Kelamin'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                    <?php
                        foreach ($grafik['graph_by_jk'] as $kbj => $vbj) {
                            echo '{ name: '."'".$vbj->gender_name."'".', y: '.$vbj->total.' },';
                        }
                    ?>
                    
            ]
        }]
    });

    // Build the chart
    Highcharts.chart('grafik_by_formasi', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Jenis Formasi'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                     <?php
                        foreach ($grafik['graph_by_formasi'] as $kbf => $vbf) {
                            echo '{ name: '."'".$vbf->formasi_jenis_name."'".', y: '.$vbf->total.' },';
                        }
                    ?>
                  ]
        }]
    });

    Highcharts.chart('grafik_by_status_marital', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Status Perkawinan'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                <?php
                    foreach ($grafik['graph_by_status_marital'] as $kbsm => $vbsm) {
                        echo '{ name: '."'".$vbsm->ms_name."'".', y: '.$vbsm->total.' },';
                    }
                ?>
            ]
        }]
    });

    Highcharts.chart('grafik_by_agama', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Agama'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                <?php
                    foreach ($grafik['graph_by_religion'] as $kbr => $vbr) {
                        echo '{ name: '."'".$vbr->religion_name."'".', y: '.$vbr->total.' },';
                    }
                ?>

            ]
        }]
    });

    Highcharts.chart('grafik_by_status_verifikasi', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Hasil Verifikasi Data Pelamar<br>Berdasarkan Status Verifikasi'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                <?php
                    foreach ($grafik['graph_by_status_verifikasi'] as $kbsv => $vbsv) {
                        echo '{ name: '."'".$vbsv->sv_name."'".', y: '.$vbsv->total.' },';
                    }
                ?>

            ]
        }]
    });

    Highcharts.chart('graifk_by_kualifikasi_pendidikan', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Data Pelamar<br>Berdasarkan Kualifikasi Pendidikan'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -25,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Pelamar'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Total: <b>{point.y:.1f} pelamar</b>'
        },
        series: [{
            name: 'Total',
            data: [

                 <?php
                    foreach ($grafik['graph_by_formasi_pendidikan'] as $kbfp => $vbfp) {
                        echo '['."'".$vbfp->kp_name."'".', '.$vbfp->total.'],';
                    }
                ?>
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
            
        }]
    });

    Highcharts.chart('grafik_by_lulus_ipk', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Hasil Verifikasi Data Pelamar<br>Berdasarkan Nilai IPK '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                { name: 'Lulus', y: <?php echo $grafik['graph_by_ipk']['L']?> },
                { name: 'Tidak Lulus', y: <?php echo $grafik['graph_by_ipk']['TL']?> },

            ]
        }]
    });

    Highcharts.chart('grafik_by_lulus_toefl', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Hasil Verifikasi Data Pelamar<br>Berdasarkan Nilai TOEFL '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                { name: 'Lulus', y: <?php echo $grafik['graph_by_toefl']['L']?> },
                { name: 'Tidak Lulus', y: <?php echo $grafik['graph_by_toefl']['TL']?> },

            ]
        }]
    });



</script>

