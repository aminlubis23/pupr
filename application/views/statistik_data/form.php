<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_data_cpns" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        // Attach file
        var data_id = $("input[type='file']").attr(data_id);
        formData.append('file', $('input[type=file]').files[0]); 
        
        $.each($("input[type='file']")[0].files, function(i, file) {
            formData.append('file', file);
        });

        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  

        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            /*window.location.href = "<?php echo base_url().'verifikasi_data_cpns'?>";*/
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();

        }

        });

        event.preventDefault();
        });
});

</script>

<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'verifikasi_data_cpns'?>">VERIFIKASI DATA CPNS</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Verifikasi data dibawah ini) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

            <?php echo validation_errors(); ?>

                <!-- content data table -->
                <div class="col-md-12">
                    <form id="form_data_cpns" method="post" action="<?php echo base_url('verifikasi_data_cpns/process') ?>" enctype="multipart/form-data">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-user"></i> Biodata Diri</h4></div>
                                <br>
                                <!-- end block title -->

                                <!-- foto profile -->
                                <div class="col-md-2">
                                    <img src="<?php echo isset($file[2])?base_url().'uploaded_files/data_pelamar/'.$file[2].'':base_url().'assets/admin/images/no_photo.jpg'?>" class="img-responsive" alt="" style="width: 100%" /><br>
                                </div>
                                <!-- end foto profile -->

                                <!-- block biodata diri -->
                                <div class="col-md-5">
                                    <table class="table">
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Nama Lengkap</th>
                                            <td><?php echo $value->dp_nama_lengkap?></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">No KK</th>
                                            <td><?php echo $value->dp_no_kk?></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Jenis Kelamin</th>
                                            <td><?php echo $value->jk?></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Status Nikah</th>
                                            <td><?php echo $value->nama_status_kawin?></td>
                                        </tr>

                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Lampiran KTP</th>
                                            <td><a href="<?php echo isset($file[1])?base_url().'uploaded_files/data_pelamar/'.$file[1].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Lampiran KK</th>
                                            <td><a href="<?php echo isset($file[4])?base_url().'uploaded_files/data_pelamar/'.$file[4].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                        </tr>

                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Surat Pernyataan</th>
                                            <td><a href="<?php echo isset($file[3])?base_url().'uploaded_files/data_pelamar/'.$file[3].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                        </tr>

                                    </table>

                                </div>
                                
                                <div class="col-md-5">

                                    <table class="table">
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">NIK</th>
                                            <td><?php echo $value->dp_nik?></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Tempat Lahir</th>
                                            <td><?php echo $value->dp_tempat_lahir?></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Lahir</th>
                                            <td><?php echo $this->tanggal->formatDate($value->dp_tanggal_lahir)?></td>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Agama</th>
                                            <td><?php echo $value->nama_agama?></td>
                                        </tr>
                                    </table>

                                </div>

                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-info"></i> Kontak Informasi</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-12">

                                <table class="table">
                                    <tr>
                                        <th style="background-color:#f1ab0de0;color:black">Alamat Lengkap</th>
                                        <th style="background-color:#f1ab0de0;color:black">Alamat Orang Tua</th>
                                        <th style="background-color:#f1ab0de0;color:black">No Telp</th>
                                        <th style="background-color:#f1ab0de0;color:black">Email</th>
                                    </tr>
                                    <tr>
                                        <td><?php echo $value->dp_alamat_lengkap?></td>
                                        <td><?php echo $value->dp_alamat_org_tua?></td>
                                        <td><?php echo $value->dp_no_telp?></td>
                                        <td><?php echo $value->dp_email?></td>
                                    </tr>
                                </table>

                                </div>

                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-list"></i> Formasi</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-12">

                                    <table class="table">
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Kode Formasi</th>
                                            <th style="background-color:#f1ab0de0;color:black">Kualifikasi Pendidikan</th>
                                            <th style="background-color:#f1ab0de0;color:black">Jenis Formasi</th>
                                            <th style="background-color:#f1ab0de0;color:black">Jabatan</th>
                                            <th style="background-color:#f1ab0de0;color:black">Penempatan</th>
                                        </tr>
                                        <tr>
                                            <td><?php echo $value->formasi_kode?></td>
                                            <td><?php echo $value->kp_name?></td>
                                            <td><?php echo $value->formasi_jenis_name?></td>
                                            <td><?php echo $value->fj_name?></td>
                                            <td><?php echo $value->up_name?></td>
                                        </tr>
                                    </table>

                                </div>

                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-graduation-cap"></i> Pendidikan</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-12">

                                    <table class="table">
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Universitas</th>
                                            <th style="background-color:#f1ab0de0;color:black">Jurusan</th>
                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Ijasah</th>
                                            <th style="background-color:#f1ab0de0;color:black">IPK (Grade)</th>
                                            <th style="background-color:#f1ab0de0;color:black">Nomor Ijasah</th>
                                            <th style="background-color:#f1ab0de0;color:black">Gelar Sarjana</th>
                                        </tr>
                                        <tr>
                                            <td><?php echo $value->univ_name?></td>
                                            <td><?php echo $value->majors_name?></td>
                                            <td><?php echo $this->tanggal->formatDate($value->pend_tgl_ijasah)?></td>
                                            <td><?php echo $value->pend_ipk?></td>
                                            <td><?php echo $value->pend_no_ijasah?></td>
                                            <td><?php echo $value->pend_gelar_sarjana?></td>
                                        </tr>
                                    </table>

                                </div>
                                
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <!-- block title -->
                                <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Kemampuan Bahasa Inggris</h4></div>
                                <br>
                                <!-- end block title -->
                                <div class="col-md-12">
                                    <table class="table">
                                        <tr>
                                            <th style="background-color:#f1ab0de0;color:black">Jenis TOEFL</th>
                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Sertifikat</th>
                                            <th style="background-color:#f1ab0de0;color:black">Lokasi Tes TOEFL</th>
                                            <th style="background-color:#f1ab0de0;color:black">Nilai TOEFL</th>
                                            <th style="background-color:#f1ab0de0;color:black">Nomor Sertifikat</th>
                                            <th style="background-color:#f1ab0de0;color:black">Tanggal Tes</th>
                                        </tr>
                                        <tr>
                                            <td><?php echo $value->bhs_jenis_toefl?></td>
                                            <td><?php echo $this->tanggal->formatDate($value->bhs_tanggal_sertifikat)?></td>
                                            <td><?php echo $value->bhs_lokasi_tes?></td>
                                            <td><?php echo $value->bhs_nilai_toefl?></td>
                                            <td><?php echo $value->bhs_no_sertifikat?></td>
                                            <td><?php echo $this->tanggal->formatDate($value->bhs_tgl_tes_toefl)?></td>
                                        </tr>
                                    </table>

                                </div>

                            </div>
                        </div>
                        <br>
                        <!-- btn submit -->
                        <center>
                        <input type="hidden" name="dp_id" value="<?php echo $value->dp_id?>">
                        <a href="<?php echo base_url().'verifikasi_data_cpns'?>" type="button" class="btn btn-danger mr-xs mb-sm" >Kembali ke sebelumnya</a>

                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

