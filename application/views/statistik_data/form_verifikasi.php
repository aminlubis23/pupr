<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_verifikasi_data" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);

        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  

        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            window.location.href = "<?php echo base_url().'verifikasi_data_cpns'?>";
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();

        }

        });

        event.preventDefault();
        });
});

</script>

<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'verifikasi_data_cpns'?>">VERIFIKASI DATA CPNS</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Verifikasi data dibawah ini) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">
                    <form role="form_verifikasi_data" method="post" action="<?php echo base_url('verifikasi_data_cpns/process') ?>" enctype="multipart/form-data">
                                                
                        <label>Status Data CPNS</label><br />
                        <select name="status_verifikasi" class="form-control">
                            <option value="">-Silahkan Pilih-</option>
                            <option value="DATA DAN DOKUMEN BELUM LENGKAP" <?php echo isset($value->status_verifikasi)?($value->status_verifikasi=='DATA DAN DOKUMEN BELUM LENGKAP')?'selected':'':''?>>Data dan Dokumen Belum Lengkap</option>
                            <option value="VERIFIED" <?php echo isset($value->status_verifikasi)?($value->status_verifikasi=='DATA DAN DOKUMEN BELUM LENGKAP')?'selected':'':''?>>Memenuhi Semua Persyaratan</option>
                        </select>
                        <br />

                        <label>Catatan</label><br />
                        <input type="text" class="form-control" id="catatan_verifikasi" name="catatan_verifikasi" value="<?php echo isset($value->catatan_verifikasi)?$value->catatan_verifikasi:''?>"/>
                        <input type="hidden" class="form-control" id="dp_id" name="dp_id" value="<?php echo isset($value->dp_id)?$value->dp_id:''?>"/>
                        <br />

                        <!-- btn submit -->
                        <center>
                        
                        <a href="<?php echo base_url().'verifikasi_data_cpns'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>

                        </center>
                    </form>
                </div>

                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

