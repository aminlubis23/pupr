<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 11/08/2018
 * Time: 7:10
 */
$site	= $this->konfigurasi_model->listing();
//include('produk.php');
//include('berita.php');
?>

<!--gallery-->
<div class="gallery">
    <div class="container">
        <div class="gallery-grids">

            <!-- Start MAIN -->
            <div role="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <br />
                            <h4 style="text-align: left">
                                <strong>Edit Data Pelamar</strong>
                            </h4>
                            <br />

                            <div class="col-md-12" style="background-color: white">
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Biodata</h5>
                                        <div class="col-md-4">
                                            <img src="<?php echo base_url () ?>assets/admin/images/foto.png" class="img-responsive" alt="" style="width: 80%" />
                                            <b style="color: #0088cc">Lokasi SKD</b> &nbsp;
                                            <label id="lbllokasiSKD">Jakarta</label><br />
                                            <b style="color: #0088cc">Lokasi SKB</b> &nbsp;<label id="lbllokasiskb">Jakarta</label><br />
                                            <b style="color: #0088cc">ID</b> &nbsp;<label id="lblID">95</label>
                                        </div>
                                        <div class="col-md-4">
                                            Nama Lengkap<br />
                                            <input type="text" id="txtnama" class="form-control" placeholder="RIZKA MASYHURA, S.T" />

                                            <br />
                                            No. KK<br />
                                            <input type="text" id="txtnokk" class="form-control" placeholder="17101079170002" />

                                            <br />
                                            Jenis Kelamin<br />
                                            <input type="text" id="txtjenkel" class="form-control" placeholder="Laki-laki" />

                                            <br />
                                            Status Nikah<br />
                                            <input type="text" id="txtstatusnikah" class="form-control" placeholder="Belum Kawin/Kawin" />

                                            <br />
                                            KTP
                                            <br />
                                            <input type="file" id="uploadktp" />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="30%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgktp" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                            <br />
                                            <br />
                                            Catatan<br />
                                            <input type="text" id="catatanbiodata" class="form-control" placeholder="catatan" />
                                        </div>
                                        <div class="col-md-4">
                                            NIK<br />
                                            <input type="text" id="txtnik" class="form-control" placeholder="117101079170001" />

                                            <br />
                                            TTL<br />
                                            <input type="text" id="txtttl" class="form-control" placeholder="BANDA ACEH, 1994-08-03" />

                                            <br />
                                            Agama<br />
                                            <input type="text" id="txtagama" class="form-control" placeholder="Islam" />
                                            <br />
                                            <br />
                                            Surat Pernyataan
                                            <br />
                                            <input type="file" id="uploadsuratpernyataan" />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="30%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgsuratpernyataan" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                            <br />
                                            <br />
                                            KK
                                            <br />
                                            <input type="file" id="uploadkk" />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="30%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgkk" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Kontak</h5>
                                        <div class="col-md-6">
                                            Alamat Lengkap<br />
                                            <textarea id="txtalamat" class="form-control" rows="2">Jl. Utami No.4 Ling Cut Meutia Kelurahan Peuniti Kecamatan Baiturrahman 23241, Banda Aceh</textarea>


                                            <br />
                                            Alamat Domisili<br />
                                            <textarea id="txtalamatortu" class="form-control" rows="2">Jl. Utami No.4 Ling Cut Meutia Kelurahan Peuniti Kecamatan Baiturrahman 23241, Banda Aceh</textarea>


                                        </div>
                                        <div class="col-md-6">
                                            Nomor Telepon<br />
                                            <input type="text" id="txttelp" class="form-control" placeholder="08116850888/065125623" />

                                            <br />
                                            Email<br />
                                            <input type="text" id="txtemail" class="form-control" placeholder="rizka.masyhura@gmail.com" />

                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Formasi</h5>
                                        <div class="col-md-6">
                                            Kode Formasi<br />
                                            <input type="text" id="txtformasi" class="form-control" placeholder="25342-1" />

                                            <br />
                                            Jurusan Pendidikan<br />
                                            <input type="text" id="txtpendidikan" class="form-control" placeholder="S.1/D.4 Teknik Sipil" />

                                            <br />
                                            Jenis Formasi<br />
                                            <input type="text" id="txtjenisformasi" class="form-control" placeholder="Umum" />

                                        </div>
                                        <div class="col-md-6">
                                            Jabatan<br />
                                            <input type="text" id="txtjabatan" class="form-control" placeholder="Teknik Pengairan Ahli Pratama" />

                                            <br />
                                            Penempatan<br />
                                            <input type="text" id="txtpenempatan" class="form-control" placeholder="Direktorat Jendral Sumber Daya Air" />

                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Pendidikan</h5>
                                        <div class="col-md-6">
                                            Jurusan Pendidikan<br />
                                            <input type="text" id="txtjurpen" class="form-control" placeholder="S.1/D.4 Teknik Sipil" />

                                            <br />
                                            Perguruan Tinggi (Jenis)<br />
                                            <input type="text" id="txtpt" class="form-control" placeholder="Universitas Syiah Kuala (Negeri)" />

                                            <br />
                                            Tanggal Ijazah / Tanggal Lulus<br />
                                            <input type="text" id="txttglijazah" class="form-control" placeholder="2017-02-01 / 2017-01-05" />

                                            <br />
                                            Ijazah
                                            <br />
                                            <input type="file" id="uploadijazah" />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="20%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgijazah" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                            <br />
                                            <br />
                                            Catatan<br />
                                            <input type="text" id="catatanpendidikan" class="form-control" placeholder="catatan" />
                                        </div>
                                        <div class="col-md-6">
                                            IPK (Akreditasi)
                                            <br />
                                            <input type="text" id="txtipkakreditasi" class="form-control" placeholder="3.49 (A)" />

                                            <br />
                                            Nomor Ijazah<br />
                                            <input type="text" id="txtnoijazah" class="form-control" placeholder="0780/7862/TS/2017" />

                                            <br />
                                            Gelar Sarjana<br />
                                            <input type="text" id="txtgelarsarjana" class="form-control" placeholder="Sarjana Teknik" />

                                            <br />
                                            Transkrip<br />
                                            <input type="file" id="uploadtranskrip" />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="20%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgtranskrip" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Kemampuan Bahasa Inggris</h5>
                                        <div class="col-md-6">
                                            Jenis TOEFL<br />
                                            <input type="text" id="txtjenistoefl" class="form-control" placeholder="Sarjana Teknik" />

                                            <br />
                                            Lembaga Penerbit TOEFL<br />
                                            <input type="text" id="txtlembagapenerbittoefl" class="form-control" placeholder="Kanguru International Education Service" />

                                            <br />
                                            Tanggal Sertifikat<br />
                                            <input type="date" id="txttglsertifikat" class="form-control" />

                                            <br />
                                            Lokasi Tes TOEFL
                                            <br />
                                            <input type="text" id="txtlokasitestoefl" class="form-control" placeholder="Banda Aceh" />

                                            <br />
                                            Catatan<br />
                                            <input type="text" id="catatantoefl" class="form-control" placeholder="catatan" />
                                        </div>
                                        <div class="col-md-6">
                                            Nilai TOEFL
                                            <br />
                                            <input type="text" id="txtnilaitoefl" class="form-control" placeholder="477" />

                                            <br />
                                            Nomor Sertifikat<br />
                                            <input type="text" id="txtnosertifikat" class="form-control" placeholder="BRONZE:IDN20153:129092920" />

                                            <br />
                                            Tanggal Tes TOEFL<br />
                                            <input type="date" id="txttgltestoefl" class="form-control" />

                                            <br />
                                            Sertifikat TOEFL<br />
                                            <input type="file" id="uploadsertifikattoefl" />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="20%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgsertifikattoefl" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;PTT?</h5>
                                        <div class="col-md-12">
                                            PTT<br />
                                            <input type="text" id="txtptt" class="form-control" placeholder="Tidak" />
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Status MS/TMS Sistem</h5>
                                        <div class="col-md-6">
                                            Status<br />
                                            <select id="cbstatusmstms" class="form-control">
                                                <option value="memenuhisyarat">Memenuhi Syarat</option>
                                                <option value="tidakmemenuhi">Tidak Memenuhi Syarat</option>
                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            Catatan<br />
                                            <input type="text" id="catatanmstms" class="form-control" placeholder="catatan" />
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Status Verifikasi</h5>
                                        <div class="col-md-6">
                                            Status<br />
                                            <select id="cbstatusverifikasi" class="form-control">
                                                <option value="memenuhisyarat">Memenuhi Syarat</option>
                                                <option value="tidakmemenuhi">Tidak Memenuhi Syarat</option>
                                            </select>
                                            <br />
                                            Verifikator<br />
                                            <select id="cbverifikator" class="form-control">
                                                <option value="fadholi">Fadholi</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            Catatan<br />
                                            <input type="text" id="catatanverifikasi" class="form-control" placeholder="catatan" />
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;SKD & SKB</h5>
                                        <div class="col-md-6">
                                            Lihat Kartu Peserta SKD<br />
                                            <a href="#" class="btn btn-success mr-xs mb-sm">Lihat</a>
                                        </div>
                                        <div class="col-md-6">
                                            Lihat Kartu Peserta SKB<br />
                                            <a href="#" class="btn btn-success mr-xs mb-sm">Lihat</a>
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Status</h5>
                                        <div class="col-md-6">
                                            Hasil<br />
                                            <select class="form-control" id="cbstatus">
                                                <option value="memenuhi">Memenuhi Syarat</option>
                                                <option value="tidak">Tidak Memenuhi Syarat</option>
                                            </select>
                                            <br />
                                            TIU<br />
                                            <input type="text" class="form-control" id="txttiu" />
                                            <br />
                                            Total<br />
                                            <input type="text" class="form-control" id="txttotal" />
                                        </div>
                                        <div class="col-md-6">
                                            TWK<br />
                                            <input type="text" class="form-control" id="txttwk" />
                                            <br />
                                            TKP<br />
                                            <input type="text" class="form-control" id="txttkp" />

                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="center">
                                        <button type="button" class="btn btn-primary mr-xs mb-sm" id="btnsave">Save</button>
                                        <button type="button" class="btn btn-danger mr-xs mb-sm" id="btncancel">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- END MAIN -->

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>
        </div>
    </div>
</div>