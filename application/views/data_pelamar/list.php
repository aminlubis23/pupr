<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 27/07/2018
 * Time: 10:22
 */
$site	= $this->konfigurasi_model->listing();
//include('produk.php');
//include('berita.php');
?>

<!--gallery-->

<div role="main" class="main">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <br />
                <h3 style="text-align: left">
                    <strong>Form Summary Data Pelamar</strong>
                </h3>
                <div class="row">
                    <div class="col-md-2">
                        <label>Formasi :</label><br />
                        <select class="form-control" id="cbformasi">
                            <option id="formasi1" value="Pernah Mengisi Formasi">Pernah Mengisi Formasi</option>
                            <option id="formasi2" value="Belum Pernah Mengisi Formasi">Belum Pernah Mengisi Formasi</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Lokasi:</label>
                        <br />
                        <select class="form-control" id="cblokasiSKB">
                            <option id="idlokasi1" value="Jakarta">Jakarta</option>
                            <option id="idlokasi2" value="Bandung">Bandung</option>
                            <option id="idlokasi3" value="Bali">Bali</option>
                            <option id="idlokasi4" value="Jogjakarta">Jogjakarta</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Status Pelamar :</label>
                        <br />
                        <select class="form-control" id="cbstatuspelamar">
                            <option id="Option5" value="Data Pelamar Telah Dilengkapi">Data Telah Dilengkapi</option>
                            <option id="Option6" value="Data Pelamar Belum Lengkap">Data Belum Lengkap</option>
                            <option id="Option7" value="Pelamar Belum Diverifikasi">Pelamar Belum Diverifikasi</option>
                            <option id="Option8" value="Pelamar Memenuhi Syarat">Pelamar Memenuhi Syarat</option>
                            <option id="Option9" value="Pelamar Memenuhi Syarat Dengan Catatan">Pelamar Memenuhi Syarat Dengan Catatan</option>
                            <option id="Option10" value="Pelamar Memenuhi Syarat">Pelamar Tidak Memenuhi Syarat</option>
                            <option id="Option11" value="Pelamar Masih Ada Catatan Persyaratan Sistem">Pelamar Masih Ada Catatan Persyaratan Sistem</option>
                            <option id="Option12" value="Pelamar Tidak Ada Catatan Persyaratan Sistem">Pelamar Tidak Ada Catatan Persyaratan Sistem</option>
                            <option id="Option13" value="Persyaratan Sistem Pelamar Belum Terstatus">Persyaratan Sistem Belum Terstatus</option>
                            <option id="Option14" value="Berhak Ikut SKB">Berhak Ikut SKB</option>
                            <option id="Option15" value="Tidak Berhak Ikut SKD">Tidak Berhak Ikut SKD</option>
                            <option id="Option16" value="Lulus SKB">Lulus SKB</option>
                            <option id="Option17" value="Tidak Lulus SKB">Tidak Lulus SKB</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Pencarian :</label>
                        <br />
                        <input type="text" class="form-control" />
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <select class="form-control" id="cbpencarian">
                            <option id="Option18" value="NamaPerguruanTinggi">Nama Perguruan Tinggi</option>
                            <option id="Option19" value="KampusA">Kampus A</option>
                            <option id="Option20" value="KampusB">Kampus B</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label></label>  <br />
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-search"></i> search</button>
                    </div>
                </div>
                <br />
                <div class="row" style="background-color:white">
                    <br />
                    <div class="col-md-12" style="background-color: white; overflow-x: auto; overflow-y: auto">
                        <table id="tableview" class="table table-striped table-bordered" style="width: 1800px; background-color: white">
                            <thead>
                            <tr>
                                <th style="color: #242424; text-align:center">No</th>
                                <th style="color: #242424; text-align:center">ID </th>
                                <th style="color: #242424; text-align:center">NIK</th>
                                <th style="color: #242424; text-align:center">Nama Lengkap</th>
                                <th style="color: #242424; text-align:center">Jenis Kelamin</th>
                                <th style="color: #242424; text-align:center">Agama</th>
                                <th style="color: #242424; text-align:center">Status</th>
                                <th style="color: #242424; text-align:center">Penempatan</th>
                                <th style="color: #242424; text-align:center">User Update</th>
                                <th style="color: #242424; text-align:center">Date Update</th>
                                <th style="color: #242424; text-align:center">Remarks</th>
                                <th style="color: #242424; text-align:center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach($data_pelamar as $data_pelamar) { ?>
                            <tr>
                                <td>
                                    <label style="color: #242424" id="lblno"><?php echo $data_pelamar->id_exportdatascnbkn ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lblid"><?php echo $data_pelamar->id ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lblnik"><?php echo $data_pelamar->nik ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lblnama"><?php echo $data_pelamar->nama ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbljenkel"><?php echo $data_pelamar->nama_kelamin ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbljenkel"><?php echo $data_pelamar->agama ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbljenkel"><?php echo $data_pelamar->status_kawin ?></label>
                                </td>
                                <!--<td>
                                    <label style="color: #242424" id="lbljabatan"><?php echo $data_pelamar->jabatan ?></label>
                                </td>-->
                                <td>
                                    <label style="color: #242424" id="lblpenempatan"><?php echo $data_pelamar->alamat_domisili ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbluserupdate"><?php echo $data_pelamar->create_by ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbldateupdate"><?php echo $data_pelamar->recorded_time ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lblremarks"><?php echo $data_pelamar->remarks ?></label>
                                </td>
                                <td style="text-align: center">
                                    <button type="button" id="btndelete1" class="btn btn-danger mr-xs mb-sm">
                                        <a href="<?php echo base_url('data_pelamar/delete/'.$data_pelamar->id_exportdatascnbkn) ?>"class="btn btn-primary btn-sm"><i class="fa fa-trash "></i></a></button>
                                </td>
                            </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<br />

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <br /><br /><p style="color:#565656;font-size: 12.5px;font-family: Roboto-Regular;">Copyright © 2018 <?php echo $site['namaweb']?>. Hak Cipta dilindungi</p>
            </div>
            </center>

        </div>
    </div>

</footer>

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

<!-- Examples -->
<script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

<!-- datatable-->
<!--<script src="Datatable/jquery-3.3.1.js"></script>-->
<!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
<script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tableview').DataTable();
    });
</script>
