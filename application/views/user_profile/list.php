<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 29/07/2018
 * Time: 17:48
 */

$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');

?>
<!--gallery-->
<div class="gallery">
    <div class="container">
        <div class="gallery-grids">

            <div role="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <br />
                            <h4 style="text-align: left">
                                <strong>Edit Data Pelamar</strong>
                            </h4>
                            <br />

                            <div class="col-md-12" style="background-color: white">
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Biodata</h5>
                                        <div class="col-md-4">
                                            <img src="<?php echo base_url () ?>assets/admin/images/foto.png" class="img-responsive" alt="" style="width: 80%" />
                                            <b style="color: #0088cc">Lokasi SKD</b> &nbsp;
                                            <label id="lbllokasiSKD">Jakarta</label><br />
                                            <b style="color: #0088cc">Lokasi SKB</b> &nbsp;<label id="lbllokasiskb">Jakarta</label><br />
                                            <b style="color: #0088cc">ID</b> &nbsp;<label id="lblID">95</label>
                                        </div>
                                        <div class="col-md-4">
                                            Nama Lengkap<br />
                                            <label id="lblnama">RIZKA MASYHURA, S.T</label>
                                            <br />
                                            <br />
                                            No. KK<br />
                                            <label id="lblnokk">117101079170002</label>
                                            <br />
                                            <br />
                                            Jenis Kelamin<br />
                                            <label id="lbljenkel">Laki-laki</label>
                                            <br />
                                            <br />
                                            Status Nikah<br />
                                            <label id="lblstatusnikah">Belum Kawin</label>
                                            <br />
                                            <br />
                                            KTP
                                            <br />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="30%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgktp" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                            <br />
                                            <br />
                                            Catatan<br />
                                            <label id="lblcatatanprofile"></label>
                                        </div>
                                        <div class="col-md-4">
                                            NIK<br />
                                            <label id="lblnik">117101079170001</label>
                                            <br />
                                            <br />
                                            TTL<br />
                                            <label id="lblttl">BANDA ACEH, 1994-08-03</label>
                                            <br />
                                            <br />
                                            Agama<br />
                                            <label id="lblagama">Islam</label>
                                            <br />
                                            <br />
                                            Surat Pernyataan
                                            <br />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="30%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgsuratpernyataan" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                            <br />
                                            <br />
                                            KK
                                            <br />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="30%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgkk" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Kontak</h5>
                                        <div class="col-md-6">
                                            Alamat Lengkap<br />
                                            <label id="lblalamat">Jl. Utami No.4 Ling Cut Meutia Kelurahan Peuniti Kecamatan Baiturrahman 23241, Banda Aceh</label>
                                            <br />
                                            <br />
                                            Alamat Domisili<br />
                                            <label id="lblalamatortu">Jl. Utami No.4 Ling Cut Meutia Kelurahan Peuniti Kecamatan Baiturrahman 23241, Banda Aceh</label>

                                        </div>
                                        <div class="col-md-6">
                                            Nomor Telepon<br />
                                            <label id="lbltelp">08116850888/065125623</label>
                                            <br />
                                            <br />
                                            Email<br />
                                            <label id="lblemail">rizka.masyhura@gmail.com</label>

                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Formasi</h5>
                                        <div class="col-md-6">
                                            Kode Formasi<br />
                                            <label id="lblformasi">25342-1</label>
                                            <br />
                                            <br />
                                            Jurusan Pendidikan<br />
                                            <label id="lbljurusanpendidikan">S.1/D.4 Teknik Sipil</label>
                                            <br />
                                            <br />
                                            Jenis Formasi<br />
                                            <label id="lbljenisformasi">Umum</label>

                                        </div>
                                        <div class="col-md-6">
                                            Jabatan<br />
                                            <label id="lbljabatan">Teknik Pengairan Ahli Pratama</label>
                                            <br />
                                            <br />
                                            Penempatan<br />
                                            <label id="lblpenempatan">Direktorat Jendral Sumber Daya Air</label>

                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Pendidikan</h5>
                                        <div class="col-md-6">
                                            Jurusan Pendidikan<br />
                                            <label id="lbljurpen">S.1/D.4 Teknik Sipil</label>
                                            <br />
                                            <br />
                                            Perguruan Tinggi (Jenis)<br />
                                            <label id="lblpt">Universitas Syiah Kuala (Negeri)</label>
                                            <br />
                                            <br />
                                            Tanggal Ijazah / Tanggal Lulus<br />
                                            <label id="lbltglijazah">2017-02-01 / 2017-01-05</label>
                                            <br />
                                            <br />
                                            Ijazah
                                            <br />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="20%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgijazah" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                            <br />
                                            <br />
                                            Catatan<br />
                                            <label id="lblcatatanpendidikan">Belum Kawin</label>
                                        </div>
                                        <div class="col-md-6">
                                            IPK (Akreditasi)
                                            <br />
                                            <label id="lblipkakreditasi">3.49 (A)</label>
                                            <br />
                                            <br />
                                            Nomor Ijazah<br />
                                            <label id="lblnoijazah">0780/7862/TS/2017</label>
                                            <br />
                                            <br />
                                            Gelar Sarjana<br />
                                            <label id="lblgelarsarjana">Sarjana Teknik</label>
                                            <br />
                                            <br />
                                            Transkrip<br />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="20%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="A1" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Kemampuan Bahasa Inggris</h5>
                                        <div class="col-md-6">
                                            Jenis TOEFL<br />
                                            <label id="lbljenistoefl">TOEFL/TOEFL Prediction</label>
                                            <br />
                                            <br />
                                            Lembaga Penerbit TOEFL<br />
                                            <label id="lblpenerbittoefl">Kanguru International Education Service</label>
                                            <br />
                                            <br />
                                            Tanggal Sertifikat<br />
                                            <label id="lblsertifikattoefl">18 MARET 2017</label>
                                            <br />
                                            <br />
                                            Lokasi Tes TOEFL
                                            <br />
                                            <label id="lbllokasitesttoefl">Banda Aceh</label>
                                            <br />
                                            <br />
                                            Catatan<br />
                                            <label id="lblcatatantoefl"></label>

                                        </div>
                                        <div class="col-md-6">
                                            Nilai TOEFL
                                            <br />
                                            <label id="lblnilaitoefl">477</label>
                                            <br />
                                            <br />
                                            Nomor Sertifikat<br />
                                            <label id="lblnosertifikattoefl">BRONZE:IDN20153:129092920</label>
                                            <br />
                                            <br />
                                            Tanggal Tes TOEFL<br />
                                            <label id="Label6">18 MARET 2017</label>
                                            <br />
                                            <br />
                                            Sertifikat TOEFL<br />
                                            <img src="<?php echo base_url () ?>assets/admin/images/ktp.jpg" width="20%" />
                                            &nbsp;<a href="<?php echo base_url () ?>assets/admin/images/ktp.jpg" id="imgsertifikattoefl" class="btn btn-warning mr-xs mb-sm">Preview</a>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;PTT?</h5>
                                        <div class="col-md-12">
                                            PTT<br />
                                            <label id="lblptt">Tidak</label>

                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Status MS/TMS Sistem</h5>
                                        <div class="col-md-6">
                                            Status<br />
                                            <label id="lblstatusmstms">Memenuhi Syarat<</label>

                                        </div>
                                        <div class="col-md-6">
                                            Catatan<br />
                                            <label id="lblcatatanmstms"></label>

                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Status Verifikasi</h5>
                                        <div class="col-md-6">
                                            Status<br />
                                            <label id="lblstatusverifikasi">Memenuhi Syarat</label>

                                            <br />
                                            Verifikator<br />
                                            <label id="lblverifikator">Fadholi</label>

                                        </div>
                                        <div class="col-md-6">
                                            Catatan<br />
                                            <label id="lblcatatanverifikasi"></label>

                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;SKD & SKB</h5>
                                        <div class="col-md-6">
                                            Lihat Kartu Peserta SKD<br />
                                            <a href="<?php echo base_url(); ?>admin/user_profile/kartupesertaSKD" class="btn btn-success mr-xs mb-sm">Lihat</a>
                                        </div>
                                        <div class="col-md-6">
                                            Lihat Kartu Peserta SKB<br />
                                            <a href="<?php echo base_url(); ?>admin/user_profile/kartupesertaSKD" class="btn btn-success mr-xs mb-sm">Lihat</a>
                                        </div>

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="background-color: #0088cc; color: white; height: 50px">
                                            <br />
                                            &nbsp; &nbsp;Status</h5>
                                        <div class="col-md-6">
                                            Hasil<br />
                                            <label id="lblhasil">MEMENUHI</label>

                                            <br />
                                            TIU<br />
                                            <label id="lbltiu">90</label>
                                            <br />
                                            Total<br />
                                            <label id="lbltotal">353</label>
                                        </div>
                                        <div class="col-md-6">
                                            TWK<br />
                                            <label id="lbltwk">110</label>
                                            <br />
                                            TKP<br />
                                            <label id="lbltkp">153</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>
        </div>
    </div>
</div>