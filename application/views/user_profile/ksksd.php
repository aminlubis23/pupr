<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 09/08/2018
 * Time: 20:13
 */
$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');

?>
<!--gallery-->
<div class="gallery">
    <div class="container">
        <div class="gallery-grids">

            <div class="container">
                <img src="<?php echo base_url () ?>assets/admin/images/lembarpanitia.jpg" width="1000px" />
                <div class="panitia-top-left">
                    <img src="<?php echo base_url () ?>assets/admin/images/foto.png" alt="foto peserta 4x6" width="200" id="fotolembarpanitia" class="img-responsive" />
                </div>
                <div class="panitia-bottom-left">
                    <label id="ttdlembarpanitia" style="color: #232323;">Nama Peserta CPNS</label>
                </div>
                <div class="panitia-bottom-right ">
                    <label id="ttdPUlembarpanitia" style="color: #232323">Nama Kepala Biro Kepegawaian / Panitia</label>
                </div>

                <div class="panitiaisi-top-left">
                    <div class="row">
                        <div class="col-md-4" style="text-align: left">
                            <label style="color: #7c7c7c; width: 300px; font-style: inherit">No. Peserta / NIK</label>
                            <label id="lblnikpeserta" style="color: #232323">
                                3021-113-1030004/3524141805900001<br />
                            </label>
                            <br />
                            <label style="color: #7c7c7c">Nama (JK)</label><br />
                            <label id="lblnamapeserta" style="color: #232323;">
                                ANHAS AWWAB (L)
                            </label>

                        </div>
                        <div class="col-md-4">
                            <br />
                            <br />
                            <br />

                            <label style="color: #7c7c7c">Tempat, Tanggal Lahir</label><br />
                            <label id="Label1" style="color: #232323;">
                                SITUBONDO, 18 Mei 1990
                            </label>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <label style="color: #7c7c7c">(Scan Untuk Absensi)</label>
                            </div>
                            <div id="externalbox">
                                <div id="inputdata" style="color: #232323">123GFD34</div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="text-align: left">
                            <label style="color: #7c7c7c">Formasi</label><br />
                            <label id="lblformasi" style="color: #232323;">
                                (Umum) Teknik Pengairan Ahli Pertama S.1/D.4 Teknik Geodesi
                            </label>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12" style="text-align: left">
                            <label style="color: #7c7c7c">Jadwal SKD</label><br />
                            <label id="lbljadwalSKD" style="color: #232323;">
                                Surabaya - 17 Oktober 2017 - Sesi V - no. 098
                                Alamat : Gor Brawijaya Jln. Hayam Wuruk, No. 17-42 Sawunggaling Wonokromo, Kota
                                Surabaya, Jawa Timur<br />
                                <br />
                            </label>
                        </div>
                    </div>

                </div>

                <img src="<?php echo base_url () ?>assets/admin/images/garisputusputus.png" />

            </div>

            <div class="container">
                <img src="<?php echo base_url () ?>assets/admin/images/lembarpeserta.jpg" width="1000px" />
                <div class="peserta-top-left">
                    <img src="<?php echo base_url () ?>assets/admin/images/foto.png" alt="foto peserta 4x6" width="200" id="Img1" />
                </div>
                <div class="peserta-bottom-left">
                    <label id="Label2" style="color: #232323;">Nama Peserta CPNS</label>
                </div>
                <div class="peserta-bottom-right ">
                    <label id="Label3" style="color: #232323">Nama Kepala Biro Kepegawaian / Panitia</label>
                </div>
                <div class="pesertaisi-top-left">
                    <div class="row">
                        <div class="col-md-4" style="text-align: left">
                            <label style="color: #7c7c7c; width: 300px; font-style: inherit">No. Peserta / NIK</label>
                            <label id="Label4" style="color: #232323">
                                3021-113-1030004/3524141805900001<br />
                            </label>
                            <br />
                            <label style="color: #7c7c7c">Nama (JK)</label><br />
                            <label id="Label5" style="color: #232323;">
                                ANHAS AWWAB (L)
                            </label>

                        </div>
                        <div class="col-md-4">
                            <br />
                            <br />
                            <br />

                            <label style="color: #7c7c7c">Tempat, Tanggal Lahir</label><br />
                            <label id="Label6" style="color: #232323;">
                                SITUBONDO, 18 Mei 1990
                            </label>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <label style="color: #7c7c7c">(Scan Untuk Absensi)</label>
                            </div>
                            <div id="externalbox1">
                                <div id="inputdata1" style="color: #232323">123GFD34</div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="text-align: left">
                            <label style="color: #7c7c7c">Formasi</label><br />
                            <label id="Label7" style="color: #232323;">
                                (Umum) Teknik Pengairan Ahli Pertama S.1/D.4 Teknik Geodesi
                            </label>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12" style="text-align: left">
                            <label style="color: #7c7c7c">Jadwal SKD</label><br />
                            <label id="Label8" style="color: #232323;">
                                Surabaya - 17 Oktober 2017 - Sesi V - no. 098
                                Alamat : Gor Brawijaya Jln. Hayam Wuruk, No. 17-42 Sawunggaling Wonokromo, Kota
                                Surabaya, Jawa Timur<br />
                                <br />
                            </label>
                        </div>
                    </div>


                </div>
            </div>

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

            <script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
                get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 1);
                /* ]]> */
            </script>

            <script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
                get_object("inputdata1").innerHTML = DrawCode39Barcode(get_object("inputdata1").innerHTML, 1);
                /* ]]> */
            </script>

        </div>
    </div>
</div>