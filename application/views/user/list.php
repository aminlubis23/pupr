<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');
?>

<!--main-->
<div class="main">
    <div class="container">
        <div class="main-grids">

            <div role="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <br />
                            <h3 style="text-align: center">
                                <strong>User Management</strong>
                            </h3>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <br />
                            <div class="row">
                                <div class="col-md-10">
                                </div>

                                <div class="col-md-2" style="color: gray">
                                    <br />
                                    <!--<a href="<?php echo base_url(); ?>admin/user?act=tambah" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>Buat User Baru</a>-->
                                    <a href="<?php echo base_url(); ?>user/tambah" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>Buat User Baru</a>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <table id="tableview" class="table table-striped table-bordered" style="width: 100%; background-color: white">
                                    <thead>
                                    <tr>
                                        <th style="color: #242424">No.</th>
                                        <th style="color: #242424">Nama</th>
                                        <th style="color: #242424">Email </th>
                                        <th style="color: #242424">username</th>
                                        <th style="color: #242424">Password</th>
                                        <th style="color: #242424">Level User</th>
                                        <th style="color: #242424">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                    <tbody>
                                    <?php $i=1; foreach($user as $user) { ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $user->nama ?></td>
                                            <td><?php echo $user->email ?></td>
                                            <td><?php echo $user->usernama ?></td>
                                            <td><?php echo $user->kuncimasuk ?></td>
                                            <td><?php echo $user->akses_level ?></td>
                                            <td style="text-align: center">
                                                <a href="<?php echo base_url('user/edit/'.$user->id_user) ?>" id="btnedit" class="btn btn-success"><i class="fa fa-edit"></i>edit</a> &nbsp; &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('user/delete/'.$user->id_user) ?>" class="btn btn-danger" id="btndelete" onClick="return confirm('Yakin ingin menghapus user ini?')"><i class="fa fa-trash"></i> trash</a>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

            <!-- datatable-->
            <!--<script src="Datatable/jquery-3.3.1.js"></script>-->
            <!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
            <script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#tableview').DataTable();
                });
            </script>
        </div>
    </div>
</div>