<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');
?>

<!--main-->
<div class="main">
    <div class="container">
        <div class="main-grids">

            <div role="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"></div>

                        <div class="col-md-6">
                            <br />
                            <h3 style="text-align: center">
                                <strong>Form Tambah User</strong>
                            </h3>
                            <div class="row" style="background-color: white">
                                <br />
                                <div class="col-md-12">

                                    <form role="form" method="post" action="<?php echo base_url('user/tambah') ?>" id="frmSignIn">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama lengkap" value="<?php echo set_value('nama') ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Email</label>
                                                    <input type="email" id="email" name="email" class="form-control" placeholder="email" value="<?php echo set_value('email') ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Username</label>
                                                    <input type="text" id="usernama" name="usernama" class="form-control" placeholder="username" value="<?php echo set_value('usernama') ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Password</label>
                                                    <input type="password" id="kuncimasuk" name="kuncimasuk" class="form-control" placeholder="password" value="<?php echo set_value('kuncimasuk') ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Level Hak Akses</label>
                                                    <select name="akses_level" class="form-control">
                                                        <option value="0">-- Pilih Salah satu --</option>
                                                        <option value="sadmin">Super Admin</option>
                                                        <option value="admin">Admin Content</option>
                                                        <option value="moderator">Admin Verifikator</option>
                                                        <option value="member">User</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <input id="btnsimpan" name="btnsimpan" value="Simpan" type="submit" class="btn btn-warning pull-right mb-xl">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

            <!-- datatable-->
            <!--<script src="Datatable/jquery-3.3.1.js"></script>-->
            <!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
            <script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#tableview').DataTable();
                });
            </script>

        </div>
    </div>
</div>