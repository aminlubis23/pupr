<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 11/08/2018
 * Time: 19:02
 */
$site = $this->konfigurasi_model->listing();
?>
<!--header-->
<div class="body">
    <header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 57, 'stickySetTop': '-57px', 'stickyChangeLogo': false}">
        <div class="header-body">
            <div class="header-top header-top-quaternary header-top-style-3" style="background-color: #4066c1; font-family: Roboto-Thin ;height:30px">
                <div class="container">
                    <div class="center">
                        <h5 style="color: white; font-family: Roboto-Thin ;"><b>Portal Pengadaan CPNS</b></h5>
                    </div>
                </div>
            </div>
            <div class="header-container container">
                <div class="header-row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="header-column">
                            <div class="header-logo">
                                <a href="<?php echo base_url(); ?>home">
                                    <img alt="Porto" width="200" height="54" data-sticky-width="120" data-sticky-height="40" data-sticky-top="33" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-nav">
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                        <i class="fa fa-bars"></i>
                                    </button>
                                   <!-- <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                        <nav>
                                            <ul class="nav nav-pills" id="Ul1">
                                                <li class="dropdown">
                                                    <a class="dropdown-toggle" href="<?php echo base_url(); ?>login">Home</a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>pengumuman">Pengumuman
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>tatacara">Tatacara
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>statistik">Statistik
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>unduh">Unduh
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url(); ?>bantuan">Bantuan</a>
                                                </li>
                                                <!--<li class="dropdown">
                                                    <a class="dropdown-toggle">Login</a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>login/admin"><i class="fa fa-user-circle-o"></i>&nbsp;&nbsp;LOGIN ADMIN</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>login"><i class="fa fa-user-circle-o"></i>&nbsp;&nbsp;LOGIN MEMBER</a>
                                                        </li>
                                                    </ul>
                                                </li>-->
                                                <!--<li>
                                                    <a href="<?php echo base_url(); ?>login">LOGIN</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>-->
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </header>
    <!--//header-->