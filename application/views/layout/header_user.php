<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$site = $this->konfigurasi_model->listing();
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 29/07/2018
 * Time: 18:02
 */
?>
<div class="body">
    <header id="header" class="header-no-min-height header-mobile-nav-only header-flex" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 0, 'stickySetTop': '0'}">
        <div class="header-body">
            <div class="header-container container container-xl">
                <div class="header-row">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        <div class="header-logo">
                            <a href="<?php echo base_url(); ?>admin/dasbor">
                                <img alt="Porto" width="200" height="54" data-sticky-width="120" data-sticky-height="40" data-sticky-top="33" src="<?php echo base_url () ?>assets/admin/images/logopupr2.png">
                            </a>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="header-nav header-nav-top-line">
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="header-nav-main header-nav-main-no-arrows header-nav-main-square header-nav-main-effect-3 header-nav-main-sub-effect-1 collapse">
                                <nav>
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" href="<?php echo base_url(); ?>admin/dasbor/indexUser">Beranda
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo base_url(); ?>admin/data_pelamar">Data Pelamar
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url(); ?>admin/data_pelamar/editDatapelamar">Edit Data Pelamar
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <!--<li>
                                            <a href="<?php echo base_url(); ?>admin/slider_admin/indexUser">Slider
                                            </a>
                                        </li>

                                        <li>
                                            <a href="statistikadmin.html">Statistik
                                            </a>
                                        </li>-->
                                        <li>
                                            <a href="<?php echo base_url('login/logout_user') ?>"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;LOGOUT</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </header>