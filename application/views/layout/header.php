<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//defined("BASEPATH") or exit("No direct access allowed");
$site = $this->konfigurasi_model->listing();
$menu = $this->konfigurasi_model->get_menu_level($this->session->userdata('user')->level);
//echo '<pre>';print_r($menu);die;
//$katamutiara = "Sistem ";
//$variablelogo = <img width=200 height=260 src='assets/upload/image/baru/logo-pu.png' />;
?>
<div class="body">
        <header id="header" class="header-no-min-height header-mobile-nav-only header-flex" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 0, 'stickySetTop': '0'}">
            <div class="header-body">
                <div class="header-container container container-xl">
                    <div class="header-row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class="header-logo">
                                <a href="<?php echo base_url(); ?>admin/dashboard">
                                    <img alt="cpns.pupr" width="200" height="54" data-sticky-width="120" data-sticky-height="40" data-sticky-top="33" src="<?php echo base_url () ?>assets/admin/images/logopupr2.png">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="header-nav header-nav-top-line">
                                <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <div class="header-nav-main header-nav-main-no-arrows header-nav-main-square header-nav-main-effect-3 header-nav-main-sub-effect-1 collapse">
                                    <nav>
                                        <ul class="nav nav-pills" id="mainNav">
                                            <!-- <li>
                                                <a style="font-size:10px"> Welcome, <?php echo $this->session->userdata('user')->nama; ?></a>
                                            </li> -->

                                            <?php 
                                                if ($this->session->userdata('user')->has_change_password=='Y') :
                                                foreach($menu as $key=>$row) :
                                                    if( count($row['submenu']) > 0 ) { 
                                            ?>
                                            <li class="dropdown">
                                                <a class="dropdown-toggle"><?php echo $row['name']?>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <?php foreach($row['submenu'] as $k=>$v) :?>
                                                    <li>
                                                        <a href="<?php echo base_url().$v['link']?>"><?php echo $v['name']?></a>
                                                    </li>
                                                    <?php endforeach;?>
                                                </ul>
                                            </li>
                                            <?php 

                                                    }else {
                                                        echo '<li><a href="'.base_url().$row['link'].'">'.$row['name'].'</a>
                                                        </li>';
                                                    };

                                                endforeach;
                                                endif;

                                            ?>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
        </header>
