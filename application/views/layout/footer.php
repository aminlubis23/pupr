<?php
$site = $this->konfigurasi_model->listing();
?>

<footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       <center>
                        <a href="index.html" class="logo">
                            <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                         </a>
                        <!--<p style="color:#565656">© Copyright 2017. All Rights Reserved.</p> </div>-->
                        <!--<p style="color:#565656">Copyright © 2018 <?php echo $site['namaweb']?>. All rights reserved | Design by <a style="color:#565656" href="<?php echo $site['website']?>"><?php echo $site['namaweb']?></a></p>-->

                           <br /><p style="color:#565656;font-size: 12.5px;font-family: Roboto-Regular;">Copyright © 2018 <?php echo $site['namaweb']?>. Hak Cipta dilindungi</p>
                    </center>

                </div>
            </div>

        </footer>

    </div>

    <!-- Vendor -->
    <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

    <!-- Current Page Vendor and Views -->
    <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <!-- Theme Custom -->
    <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

    <!-- Examples -->
    <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

    
    
</body>
</html>
