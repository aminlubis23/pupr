<!-- disini sebagai import library dan devine variiablenya -->
<?php defined("BASEPATH") or exit("No direct access allowed");
  $site = $this->konfigurasi_model->listing();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo $title ?></title>

    <meta name="keywords" content="CPNS PUPR Recruitment" />
    <meta name="description" content="cpns.pupr - Website CPNS Recruitment">
    <meta name="author" content="cpns.pu.go.id">

    <!-- Favicon -->
    <link href="<?php echo base_url('assets/upload/image/'.$site['icon']) ?>" rel="shortcut icon" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo base_url () ?>assets/front/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Web Fonts  -->

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/vendor/animate/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/vendor/simple-line-icons/css/simple-line-icons.min.css">
    
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/vendor/font-awesome/css/font-awesome.min.css">
    

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/css/theme-elements.css">
    
    <!-- Skin CSS -->
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/css/skins/default.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url () ?>assets/admin/css/custom.css">

    <!-- Head Libs -->
    <script src="<?php echo base_url () ?>assets/admin/vendor/modernizr/modernizr.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url () ?>assets/admin/ckeditor/ckeditor.js"></script>

    <!--datatable-->
    <!-- <link href="Datatable/bootstrap.min.css" rel="stylesheet" />-->
    <link href="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url () ?>assets/admin/Datatable/datatables.css" rel="stylesheet" />

      <!--chart-->
    <script src="<?php echo base_url () ?>assets/admin/code/highcharts.js"></script>
    <script src="<?php echo base_url () ?>assets/admin/code/modules/series-label.js"></script>
    <script src="<?php echo base_url () ?>assets/admin/code/modules/exporting.js"></script>
    <script src="<?php echo base_url () ?>assets/admin/code/modules/export-data.js"></script>
    <script src="<?php echo base_url () ?>assets/admin/code/modules/data.js"></script>

    <script src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

</head>
<body>
