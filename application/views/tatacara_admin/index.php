<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-6">
                    <h3 class="form-title">
                        <strong>TATA CARA </strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul Pengaturan Tata Cara Pendaftaran) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- add btn -->
                <div class="add-btn">
                    <a href="<?php echo base_url(); ?>tatacara_admin/form" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus-circle"></i> Buat Data Baru</a>
                </div>
                <!-- end add btn -->

                <!-- content data table -->
                <div class="col-md-12">
                    <table id="dynamic-table" base-url="tatacara_admin" class="table table-striped table-bordered" >
                        <thead>
                        <tr>
                            <th style="color: #242424; text-align: center; width:30px; ">No</th>
                            <th style="color: #242424;width:150px">Image</th>
                            <th style="color: #242424;width:200px">Judul</th>
                            <th style="color: #242424; text-align: center;">Deskripsi</th>
                            <th style="color: #242424;width:100px">Flow Number</th>
                            <th style="color: #242424; text-align: center; width:100px"">Last Update </th>
                            <th style="color: #242424; text-align: center; width:150px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>js/als_datatable.js"></script>
<!-- End Main -->