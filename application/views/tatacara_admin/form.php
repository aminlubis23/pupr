<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">

<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'tatacara_admin'?>">TATA CARA</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Modul Tata Cara) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">
                    <form role="form" method="post" id="form_tatacara_admin" action="<?php echo base_url('tatacara_admin/process') ?>" enctype="multipart/form-data">


                        <label>Judul</label><br />
                        <input type="text" class="form-control" id="judul" name="judul" value="<?php echo isset($value->judul)?$value->judul:set_value('judul')?>"/><?php echo form_error('judul'); ?>

                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($value->id)?$value->id:''?>"/>
                        <br />

                        <label>Images</label><br />
                        <input type="file" id="uploadfile" name="image" class="form-control"/>
                        <div class="pull-right" style="font-size: small">*Maximum image size 200 x 200</div>
                        <br />
                        
                        <label>Flow Number</label><br />
                        <input type="text" class="form-control" id="flow_number" name="flow_number" value="<?php echo isset($value->flow_number)?$value->flow_number:set_value('flow_number')?>"/><?php echo form_error('flow_number'); ?>
                        <br />
                        
                        <label>Link</label><br />
                        <input type="text" class="form-control" id="link" name="link" value="<?php echo isset($value->link)?$value->link:set_value('link')?>"/><?php echo form_error('link'); ?>
                        <br />
                        
                        <label>Deskripsi</label><br />
                        <textarea class="ckeditor" id="ckeditor" name="deskripsi"><?php echo isset($value->deskripsi)?$value->deskripsi:set_value('deskripsi')?></textarea><?php echo form_error('deskripsi'); ?>
                        <br />
                        <!-- btn submit -->
                        <center>
                        
                        <a href="<?php echo base_url().'tatacara_admin'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>

                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->
