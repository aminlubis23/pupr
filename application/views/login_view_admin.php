<?php
$site	= $this->konfigurasi_model->listing();
//include('produk.php');
//include('berita.php');
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');
?>

<!--main-->
<div class="main">
    <div class="container">
        <div class="main-grids">

        <div role="main" class="main">

            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <br /><br /><br />
                        <div class="featured-boxes">
                            <div class="featured-box featured-box-primary align-left mt-xlg">
                                <div class="box-content">
                                    <h4 style="text-align:center;font-family: "Roboto", Roboto-Regular, Roboto-Bold>Admin Login</h4>

                                    <form role="form" action="<?php echo base_url('login/process') ?>" id="frmSignIn" method="post">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>User ID</label>
                                                    <input type="text" id="user" name="username" placeholder="username" class="form-control input-lg">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Password</label>
                                                    <input type="password" id="password" name="password" placeholder="password" class="form-control input-lg">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <input id="btnlogin" type="submit" value="Login" name="loginadmin" class="btn btn-warning pull-left mb-xl">
                                            </div>
                                            <!--<div class="col-md-6">

                                                 <a class="pull-right" href="#">Forgot Password?</a>
                                            </div>-->
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                         <br /><br /><br /> <br />
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>


        </div>
        </div>
    </div>
</div>