<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_set_parameter" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        // Attach file

        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  

        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            window.location.href=jsonResponse.redirect;
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();

        }

        });

        event.preventDefault();
    });


});

</script>
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'set_parameter'?>">GLOBAL PARAMETER</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Modul Global Paameter) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">
                    <form role="form" method="post" id="form_set_parameter" action="<?php echo base_url('set_parameter/process') ?>" enctype="multipart/form-data">


                        <div class="form-group">
                          <label class="control-label col-md-2">Nilai TOEFL Minimal</label>
                          <div class="col-md-2">
                            <input type="text" class="form-control" id="min_toefl" name="min_toefl" value="<?php echo isset($value->min_toefl)?$value->min_toefl:''?>"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-2">Nilai IPK Minimal</label>
                          <div class="col-md-2">
                            <input type="text" class="form-control" id="min_ipk" name="min_ipk" value="<?php echo isset($value->min_ipk)?$value->min_ipk:''?>"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-2">Usia Maximal PTT</label>
                          <div class="col-md-2">
                            <input type="text" class="form-control" id="max_usia_ptt" name="max_usia_ptt" value="<?php echo isset($value->max_usia_ptt)?$value->max_usia_ptt:''?>"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-2">Usia Maximal S1</label>
                          <div class="col-md-2">
                            <input type="text" class="form-control" id="max_usia_s1" name="max_usia_s1" value="<?php echo isset($value->max_usia_s1)?$value->max_usia_s1:''?>"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-2">Usia Maximal S2</label>
                          <div class="col-md-2">
                            <input type="text" class="form-control" id="max_usia_s2" name="max_usia_s2" value="<?php echo isset($value->max_usia_s2)?$value->max_usia_s2:''?>"/>
                          </div>
                        </div>


                        <!-- btn submit -->
                        <center>
                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($value->id)?$value->id:''?>"/>
                        <a href="<?php echo base_url().'set_parameter'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>

                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->
