<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>
<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_change_password" ).submit(function( event ) {

        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);

        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  

        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        complete: function(xhr) {     
          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            window.location.href=jsonResponse.redirect;
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();

        }

        });

        event.preventDefault();
    });



});

</script>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <br /><br /><br />
            <div class="featured-boxes">
                <div class="featured-box featured-box-primary align-left mt-xlg">
                    <div class="box-content">
                        <h3 style="text-align:center;font-family: "Roboto", Roboto-Regular, Roboto-Bold>Ubah Password</h3>

                        <form role="form" action="<?php echo base_url('login/change_password_process') ?>" id="form_change_password" method="post">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>New Password</label>
                                        <input type="password" id="user" name="password" placeholder="New Password" class="form-control input-lg">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Confirm Password</label>
                                        <input type="password" id="confirm_password" name="confirm_password" placeholder="confirm_password" class="form-control input-lg">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 center">
                                     <input id="btnlogin" type="submit" value="Proses Ubah Password" name="loginadmin" class="btn btn-warning pull-left mb-xl">
                                </div>
                                <!--<div class="col-md-6">

                                     <a class="pull-right" href="#">Forgot Password?</a>
                                </div>-->
                            </div>
                        </form>
                        <div class="alert alert-danger">
                            <b>Pemberitahuan !</b><br>
                        <i>Silahkan lakukan ubah password untuk keamanan data anda</i>
                        </div>

                        
                    </div>
                </div>


            </div>
             <br /><br /><br /> <br />
        </div>
        <div class="col-md-3"></div>
    </div>
</div>