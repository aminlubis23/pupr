<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>DASHBOARD </strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Selamat Datang Pdi Aplikasi Rekrutmen Kementerian PUPR) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- add btn -->
                <div class="row">
                    <div class="col-md-12">

                        <div class="row" style="background-color: white;padding-top:10px">
                            <div class="container">
                                <div class="counters">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-primary">
                                            <strong id="valuepelamar"><?php echo $count['total_all']?></strong>
                                            <label class="title-counter">Total Data Peserta<br>Hasil Import Data Dari BKN</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-secondary">
                                            <strong id="valueadm"><?php echo $count['ttl_pelamar']?></strong>
                                            <label class="title-counter">Total Pelamar Yang Sudah <br>Melengkapi Data Pada Aplikasi Kementerian</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-tertiary">
                                            <strong id="valueSKD"><?php echo $count['memenuhi_syarat']?></strong>
                                            <label class="title-counter">Total Pelamar Yang Telah<br> Memenuhi Syarat</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="counter counter-quaternary">
                                            <strong id="valueSKB"><?php echo $count['lulus']?></strong>
                                            <label class="title-counter">Total Pelamar Yang Lulus<br>Pada Seleksi Akhir</label>
                                        </div>
                                    </div>
                                </div>
                                <hr class="tall" />
                            </div>
                            <br>
                            <hr>
                            <div class="row">
                              <div class="col-md-6">
                                 <div id="grafik_by_jk" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                              </div>

                              <div class="col-md-6">
                                 <div id="grafik_by_formasi" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                              </div>
                              <hr>
                              <div class="col-md-6">
                                 <div id="grafik_by_status_marital" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                              </div>
                              <div class="col-md-6">
                                 <div id="grafik_by_agama" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                              </div>
                          </div>
                            <br>                            

                        </div>

                    </div>
                    
                </div>

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- End Main -->

<script type="text/javascript">

    // Build the chart
    Highcharts.chart('grafik_by_jk', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Jenis Kelamin'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                    <?php
                        foreach ($grafik['graph_by_jk'] as $kbj => $vbj) {
                            echo '{ name: '."'".$vbj->gender_name."'".', y: '.$vbj->total.' },';
                        }
                    ?>
                    
            ]
        }]
    });

    // Build the chart
    Highcharts.chart('grafik_by_formasi', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Jenis Formasi'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                     <?php
                        foreach ($grafik['graph_by_formasi'] as $kbf => $vbf) {
                            echo '{ name: '."'".$vbf->formasi_jenis_name."'".', y: '.$vbf->total.' },';
                        }
                    ?>
                  ]
        }]
    });

    Highcharts.chart('grafik_by_status_marital', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Status Perkawinan'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                <?php
                    foreach ($grafik['graph_by_status_marital'] as $kbsm => $vbsm) {
                        echo '{ name: '."'".$vbsm->ms_name."'".', y: '.$vbsm->total.' },';
                    }
                ?>
            ]
        }]
    });

    Highcharts.chart('grafik_by_agama', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentase Data Pelamar<br>Berdasarkan Agama'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                <?php
                    foreach ($grafik['graph_by_religion'] as $kbr => $vbr) {
                        echo '{ name: '."'".$vbr->religion_name."'".', y: '.$vbr->total.' },';
                    }
                ?>

            ]
        }]
    });

    Highcharts.chart('grafik_by_status_verifikasi', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Hasil Verifikasi Data Pelamar<br>Berdasarkan Status Verifikasi'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                <?php
                    foreach ($grafik['graph_by_status_verifikasi'] as $kbsv => $vbsv) {
                        echo '{ name: '."'".$vbsv->sv_name."'".', y: '.$vbsv->total.' },';
                    }
                ?>

            ]
        }]
    });


    Highcharts.chart('grafik_by_lulus_ipk', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Hasil Verifikasi Data Pelamar<br>Berdasarkan Nilai IPK '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                { name: 'Lulus', y: <?php echo $grafik['graph_by_ipk']['L']?> },
                { name: 'Tidak Lulus', y: <?php echo $grafik['graph_by_ipk']['TL']?> },

            ]
        }]
    });

    Highcharts.chart('grafik_by_lulus_toefl', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Hasil Verifikasi Data Pelamar<br>Berdasarkan Nilai TOEFL '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Percenteage',
            colorByPoint: true,
            data: [
                { name: 'Lulus', y: <?php echo $grafik['graph_by_toefl']['L']?> },
                { name: 'Tidak Lulus', y: <?php echo $grafik['graph_by_toefl']['TL']?> },

            ]
        }]
    });



</script>

