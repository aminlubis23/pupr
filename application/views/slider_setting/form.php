<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'slider_setting'?>">SLIDER SETTING</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Modul Pengaturan Slider) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">
                    <form role="form" method="post" action="<?php echo base_url('slider_setting/process') ?>" enctype="multipart/form-data">
                        
                        <label>Judul</label><br />
                        <input type="text" class="form-control" id="judul" name="judul" value="<?php echo isset($value->judul)?$value->judul:set_value('judul')?>"/>
                        <?php echo form_error('judul'); ?>
                        <br />

                        <label>Sumber</label><br />
                        <input type="text" class="form-control" id="link_berita" name="link_berita" value="<?php echo isset($value->link_berita)?$value->link_berita:set_value('link_berita')?>"/>
                        <?php echo form_error('link_berita'); ?>
                        <br />

                        <label>Image Slider</label><br />
                        <input type="file" id="uploadfile" name="gambar" class="form-control"/>
                        <br />
                        <?php 
                        if(isset($value)){
                            echo 'Klik <a href="'.base_url().'assets/upload/image/'.$value->gambar.'" target="_blank"><i>disini</i></a> untuk melihat lampiran';
                        }

                        ?>
                        <br>
                        <label>Isi Berita</label><br />
                        <textarea class="ckeditor" id="ckedtor" name="isi_berita"><?php echo isset($value->isi_berita)?$value->isi_berita:''?></textarea>
                        <br />

                        <!-- btn submit -->
                        <center>
                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($value->id_slider)?$value->id_slider:''?>"/>
                        <a href="<?php echo base_url().'slider_setting'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>

                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->
