<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>

<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $('select[name="search_by"]').change(function () {
    
        if ($(this).val() == 1) {
            /*pencarian berdasarkan formasi*/
            $('#search_by_formasi').show('fast');
            $('#search_by_lokasi').hide('fast');
            $('#search_by_status').hide('fast');
            $('#search_by_univ').hide('fast');
            $('#search_by_majors').hide('fast');
            $('#search_by_field').hide('fast');
        } else if ( $(this).val() == 2 ) {
            $('#search_by_lokasi').show('fast');
            $('#search_by_formasi').hide('fast');
            $('#search_by_status').hide('fast');
            $('#search_by_univ').hide('fast');
            $('#search_by_majors').hide('fast');
            $('#search_by_field').hide('fast');
        } else if ( $(this).val() == 3 ) {
            $('#search_by_status').show('fast');
            $('#search_by_formasi').hide('fast');
            $('#search_by_lokasi').hide('fast');
            $('#search_by_univ').hide('fast');
            $('#search_by_majors').hide('fast');
            $('#search_by_field').hide('fast');
        } else if ( $(this).val() == 4 ) {
            $('#search_by_univ').show('fast');
            $('#search_by_majors').show('fast');
            $('#search_by_formasi').hide('fast');
            $('#search_by_lokasi').hide('fast');
            $('#search_by_status').hide('fast');
            $('#search_by_field').hide('fast');
        } else if ( $(this).val() == 5 ) {
            $('#search_by_field').show('fast');
            $('#search_by_formasi').hide('fast');
            $('#search_by_lokasi').hide('fast');
            $('#search_by_status').hide('fast');
            $('#search_by_univ').hide('fast');
            $('#search_by_majors').hide('fast');
        }else{
            $('#search_by_field').hide('fast');
            $('#search_by_formasi').hide('fast');
            $('#search_by_lokasi').hide('fast');
            $('#search_by_status').hide('fast');
            $('#search_by_univ').hide('fast');
            $('#search_by_majors').hide('fast');
        }   
    });

    $('select[name="select_field"]').change(function () {
        if ($(this).val() == 'dp_agama') {
            /*pencarian berdasarkan formasi*/
            $('#div_agama').show('fast');
            $('#div_status_nikah').hide('fast');
            $('#div_keyword').hide('fast');
        } else if ( $(this).val() == 'dp_status_nikah' ) {
            $('#div_agama').hide('fast');
            $('#div_status_nikah').show('fast');
            $('#div_keyword').hide('fast');
        }else{
            $('#div_agama').hide('fast');
            $('#div_status_nikah').hide('fast');
            $('#div_keyword').show('fast');
        }
    });

    $('select[name="pend_universitas"]').change(function () {
        if ($(this).val()) {
            $.getJSON("<?php echo site_url('mst_university/get_detail_univ') ?>/" + $(this).val(), '', function (data) {
                $('#detail_university').html('<span style="color:green;">Kategori : <i>Perguruan Tinggi "'+data.univ_status+'"</i> Akreditasi <i>"'+data.univ_akreditasi+'"</i></span><br><br>');
            });

            $.getJSON("<?php echo site_url('mst_university/getMajorsByUniversity') ?>/" + $(this).val(), '', function (data) {
                $('#pend_jurusan option').remove();
                $('<option value="">-Pilih Jurusan-</option>').appendTo($('#pend_jurusan'));
                $.each(data, function (i, o) {
                    $('<option value="' + o.majors_id + '">' + o.majors_name + '</option>').appendTo($('#pend_jurusan'));
                });

            });

        } else {
            $('#detail_university').remove();
            $('#pend_jurusan option').remove();
        }
    });

    $('select[name="pend_jurusan"]').change(function () {
        var univ_id = $('select[name="pend_universitas"]').val();
        if ($(this).val()) {
            $.getJSON("<?php echo site_url('mst_majors/get_detail_majors') ?>/" + $(this).val() + '/'+ univ_id, '', function (data) {
                $('#detail_majors').html('<span style="color:green;"> Akreditasi Prodi <i>"'+data.akreditasi+'"</i></span><br><br>');
            });


        } else {
            $('#detail_majors').remove();
        }
    });

    $("#btn_verifikasi_selected_item").click(function(event){

        event.preventDefault();
        var searchIDs = $("#dynamic-table input:checkbox:checked").map(function(){
        return $(this).val();
        }).toArray();
        
        count = searchIDs.length;
        if(count > 0){
            /*action here*/
            $('#field_verifikasi').show('fast');
            $('#myModalShow').modal('show');
            $('#array_selected_id').val(searchIDs);

            $.post("verifikasi_data_cpns/getDetailFromSelectedData", { arr_id: searchIDs },function(data, status){
                var obj = JSON.parse(data);
                $('#detailSelected').html(obj.html);

            });
             
        }else{
            alert('Tidak ada item yang dipilih !'); return false;
        }

        console.log(searchIDs);
    });

    $("#btn_submit_verifikasi").click(function(event){

        event.preventDefault();
        var searchIDs = $("#dynamic-table input:checkbox:checked").map(function(){
        return $(this).val();
        }).toArray();
        
        var postData = {
            arr_id : searchIDs,
            status : $('#verifikasi_status').val(),
            catatan : $('#verifikasi_catatan_kelengkapan_data').val(),
        };
        $.post("verifikasi_data_cpns/processQuickVerified", postData ,function(data, status){
            
            var jsonResponse = JSON.parse(data);
            if(status === 'success'){
                $.achtung({message: jsonResponse.message, timeout:5});
                $('#myModalShow').modal('toggle');
                reload_table();
            }else{
                $.achtung({message: jsonResponse.message, timeout:5});
            }
            achtungHideLoader();

        });

        console.log(data);
    });



});

function reviewUlang(dp_id){
    $.post("verifikasi_data_cpns/processReviewUlang", {ID:dp_id} ,function(data, status){
            
        var jsonResponse = JSON.parse(data);
        if(status === 'success'){
            $.achtung({message: jsonResponse.message, timeout:5});
            reload_table();
        }else{
            $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();

    });
}

</script>
<!-- Modal -->

<div id="myModalShow" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalShow" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Quick Verifikasi</h4>
      </div>
      <div class="modal-body">
        <p>
            <form class="form-horizontal" id="form_verifikasi" action="#" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                  <label class="control-label col-md-2">Hasil Verifikasi</label>
                  <div class="col-md-5">
                    <?php echo $this->master->custom_selection(array('table' => 'mst_status_verifikasi', 'id' => 'sv_id', 'name' => 'sv_name', 'where' => array() ), '' ,'verifikasi_status','verifikasi_status','form-control','','')?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Catatan</label>
                  <div class="col-md-8">
                    <input type="hidden" class="form-control" name="array_id" id="array_selected_id" value="">
                    <input type="text" class="form-control" name="verifikasi_catatan_kelengkapan_data" id="verifikasi_catatan_kelengkapan_data">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Verifikator</label>
                  <div class="col-md-3" style="padding-top:7px">
                    <b><?php echo $this->session->userdata('user')->nama?></b>
                  </div>
                  <label class="control-label col-md-1">Tanggal</label>
                  <div class="col-md-4" style="padding-top:7px">
                    <b><?php echo $this->tanggal->formatDateTime(date('Y-m-d H:i:s'))?></b>
                  </div>
                </div>
                <br>

                <div id="detailSelected"></div>

                <div class="col-md-12">
                    <div class="alert alert-warning center">
                        <b>Peringatan ! </b> <br>
                        Form ini merupakan <b><i>Verifikasi Kelengkapan Data dan Dokumen</i></b> Jika hasil verifikasi <b>Belum Memenuhi Syarat</b> maka peserta tidak dapat lanjut ke proses berikutnya <br> ~ <?php echo $this->session->userdata('user')->nama?> ~ <br>
                        <small style="font-size:11px !important">(Tanggal Verifikasi <?php echo date('d/m/Y')?>)</small>
                    </div>
                </div>


            </form>

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn_submit_verifikasi">Submit</button>
      </div>
    </div>
  </div>
</div>

<!-- End Modal -->



<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>VERIFIKASI DATA CPNS </strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul Verifikasi Data CPNS) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">
                <!-- add btn -->
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" id="form_search" action="#" method="POST" enctype="multipart/form-data">

                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#5_coll"> <i class="fa fa-search"></i> FORM PENCARIAN DATA <i class="fa fa-angle-double-right"></i> <small style="color:white; font-style: italic">Silahkan lakukan pencarian data disini</small></a>
                                    </h4>
                                  </div>
                                  <div id="5_coll" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="form-group">
                                          <label class="control-label col-md-2">Pencarian Berdasdarkan</label>
                                          <div class="col-md-3">
                                            <select class="form-control" id="search_by" name="search_by">
                                                <option value="">-Pilih-</option>
                                                <option value="1">Formasi</option>
                                                <option value="2">Lokasi Penempatan</option>
                                                <option value="3">Status Pelamar</option>
                                                <option value="4">Universitas</option>
                                                <option value="5">Field Table</option>
                                            </select>
                                          </div>
                                        </div>

                                        <div class="form-group" style="display:none" id="search_by_formasi">
                                          <label class="control-label col-md-2">Formasi</label>
                                          <div class="col-md-3">
                                            <?php echo $this->master->custom_selection(array('table' => 'mst_formasi_jenis', 'id' => 'formasi_jenis_id', 'name' => 'formasi_jenis_name', 'where' => array() ), '' ,'formasi_jenis','formasi_jenis','form-control','','')?>
                                          </div>
                                        </div>

                                        <div class="form-group" style="display:none" id="search_by_lokasi">
                                          <label class="control-label col-md-2">Lokasi Penempatan</label>
                                          <div class="col-md-3">
                                            <?php echo $this->master->custom_selection(array('table' => 'mst_unit_penempatan', 'id' => 'up_id', 'name' => 'up_name', 'where' => array() ), '' ,'up_id','up_id','form-control','','')?>
                                          </div>
                                        </div>

                                        <div class="form-group" style="display:none" id="search_by_status">
                                          <label class="control-label col-md-2">Status Pelamar</label>
                                          <div class="col-md-4">
                                            <?php echo $this->master->custom_selection(array('table' => 'mst_status_verifikasi', 'id' => 'sv_id', 'name' => 'sv_name', 'where' => array() ), '' ,'sv_id','sv_id','form-control','','')?>
                                          </div>
                                        </div>

                                        <div class="form-group" style="display:none" id="search_by_field">

                                            <label class="control-label col-md-2">Field</label>
                                            <div class="col-md-3">
                                                <select class="form-control" id="select_field" name="select_field">
                                                    <option value="">-Pilih-</option>
                                                    <option value="dp_nik">NIK</option>
                                                    <option value="dp_nama_lengkap">Nama Peserta</option>
                                                    <option value="dp_no_telp">No Telp/No HP</option>
                                                    <option value="dp_alamat_lengkap">Alamat</option>
                                                    <option value="dp_agama">Agama</option>
                                                    <option value="dp_status_nikah">Status Nikah</option>
                                                </select>
                                            </div>

                                            <div id="div_keyword" style="display:none">
                                              <label class="control-label col-md-2">Masukan Kata Kunci</label>
                                              <div class="col-md-3">
                                                <input type="text" name="keyword" id="keyword" class="form-control">
                                              </div>
                                            </div>

                                            <div id="div_agama" style="display:none">
                                              <label class="control-label col-md-1">Agama</label>
                                              <div class="col-md-3">
                                                <?php echo $this->master->custom_selection(array('table' => 'mst_religion', 'id' => 'religion_id', 'name' => 'religion_name', 'where' => array() ), '' ,'dp_agama','dp_agama','form-control','','')?>
                                              </div>
                                            </div>

                                            <div id="div_status_nikah" style="display:none">
                                              <label class="control-label col-md-2">Status Nikah</label>
                                              <div class="col-md-3">
                                                <?php echo $this->master->custom_selection(array('table' => 'mst_marital_status', 'id' => 'ms_id', 'name' => 'ms_name', 'where' => array() ), '' ,'dp_status_nikah','dp_status_nikah','form-control','','')?>
                                              </div>
                                            </div>


                                        </div>

                                        <div class="form-group" style="display:none" id="search_by_univ">
                                          <label class="control-label col-md-2">Universitas</label>
                                          <div class="col-md-4">
                                            <?php echo $this->master->custom_selection(array('table' => 'mst_university', 'id' => 'univ_id', 'name' => 'univ_name', 'where' => array() ), '' ,'pend_universitas','pend_universitas','form-control','','')?><br>
                                            <div id="detail_university"></div>
                                          </div>
                                          <label class="control-label col-md-1">Jurusan</label>
                                          <div class="col-md-4">
                                            <?php echo $this->master->custom_selection(array('table' => 'mst_majors', 'id' => 'majors_id', 'name' => 'majors_name', 'where' => array() ), '' ,'pend_jurusan','pend_jurusan','form-control','','')?><br>
                                            <div id="detail_majors"></div>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <label class="control-label col-md-2">&nbsp;</label>
                                          <div class="col-md-10">
                                            <button type="button" id="btn_reset_data" class="mb-xs mt-xs mr-xs btn btn-sm btn-warning"><i class="fa fa-history"></i> Reset </button><button type="button" id="btn_search_data" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary"><i class="fa fa-search"></i> Pencarian </button><button type="button" id="btn_export_excel" class="mb-xs mt-xs mr-xs btn btn-sm btn-success"><i class="fa fa-download"></i> Export to Excel </button>
                                          </div>
                                        </div>

                                        


                                    </div>
                                  </div>
                                </div>
                            
                            </div> 

                        </form>    
                    </div>
                    
                </div>

                <div class="col-md-12">
                    <small style="font-size:11px !important; font-style: italic; font-weight: bold">Here action for selected items</small><br>
                    <button type="button" id="btn_verifikasi_selected_item" class="mb-xs mt-xs mr-xs btn btn-sm btn-danger"><i class="fa fa-eye"></i> Verifikasi data pelamar yang dipilih</button>
                    <!-- <button type="button" id="btn_review_selected_item" class="mb-xs mt-xs mr-xs btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Review Ulang  </button> -->
                </div>
                <hr class="separator">
                <!-- content data table -->
                <div class="col-md-12">
                    <table id="dynamic-table" base-url="verifikasi_data_cpns" class="table table-striped table-bordered" >
                        <thead>
                        <tr>
                            <th style="color: #242424; text-align: center; width:30px; "><input type="checkbox" name="" id="checked_all"></th>
                            <th>Pas Foto</th>
                            <th style="color: #242424;">Biodata Diri</th>
                            <th style="color: #242424;">Kontak Informasi</th>
                            <th style="color: #242424;">Pendidikan dan Akreditasi PT/Prodi</th>
                            <th style="color: #242424;">Kemampuan Bahasa Inggris</th>
                            <th style="color: #242424;">Jenis Formasi/ Kualifikasi Pendidikan / Formasi Jabatan</th>
                            <th style="color: #242424;" width="160px">Kelengkapan Persyaratan Dokumen</th>
                            <!-- <th style="color: #242424;">Status</th> -->
                            <th style="color: #242424;">Action / Status Akhir</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>js/als_datatable.js"></script>
<!-- End Main -->

