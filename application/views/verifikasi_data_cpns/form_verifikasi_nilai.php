<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<script src="<?php echo base_url ().'js/jquery.min.js' ?>"></script>

<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- achtung loader -->
<link href="<?php echo base_url()?>assets/achtung/ui.achtung-mins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/ui.achtung-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/achtung/achtung.js"></script> 
<script>
$(document).ready(function(){
  
    $( "#form_verifikasi_nilai" ).submit(function( event ) {
        var form = $( this )[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        

        $.ajax({
        url: $( this ).attr('action'),
        type: "post",
        data: formData,
        dataType: "json",
        contentType: false, 
        processData: false,
        beforeSend: function() {
          achtungShowLoader();  
        },
        uploadProgress: function(event, position, total, percentComplete) {
        },
        complete: function(xhr) {   

          var data=xhr.responseText;
          var jsonResponse = JSON.parse(data);
          if(jsonResponse.status === 200){
            $.achtung({message: jsonResponse.message, timeout:5});
            window.location.href = jsonResponse.redirect;
          }else{
            $.achtung({message: jsonResponse.message, timeout:5});
          }
          achtungHideLoader();
        }

        });

        event.preventDefault();
        });
});

</script>

<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-title">
                        <strong>VERIFIKASI DATA CPNS </strong> <i class="fa fa-angle-double-right"></i>
                        <small> <i>(Modul Verifikasi Data CPNS) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <form class="form-horizontal" id="form_verifikasi_nilai" action="<?php echo base_url().'verifikasi_data_cpns/process_verifikasi_nilai'?>" method="POST" enctype="multipart/form-data">

                <!-- page content -->
                <div class="row page-content">
                    <!-- add btn -->
                    <div class="col-md-12">
                        <div class="row">
                            <br>
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#5_coll" style="color:white"> <i class="fa fa-user"></i> Lihat Biodata Selengkapnya <i class="fa fa-angle-double-right"></i> </a>
                                    </h4>
                                  </div>
                                  <div id="5_coll" class="panel-collapse collapse">
                                    <div class="panel-body">

                                       <div class="row">
                                        <div class="col-md-12">
                                            <!-- block title -->
                                            <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-user"></i> Biodata Diri</h4></div>
                                            <br>
                                            <!-- end block title -->

                                            <!-- foto profile -->
                                            <div class="col-md-2">
                                                <img src="<?php echo isset($file[2])?base_url().'uploaded_files/data_pelamar/'.$file[2].'':base_url().'assets/admin/images/no_photo.jpg'?>" class="img-responsive" alt="" style="width: 100%" /><br>
                                            </div>
                                            <!-- end foto profile -->

                                            <!-- block biodata diri -->
                                            <div class="col-md-5">
                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Nama Lengkap</th>
                                                        <td><?php echo $value->dp_nama_lengkap?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">No KK</th>
                                                        <td><?php echo $value->dp_no_kk?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Jenis Kelamin</th>
                                                        <td><?php echo $value->jk?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Status Nikah</th>
                                                        <td><?php echo $value->nama_status_kawin?></td>
                                                    </tr>

                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Lampiran KTP</th>
                                                        <td><a href="<?php echo isset($file[1])?base_url().'uploaded_files/data_pelamar/'.$file[1].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Lampiran KK</th>
                                                        <td><a href="<?php echo isset($file[4])?base_url().'uploaded_files/data_pelamar/'.$file[4].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                                    </tr>

                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Surat Pernyataan</th>
                                                        <td><a href="<?php echo isset($file[3])?base_url().'uploaded_files/data_pelamar/'.$file[3].'':'#'?>" target="_blank" style="color:red"><i>Download</i></a></td>
                                                    </tr>

                                                </table>

                                            </div>
                                            
                                            <div class="col-md-5">

                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">NIK</th>
                                                        <td><?php echo $value->dp_nik?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Tempat Lahir</th>
                                                        <td><?php echo $value->dp_tempat_lahir?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Tanggal Lahir</th>
                                                        <td><?php echo $this->tanggal->formatDate($value->dp_tanggal_lahir)?></td>
                                                    </tr>
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Agama</th>
                                                        <td><?php echo $value->nama_agama?></td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- block title -->
                                            <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-info"></i> Kontak Informasi</h4></div>
                                            <br>
                                            <!-- end block title -->
                                            <div class="col-md-12">

                                            <table class="table">
                                                <tr>
                                                    <th style="background-color:#f1ab0de0;color:black">Alamat Lengkap</th>
                                                    <th style="background-color:#f1ab0de0;color:black">Alamat Domisili</th>
                                                    <th style="background-color:#f1ab0de0;color:black">No Telp</th>
                                                    <th style="background-color:#f1ab0de0;color:black">Email</th>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $value->dp_alamat_lengkap?></td>
                                                    <td><?php echo $value->dp_alamat_org_tua?></td>
                                                    <td><?php echo $value->dp_no_telp?></td>
                                                    <td><?php echo $value->dp_email?></td>
                                                </tr>
                                            </table>

                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- block title -->
                                            <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-list"></i> Formasi</h4></div>
                                            <br>
                                            <!-- end block title -->
                                            <div class="col-md-12">

                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Kode Formasi</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Kualifikasi Pendidikan</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Jenis Formasi</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Jabatan</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Penempatan</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $value->formasi_kode?></td>
                                                        <td><?php echo $value->kp_name?></td>
                                                        <td><?php echo $value->formasi_jenis_name?></td>
                                                        <td><?php echo $value->fj_name?></td>
                                                        <td><?php echo $value->up_name?></td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- block title -->
                                            <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-graduation-cap"></i> Pendidikan</h4></div>
                                            <br>
                                            <!-- end block title -->
                                            <div class="col-md-12">

                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Universitas</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Jurusan</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Tanggal Ijasah</th>
                                                        <th style="background-color:#f1ab0de0;color:black">IPK (Grade)</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Nomor Ijasah</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Gelar Sarjana</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $value->univ_name?></td>
                                                        <td><?php echo $value->majors_name?></td>
                                                        <td><?php echo $this->tanggal->formatDate($value->pend_tgl_ijasah)?></td>
                                                        <td>
                                                            <?php 
                                                                $ver_ipk = $this->konfigurasi_model->verifikasi_ipk($value->pend_ipk);
                                                                 $ipk = ( $ver_ipk )?'<span style="font-size:14px; color:green; font-weight:bold">'.$value->pend_ipk.' (Memenuhi Syarat)</span>':'<span style="font-size:14px; color:red; font-weight:bold">'.$value->pend_ipk.' (Belum Memenuhi Syarat)</span>';

                                                            echo $ipk?>
                                                                
                                                        </td>
                                                        <td><?php echo $value->pend_no_ijasah?></td>
                                                        <td><?php echo $value->pend_gelar_sarjana?></td>
                                                    </tr>
                                                </table>

                                            </div>
                                            
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- block title -->
                                            <div style="background-color: #0088cc; height:50px; padding-bottom:10px"><h4 class="text-title"><i class="fa fa-certificate"></i> Kemampuan Bahasa Inggris</h4></div>
                                            <br>
                                            <!-- end block title -->
                                            <div class="col-md-12">
                                                <table class="table">
                                                    <tr>
                                                        <th style="background-color:#f1ab0de0;color:black">Jenis TOEFL</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Tanggal Sertifikat</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Lokasi Tes TOEFL</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Nilai TOEFL</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Nomor Sertifikat</th>
                                                        <th style="background-color:#f1ab0de0;color:black">Tanggal Tes</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $value->bhs_jenis_toefl?></td>
                                                        <td><?php echo $this->tanggal->formatDate($value->bhs_tanggal_sertifikat)?></td>
                                                        <td><?php echo $value->bhs_lokasi_tes?></td>
                                                        <td>
                                                            <?php 
                                                                $ver_toefl = $this->konfigurasi_model->verifikasi_toefl($value->bhs_nilai_toefl);
                                                                $toefl = ( $ver_toefl )?'<span style="font-size:14px; color:green; font-weight:bold">'.$value->bhs_nilai_toefl.' (Memenuhi Syarat)</span>':'<span style="font-size:14px; color:red; font-weight:bold">'.$value->bhs_nilai_toefl.' (Belum Memenuhi Syarat)</span>';
                                                                echo $toefl?>
                                                                                                                
                                                        </td>
                                                        <td><?php echo $value->bhs_no_sertifikat?></td>
                                                        <td><?php echo $this->tanggal->formatDate($value->bhs_tgl_tes_toefl)?></td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                    <br>
                                    <center>
                                        <a data-toggle="collapse" class="btn btn-danger" data-parent="#accordion" href="#5_coll"> <i class="fa fa-times-o"></i> Close Toogle</a>
                                    </center>
                                        

                                    </div>
                                  </div>
                                </div>
                            
                            </div> 
                        </div>
                        
                    </div>

                    <!-- content data table -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#6_coll" style="background-color: #305d08; color:white;height:50px; padding-bottom:10px" ><i class="fa fa-list"></i> Hasil Verifikasi Kelengkapan Data <i class="fa fa-angle-double-right"></i> </a>
                                    </h4>
                                  </div>
                                  <div id="6_coll" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                         
                                        <div class="col-md-12" style="background-color:#e8eaea52; padding-top:10px; margin-bottom:10px">
                                            <div class="col-md-6">
                                                <label>Nilai IPK</label><br />
                                                <?php 
                                                    $ver_ipk = $this->konfigurasi_model->verifikasi_ipk($value->pend_ipk);
                                                     $ipk = ( $ver_ipk )?'<div class="alert alert-success">
                                                                <b style="color:green">Memenuhi Syarat </b>
                                                                Nilai IPK '.$value->pend_ipk.' (Memenuhi nilai minimum)<br>
                                                            </div>':'<div class="alert alert-danger">
                                                            <b style="color:red">Tidak Memenuhi Syarat </b>
                                                            Nilai IPK '.$value->pend_ipk.' (Kurang dari nilai minimum)<br>
                                                        </div>';

                                                echo $ipk?>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Nilai TOEFL</label><br />
                                                <?php 
                                                    $ver_toefl_2 = $this->konfigurasi_model->verifikasi_toefl($value->bhs_nilai_toefl);
                                                     $show_toefl = ( $ver_toefl_2 )?'<div class="alert alert-success">
                                                                <b style="color:green">Memenuhi Syarat </b>
                                                                Nilai TOEFL '.$value->bhs_nilai_toefl.' (Memenuhi nilai minimum)<br>
                                                            </div>':'<div class="alert alert-danger">
                                                            <b style="color:red">Tidak Memenuhi Syarat </b>
                                                            Nilai TOEFL '.$value->bhs_nilai_toefl.' (Kurang dari nilai minimum)<br>
                                                        </div>';

                                                echo $show_toefl?>
                                            </div>

                                            <div class="col-md-12">
                                                <label>Catatan Hasil Verifikasi Sistem</label><br />
                                                <?php echo isset($value->verifikasi_catatan_hasil_sistem)?'<i>'.$value->verifikasi_catatan_hasil_sistem.'</i>':''?><br><br>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Apakah merupakan PTT (Pegawai Tidak Tetap) ? </label><br />
                                                <?php echo isset($value->verifikasi_is_ptt)?($value->verifikasi_is_ptt=='Y')?'<b>Ya</b>':'<b>Tidak</b>':''?> 
                                                
                                            </div>

                                            <div class="col-md-4">
                                                <label>Hasil Verifikasi Kelengkapan Data</label><br />
                                                <b><?php echo isset($value->sv_name)?strtoupper($value->sv_name):''?> <br /></b>
                                                
                                            </div>
                                            <div class="col-md-4">
                                                <label>Catatan </label><br />
                                                <?php echo isset($value->verifikasi_catatan_kelengkapan_data)?'<i>'.$value->verifikasi_catatan_kelengkapan_data.'</i>':''?>
                                            </div>

                                        </div>

                                    <br>
                                    <center>
                                        <a data-toggle="collapse" class="btn btn-danger" data-parent="#accordion" href="#6_coll"> <i class="fa fa-times-o"></i> Close Toogle</a>
                                    </center>
                                        

                                    </div>
                                  </div>
                                </div>
                            
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#7_coll" style="background-color: #e40f0f; color:white;height:50px; padding-bottom:10px" ><i class="fa fa-edit"></i> Verifikasi Nilai SKD/SKB <i class="fa fa-angle-double-right"></i> </a>
                                    </h4>
                                  </div>
                                  <div id="7_coll" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                         
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label>Hasil Ujian SKD</label><br />
                                                <select name="verifikasi_ujian_skd" class="form-control">
                                                    <option value="">-Silahkan Pilih-</option>
                                                    <option value="L" <?php echo isset($value->verifikasi_ujian_skd)?($value->verifikasi_ujian_skd=='L')?'selected':'':''?> >Lulus</option>
                                                    <option value="TL" <?php echo isset($value->verifikasi_ujian_skd)?($value->verifikasi_ujian_skd=='TL')?'selected':'':''?>>Tidak Lulus</option>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Hasil Ujian SKB</label><br />
                                                <select name="verifikasi_ujian_skb" class="form-control">
                                                    <option value="">-Silahkan Pilih-</option>
                                                    <option value="L" <?php echo isset($value->verifikasi_ujian_skb)?($value->verifikasi_ujian_skb=='L')?'selected':'':''?>>Lulus</option>
                                                    <option value="TL" <?php echo isset($value->verifikasi_ujian_skb)?($value->verifikasi_ujian_skb=='TL')?'selected':'':''?>>Tidak Lulus</option>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Hasil Akhir</label><br />
                                                <select name="verifikasi_hasil_akhir" class="form-control">
                                                    <option value="">-Silahkan Pilih-</option>
                                                    <option value="L" <?php echo isset($value->verifikasi_hasil_akhir)?($value->verifikasi_hasil_akhir=='L')?'selected':'':''?>>Lulus</option>
                                                    <option value="TL" <?php echo isset($value->verifikasi_hasil_akhir)?($value->verifikasi_hasil_akhir=='TL')?'selected':'':''?> >Tidak Lulus</option>
                                                </select><br>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="alert alert-warning center">
                                                    <b>Peringatan ! </b> <br>
                                                    Form ini merupakan <b><i>HASIL AKHIR</i></b> Proses Rekrutmen CPNS pada Kementerian PUPR <br> Dengan proses akhir ini, hasil tidak dapat diubah kembali atau tidak dapat diganggu gugat kembali. <br> ~ <?php echo $this->session->userdata('user')->nama?> ~ <br>
                                                    <small style="font-size:11px !important">(Tanggal Verifikasi <?php echo date('d/m/Y')?>)</small>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <center>
                                                    <input type="hidden" name="dp_id" value="<?php echo $value->dp_id?>">
                                                    <?php if( in_array($this->session->userdata('user')->level, array(0,2))) :?>
                                                    <a href="<?php echo base_url().'verifikasi_data_cpns'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                                                    &nbsp;
                                                    <?php endif;?>
                                                    <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>
                                                </center>
                                            </div>

                                        </div>

                                    </div>
                                  </div>
                                </div>
                            
                            </div> 
                        </div>
                        <br>
                    </div>

                    <!-- end content data table -->

                </div>
                <!-- end page content -->


            </form>
        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->


