<link rel="stylesheet" href="<?php echo base_url () ?>assets/als_custom.css">
<!--main-->
<div role="main" class="main">

    <div class="container">

        <div class="row">

            <!-- page header form -->
            <div class="row">
                <div class="col-md-6">
                    <h3 class="form-title">
                        <strong><a href="<?php echo base_url().'dokumen_admin'?>">DOKUMEN UPLOAD</a> </strong> <i class="fa fa-angle-double-right"></i>
                        <small> Form <i class="fa fa-angle-double-right"></i> <i>(Modul Upload Dokumen) </i></small>
                    </h3>
                </div>
            </div>
            <!-- end page header form -->

            <!-- page content -->
            <div class="row page-content">

                <!-- content data table -->
                <div class="col-md-12">
                    <form role="form" method="post" action="<?php echo base_url('dokumen_admin/process') ?>" enctype="multipart/form-data">
                        
                        <label>Nama Dokumen</label><br />
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo isset($value->name)?$value->name:''?>"/>
                        <?php echo form_error('name'); ?>

                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($value->id)?$value->id:''?>"/>
                        <br />

                        <label>Lampiran File</label><br />
                        <input type="file" id="uploadfile" name="image" class="form-control"/>
                        <br />
                        <!-- btn submit -->
                        <center>
                        
                        <a href="<?php echo base_url().'dokumen_admin'?>" type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" >Cancel</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>

                        </center>
                    </form>
                </div>
                <!-- end content data table -->

            </div>
            <!-- end page content -->

        </div>

    </div>

</div>

<!-- footer  -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>
        </div>
    </div>
</footer>
<!-- end footer -->
