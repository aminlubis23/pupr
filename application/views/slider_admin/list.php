<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 30/07/2018
 * Time: 14:43
 */
?>

<!--<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?php echo base_url() ?>assets/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        },
        selector: "#keterangan",
        height: 150,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
</script>-->

<?php
$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');

// Notifikasi
if($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

// Error
if(isset($error)) {
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

// Validasi
echo validation_errors('<div class="alert alert-success">','</div>');

?>

<!--main-->
<div role="main" class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br />
                <h3 style="text-align: center">
                    <strong>Slider Admin Side</strong>
                </h3>
                <div class="row">
                    <div class="col-md-3"></div>
                    <!--<form role="form" method="post" action="<?php echo base_url('slider_admin/list') ?>" id="slider_admin" name="slider_admin">-->
                    <?php // Form
                    echo form_open_multipart('slider_admin/tambah'); ?>
                    <div class="col-md-6">
                        <label>Title</label><br />
                        <input type="text" class="form-control" id="judul" name="judul" />
                        <br />
                        <label>Image Slider</label><br />
                        <input type="file" id="gambar" name="gambar" title="" class="form-control"/>
                        <div class="pull-right" style="font-size: small">*Maximum file size 5mb</div> | 
                        <div class="pull-right" style="font-size: small">*Maximum image size 1920 x 550</div>
                        <br />
                        <label>Tanggal</label><br />
                        <input type="date" name="tanggal" id="tanggal" class="form-control" />
                        <br />
                        <label>Link Berita</label><br />
                        <input type="text" class="form-control" id="link_berita" name="link_berita" /><br />

                        <label>Isi Berita</label><br />
                        <!--<textarea rows="10" class="form-control" id="isi_berita" name="isi_berita"></textarea>-->
                        <textarea rows="10" class="ckeditor" id="isi_berita" name="isi_berita"></textarea>

                        <br />

                        <button type="submit" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave" name="btnsave" >Save</button>

                    </div>
                    <!--</form>-->
                    <?php echo form_close() ?>
                    <div class="col-md-3"></div>
                </div>

                <div class="row" style="background-color:white">
                    <br />
                    <div class="col-md-12" style="background-color: white; overflow-x: auto; overflow-y: auto">
                        <table id="tableview" class="table table-striped table-bordered" style="width: 100%; background-color: white">
                            <thead>
                            <tr>
                                <th style="color: #242424; text-align: center">Nomer</th>
                                <th style="color: #242424; text-align: center">Judul</th>
                                <th style="color: #242424; text-align: center">Gambar</th>
                                <th style="color: #242424; text-align: center">Tanggal </th>
                                <th style="color: #242424; text-align: center">Link Berita </th>
                                <th style="color: #242424; text-align: center">Isi Berita </th>
                                <th style="color: #242424; text-align: center">Users</th>
                                <th style="color: #242424; text-align: center">Date Update</th>
                                <th style="color: #242424; text-align: center">Remarks</th>
                                <th style="color: #242424; text-align: center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach($slider as $slider) { ?>
                            <tr>
                                <td>
                                    <label style="color: #242424" id="lbltgl"><?php echo $slider->id_slider ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lblno"><?php echo $slider->judul ?></label>
                                </td>
                                <td>
                                    <img src="<?php echo base_url('assets/upload/image/'.$slider->gambar) ?>" class="img img-responsive" width="200" id="slider1">
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbltgl"><?php echo $slider->create_date ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lbllinkberita"><?php echo $slider->link_berita ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424; width: 500px" id="lblisiberita"><?php echo $slider->isi_berita ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424; width:150px" id="lbluserupload"><?php echo $slider->create_by ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424; width:150px" id="lbldateupload"><?php echo $slider->update_date ?></label>
                                </td>
                                <td>
                                    <label style="color: #242424" id="lblremarks"><?php echo $slider->remarks ?></label>
                                </td>
                                <td style="text-align: center">
                                    <button type="button" id="btndelete1" class="btn btn-danger mr-xs mb-sm">
                                        <a href="<?php echo base_url('slider_admin/delete/'.$slider->id_slider) ?>"class="btn btn-primary btn-sm"><i class="fa fa-trash "></i></a></button>
                                </td>
                            </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>

        </div>
    </div>

</footer>

<!-- Vendor -->
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

<!-- Examples -->
<script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

<!-- datatable-->
<!--<script src="Datatable/jquery-3.3.1.js"></script>-->
<!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
<script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
<script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tableview').DataTable({
            "scrollX": true
        });
    });
</script>
