<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 31/07/2018
 * Time: 2:52
 */

$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');
?>

<!--main-->
<div class="main">
    <div class="container">
        <div class="main-grids">

            <div role="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <br />
                            <h3 style="text-align: left">
                                <strong>Tambah Statistik Berdasarkan Formasi Admin Content</strong>
                            </h3>
                            <div class="row" style="background-color: white">
                                <br />
                                <div class="col-md-8">
                                    <label>Category</label><br />
                                    <input type="text" class="form-control" id="txtcategory" />
                                    <br />
                                    <label>Data Formasi</label><br />
                                    <input type="text" class="form-control" id="txtdataformasi" />
                                    <br />
                                    <label>Data Pelamar</label><br />
                                    <input type="text" class="form-control" id="txtdatapelamar" />
                                    <br />
                                    <button type="button" class="btn btn-primary mr-xs mb-sm" style="width: 150px" id="btnsave">Save</button>
                                    &nbsp;
                                    <button type="button" class="btn btn-danger mr-xs mb-sm" style="width: 150px" id="btncancel">Cancel</button>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

            <!-- datatable-->
            <!--<script src="Datatable/jquery-3.3.1.js"></script>-->
            <!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
            <script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#tableview').DataTable();
                });
            </script>
        </div>
    </div>
</div>