<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 30/07/2018
 * Time: 15:04
 */

$site	= $this->konfigurasi_model->listing();
//include('pengumuman.php');
//include('tatacara.php');
//include('bantuan.php');
?>

<!--main-->
<div role="main" class="main">
    <div class="container">
        <div class="row">
            <br />
            <h3 style="text-align: left">
                <strong>Input Statistik Berdasarkan Jenis Formasi</strong>
            </h3>
            <div class="row" style="background-color:white"><br />
                <div class="col-md-12">
                    <table id="chartjenisformasi" class="table table-striped table-bordered" style="width: 100%; background-color: white">
                        <thead>
                        <tr>
                            <th style="color: #242424; text-align:center">Category/Formasi</th>
                            <th style="color: #242424; text-align:center">Pelamar</th>
                            <th style="color: #242424; text-align:center">Formasi </th>
                            <th style="color: #242424; text-align:center">User Update</th>
                            <th style="color: #242424; text-align:center">Date Update</th>
                            <th style="color: #242424; text-align:center">Remarks</th>
                            <th style="color: #242424; text-align:center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <label style="color: #242424" id="lblcatformasi1">Umum</label>
                            </td>

                            <td>
                                <label style="color: #242424" id="isiformasi1">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="isipelamarformasi1">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbluserupdate">Admin</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbldateupdate">13 Agustus 2019</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblremarks">Remarks</label>
                            </td>
                            <td style="text-align: center">
                                <a href="<?php echo base_url(); ?>statistik_admin/inputstatistikformasi" id="btnnewformasi" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>New</a>
                                <a href="inputstatistikformasi.html" id="btneditformasi" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-edit"></i>edit</a>
                                <a href="#" class="mb-xs mt-xs mr-xs btn btn-danger" id="btndeleteformasi"><i class="fa fa-trash"></i> trash</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label style="color: #242424" id="Label1">Cum Laude</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label2">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label3">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label4">Admin</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label5">13 Agustus 2019</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label6">Remarks</label>
                            </td>
                            <td style="text-align: center">
                                <a href="<?php echo base_url(); ?>statistik_admin/inputstatistikformasi" id="A1" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>New</a>
                                <a href="#" id="A2" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-edit"></i>edit</a>
                                <a href="#" class="mb-xs mt-xs mr-xs btn btn-danger" id="A3"><i class="fa fa-trash"></i> trash</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label style="color: #242424" id="Label7">Disabilitas</label>
                            </td>

                            <td>
                                <label style="color: #242424" id="Label8">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label9">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label10">Admin</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label11">13 Agustus 2019</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label12">Remarks</label>
                            </td>
                            <td style="text-align: center">
                                <a href="<?php echo base_url(); ?>statistik_admin/inputstatistikformasi" id="A4" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>New</a>
                                <a href="#" id="A5" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-edit"></i>edit</a>
                                <a href="#" class="mb-xs mt-xs mr-xs btn btn-danger" id="A6"><i class="fa fa-trash"></i> trash</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label style="color: #242424" id="Label13">Putra/i Papua</label>
                            </td>

                            <td>
                                <label style="color: #242424" id="Label14">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label15">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label16">Admin</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label17">13 Agustus 2019</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label18">Remarks</label>
                            </td>
                            <td style="text-align: center">
                                <a href="<?php echo base_url(); ?>statistik_admin/inputstatistikformasi" id="A7" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>New</a>
                                <a href="#" id="A8" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-edit"></i>edit</a>
                                <a href="#" class="mb-xs mt-xs mr-xs btn btn-danger" id="A9"><i class="fa fa-trash"></i> trash</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <br />
            <h3 style="text-align: left">
                <strong>Input Statistik Berdasarkan Jabatan</strong>
            </h3>
            <div class="row" style="background-color:white"><br />
                <div class="col-md-12">
                    <table id="chartjabatan" class="table table-striped table-bordered" style="width: 100%; background-color: white">
                        <thead>
                        <tr>
                            <th style="color: #242424; text-align:center">Category/Jabatan</th>
                            <th style="color: #242424; text-align:center">Pelamar</th>
                            <th style="color: #242424; text-align:center">Formasi </th>
                            <th style="color: #242424; text-align:center">User Update</th>
                            <th style="color: #242424; text-align:center">Date Update</th>
                            <th style="color: #242424; text-align:center">Remarks</th>
                            <th style="color: #242424; text-align:center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <label style="color: #242424" id="lblcatjabatan">Teknik Pengairan</label>
                            </td>

                            <td>
                                <label style="color: #242424" id="lblformasijabatan">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpelamarjabatan">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbluserupdatejabatan">Admin</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lbldateupdatejabatan">13 Agustus 2019</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblremarksjabatan">Remarks</label>
                            </td>
                            <td style="text-align: center">
                                <a href="<?php echo base_url(); ?>statistik_admin/inputstatistikjabatan" id="btnnewjabatan" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>New</a>
                                <a href="inputstatistikjabatan.html" id="btneditjabatan" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-edit"></i>edit</a>
                                <a href="#" class="mb-xs mt-xs mr-xs btn btn-danger" id="btndeletejabatan"><i class="fa fa-trash"></i> trash</a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <br />
            <h3 style="text-align: left">
                <strong>Input Statistik Berdasarkan Pendidikan / Chart Filtering</strong>
            </h3>
            <div class="row" style="background-color:white"><br />
                <div class="col-md-12" style="overflow-x: scroll;">
                    <table id="chartpendidikan" class="table table-striped table-bordered" style="width: 2700px; background-color: white">
                        <thead>
                        <tr>
                            <th style="color: #242424; text-align:center; width:150px">Pendidikan</th>
                            <th style="color: #242424; text-align:center; width:150px">Formasi Umum</th>
                            <th style="color: #242424; text-align:center; width:150px">Pelamar Umum </th>
                            <th style="color: #242424; text-align:center; width:150px">Formasi Cum Laude</th>
                            <th style="color: #242424; text-align:center; width:200px">Pelamar Cum Laude </th>
                            <th style="color: #242424; text-align:center; width:200px">Formasi Disabilitas</th>
                            <th style="color: #242424; text-align:center; width:200px">Pelamar Disabilitas </th>
                            <th style="color: #242424; text-align:center; width:200px">Formasi Putra/i Papua</th>
                            <th style="color: #242424; text-align:center; width:200px">Pelamar Putra/i Papua </th>
                            <th style="color: #242424; text-align:center; width:150px">User Update</th>
                            <th style="color: #242424; text-align:center; width:150px">Date Update</th>
                            <th style="color: #242424; text-align:center">Remarks</th>
                            <th style="color: #242424; text-align:center; width:270px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <label style="color: #242424" id="lblcatpendidikan">S1 Teknik Sipil</label>
                            </td>

                            <td>
                                <label style="color: #242424" id="lblformasiumum">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpelamarumum">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblformasicumlaude">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpelamarcumlaude">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblformasidisabilitas">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpelamardisabilitas">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblformasipapua">8</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="lblpelamarpapua">5</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label22">Admin</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label23">13 Agustus 2019</label>
                            </td>
                            <td>
                                <label style="color: #242424" id="Label24">Remarks</label>
                            </td>
                            <td style="text-align: center">
                                <a href="<?php echo base_url(); ?>statistik_admin/inputstatistikpendidikan" id="btnnewpendidikan" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-plus"></i>New</a>
                                <a href="inputstatistikpendidikan.html" id="A11" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-edit"></i>edit</a>
                                <a href="#" class="mb-xs mt-xs mr-xs btn btn-danger" id="A12"><i class="fa fa-trash"></i> trash</a>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a href="index.html" class="logo">
                        <img alt="Logo Kementerian PUPR" style="height:30px; width:120px;" class="img-responsive" src="<?php echo base_url () ?>assets/front/images/logopupr2.png">
                    </a>
                    <p style="color: #565656">© Copyright 2017. All Rights Reserved.</p>
            </div>
            </center>

        </div>
    </div>

</footer>

            <!-- Vendor -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.appear/jquery.appear.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery-cookie/jquery-cookie.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/common/common.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.validation/jquery.validation.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.gmap/jquery.gmap.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/isotope/jquery.isotope.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/vide/vide.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

            <!-- Theme Custom -->
            <script src="<?php echo base_url () ?>assets/admin/js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="<?php echo base_url () ?>assets/admin/js/theme.init.js"></script>

            <!-- Examples -->
            <script src="<?php echo base_url () ?>assets/admin/js/examples/examples.demos.js"></script>

            <!-- datatable-->
            <!--<script src="Datatable/jquery-3.3.1.js"></script>-->
            <!-- <script src="Datatable/jquery.dataTables.min.js"></script>-->
            <script src="<?php echo base_url () ?>assets/admin/Datatable/datatables.min.js"></script>
            <script src="<?php echo base_url () ?>assets/admin/Datatable/dataTables.bootstrap.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#tableview').DataTable();
                });
            </script>
        </div>
    </div>
</div>