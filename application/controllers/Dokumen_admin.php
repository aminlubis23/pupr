<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Dokumen_admin extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('dokumen_admin_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Upload Dokumen',
            'isi'  	=> 'dokumen_admin/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Upload Dokumen',
            'isi'   => 'dokumen_admin/form'
            );

        if ($id!='') {
            $data['value'] = $this->dokumen_admin_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->dokumen_admin_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = strtoupper($row_list->name);
            $row[] = '<div class="right" style="text-align:right">'.number_format($row_list->size).' Kb</div>';
            $row[] = $row_list->type;
            $row[] = '<div class="center"><a  style="color:red" href="'.base_url().$row_list->fullpath.'" target="_blank">Download</a></div>';
            $row[] = $this->tanggal->formatDate($row_list->created_date);
            $row[] = '<div class="center">
            <a href="'.base_url().'dokumen_admin/form/'.$row_list->id.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'dokumen_admin/delete/'.$row_list->id.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->dokumen_admin_model->count_all(),
                        "recordsFiltered" => $this->dokumen_admin_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_FILES);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('name', 'Nama Dokumen', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:red; font-style:italic">', '</div>');
            $data = array('title' => 'Tambah Upload Dokumen',
                'isi' => 'dokumen_admin/form');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'name' => $val->set_value('name'),
            );

            /*upload*/
            if($_FILES['image']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $old = $this->dokumen_admin_model->get_by_id($id);
                    if (file_exists('assets/upload/image/'.$old->image.'')) {
                        unlink('assets/upload/image/'.$old->image.'');
                    }
                }

                $upload_response = $this->upload_file->doUploadCallbackFullResponse('image', 'assets/upload/image/');
                $dataexc['size'] = $upload_response['size'];
                $dataexc['type'] = $upload_response['type'];
                $dataexc['path'] = $upload_response['path'];
                $dataexc['fullpath'] = $upload_response['fullpath'];
            }


            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->dokumen_admin_model->save($dataexc);
                /*save logs*/
                $this->logs->save('dokumen_upload', $newId, 'insert new record on Upload Dokumen module', json_encode($dataexc),'id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->dokumen_admin_model->update(array('id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('dokumen_upload', $newId, 'update record on Upload Dokumen module', json_encode($dataexc),'id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                redirect(base_url().'dokumen_admin/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
                redirect(base_url().'dokumen_admin');
            }
        }
    }

     public function delete($id){
        $this->dokumen_admin_model->delete_by_id($id);
        redirect(base_url().'dokumen_admin');
    }

}