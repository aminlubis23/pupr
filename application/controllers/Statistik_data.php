<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Statistik_data extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        if($this->session->userdata('logged')!=TRUE){
            redirect(base_url().'Login');exit;
        }
        $this->load->model('konfigurasi_model');
        $this->load->model('statistik_data_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Statistik Data CPNS',
            'jenis_formasi' => $this->db->get('mst_formasi_jenis')->result(),
            'data' => $this->statistik_data_model->get_data(),
            'count' => $this->statistik_data_model->count_global_data(),
            'grafik' => $this->statistik_data_model->grafik(),
            'isi'  	=> 'statistik_data/index'
            );
        $this->load->view('layout/wrapper',$data);
    }



}