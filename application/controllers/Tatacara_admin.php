<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Tatacara_admin extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('tatacara_admin_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Tatacara Master Admin Side',
            'isi'  	=> 'tatacara_admin/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Tatacara Master Admin Side',
            'isi'   => 'tatacara_admin/form'
            );

        if ($id!='') {
            $data['value'] = $this->tatacara_admin_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->tatacara_admin_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = '<div class="center"><img src="'.base_url().'assets/upload/image/'.$row_list->image.'" width="150px"></div>';
            $row[] = strtoupper($row_list->judul);
            $row[] = $row_list->deskripsi;
            $row[] = '<div class="center">'.$row_list->flow_number.'</div>';
            $row[] = $this->tanggal->formatDate($row_list->created_date);
            $row[] = '<div class="center">
            <a href="'.base_url().'tatacara_admin/form/'.$row_list->id.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'tatacara_admin/delete/'.$row_list->id.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->tatacara_admin_model->count_all(),
                        "recordsFiltered" => $this->tatacara_admin_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('judul', 'Judul', 'trim|required');
        $val->set_rules('flow_number', 'Flow Number', 'trim|required');
        $val->set_rules('deskripsi', 'Deskripsi', 'trim');
        $val->set_rules('link', 'Link', 'trim');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'judul' => $val->set_value('judul'),
                'flow_number' => $val->set_value('flow_number'),
                'deskripsi' => $val->set_value('deskripsi'),
                'link' => $val->set_value('link'),
            );

            //print_r($dataexc);die;

            /*upload*/
            if($_FILES['image']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $old = $this->tatacara_admin_model->get_by_id($id);
                    if (file_exists('assets/upload/image/'.$old->image.'')) {
                        unlink('assets/upload/image/'.$old->image.'');
                    }
                }

                $dataexc['image'] = $this->upload_file->doUpload('image', 'assets/upload/image/');
            }


            /*print_r($dataexc);die;*/
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->tatacara_admin_model->save($dataexc);
                /*save logs*/
                $this->logs->save('tatacara', $newId, 'insert new record on Tata Cara module', json_encode($dataexc),'id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->tatacara_admin_model->update(array('id' => $id), $dataexc);
                //print_r($this->db->last_query());die;
                $newId = $id;
                /*save logs*/
                $this->logs->save('tatacara', $newId, 'update record on Tata Cara module', json_encode($dataexc),'id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan', 'redirect' => base_url().'tatacara_admin'));
                redirect(base_url().'tatacara_admin/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => base_url().'tatacara_admin'));
                redirect(base_url().'tatacara_admin');
            }
        }
    }

     public function delete($id){
        $this->tatacara_admin_model->delete_by_id($id);
        redirect(base_url().'tatacara_admin');
    }

}