<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Data_import extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        if($this->session->userdata('logged')!=TRUE){
            redirect(base_url().'Login');exit;
        }
        $this->load->model('konfigurasi_model');
        $this->load->model('data_import_model');
        $this->load->model('verifikasi_data_cpns_model');
        $this->load->model('mst_majors_model');
    }

    // Index
    public function index() {
        $data = array(  
            'title' => 'Hasil Import Data CPNS',
            'isi'   => 'data_import/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function success() {
        $id = $this->input->get('id');
        $data = array(  
            'title' => 'Hasil Import Data CPNS',
            'isi'   => 'data_import/success_view'
            );
        $this->load->view('layout/wrapper',$data);
    }

    public function lulus() {
        $id = '6471040907930002';
        $data = array(  
            'title' => 'Hasil Akhir Seleksi CPNS',
            'isi'   => 'data_import/lulus_view'
            );

        $data['value'] = $this->verifikasi_data_cpns_model->get_by_nik($id);
        $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);

        $this->load->view('layout/wrapper',$data);
    }

    public function cetak_kartu_peserta($tipe,$nik) {
        $id = $this->input->get('id');
        $data = array(  
            'title' => 'Hasil Import Data CPNS',
            'isi'   => 'data_import/kartu_peserta',
            'tipe'   => $tipe,
            'value'   => $this->verifikasi_data_cpns_model->get_by_nik($nik),
            );
        $this->load->view('data_import/kartu_peserta',$data);
    }

    // Index
    public function form($id='') {

        $data = array('profile' => $this->konfigurasi_model->get_config_web());

        if ($id!='') {
            /*check apakah sudah diverifikasi atau belum*/
            $is_verified = $this->verifikasi_data_cpns_model->get_by_nik($id);
            $data['value'] = $this->data_import_model->get_by_nik($id);
            $data['akreditasi_jurusan'] = $this->mst_majors_model->get_majors_by_id_and_univ($data['value']->pend_jurusan,$data['value']->pend_universitas);
            //echo '<pre>';print_r($data);die;
            $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);

            //echo '<pre>';print_r($is_verified);die;
            if( empty($is_verified) ){
                $data['value'] = $this->data_import_model->get_by_nik($id);
                $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);
                $default_view = 'data_import/form';
            }else{
                if($is_verified->sv_id==1){
                    $data['value'] = $this->data_import_model->get_by_nik($id);
                    $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);
                    $default_view = 'data_import/form';
                }else {
                    if ($is_verified->verifikasi_hasil_akhir==NULL) {
                        $data['value'] = $this->verifikasi_data_cpns_model->get_by_nik($id);
                        $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);
                        $default_view = 'data_import/view_data_pelamar';
                    }else{
                        $data['value'] = $this->verifikasi_data_cpns_model->get_by_nik($id);
                        $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);
                        $data['nilai'] = $this->verifikasi_data_cpns_model->get_nilai($id);
                        //$data['info'] = $this->verifikasi_data_cpns_model->view_hasil_akhir($data['value']);
                        $data['info'] = 'data_import/lulus_view.php';
                        $default_view = 'data_import/view_informasi_akhir';
                    }
                }
                
            }

            $data['title'] = 'Data Pelamar';
            $data['isi'] = $default_view;
           
            //echo '<pre>';print_r($data);
        }else{
            $data['title'] = 'Data Pelamar';
            $data['isi'] = 'data_import/form';
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->data_import_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = $row_list->id;
            $row[] = $row_list->id;
            $row[] = '';
            $row[] = '<a href="'.base_url().'data_import/form/'.$row_list->nik.'">'.$row_list->nik.'</a>';
            $row[] = strtoupper($row_list->nama);
            $row[] = $row_list->email;
            $row[] = $row_list->no_telp;
            $row[] = $row_list->alamat_ktp;
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data_import_model->count_all(),
                        "recordsFiltered" => $this->data_import_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
         
        //echo '<pre>';print_r($_POST);die;
        $this->load->library('form_validation');
        $val = $this->form_validation;
        /*biodata diri*/
        $val->set_rules('dp_nama_lengkap', 'Nama Lengkap', 'trim|required');
        $val->set_rules('dp_nik', 'NIK', 'trim|required');
        $val->set_rules('dp_no_kk', 'No KK', 'trim|required');
        $val->set_rules('dp_tempat_lahir', 'Tempat Lahir', 'trim|required');
        $val->set_rules('dp_tanggal_lahir', 'Tanggal Lahir', 'trim|required');
        $val->set_rules('dp_jk', 'Jenis Kelamin', 'trim|required');
        $val->set_rules('dp_agama', 'Agama', 'trim|required');
        $val->set_rules('dp_status_nikah', 'Status Nikah', 'trim|required');

        /*informasi kontak*/
        $val->set_rules('dp_alamat_lengkap', 'Alamat Lengkap', 'trim|required');
        $val->set_rules('dp_alamat_org_tua', 'Alamat Orang Tua', 'trim');
        $val->set_rules('dp_domisili_negara', 'Domisili Negara', 'trim|required');
        $val->set_rules('dp_no_telp', 'No Telp', 'trim|required');
        $val->set_rules('dp_no_hp', 'No HP', 'trim|required');
        $val->set_rules('dp_email', 'Email', 'trim|valid_email|required');

        /*formasi*/
        /*$val->set_rules('formasi_kode', 'Kode Formasi', 'trim|required');*/
        $val->set_rules('formasi_jurusan_pendidikan', 'Jurusan Pendidikan', 'trim|required');
        $val->set_rules('formasi_jenis', 'Jenis Formasi', 'trim|required');
        $val->set_rules('formasi_jabatan', 'Jabatan', 'trim|required');
        $val->set_rules('formasi_penempatan', 'Penempatan', 'trim|required');
        $val->set_rules('verifikasi_is_ptt', 'Apakah PTT?', 'trim|required');
        $val->set_rules('formasi_lokasi_ujian_skd', 'Lokasi Ujian SKD', 'trim|required');
        $val->set_rules('formasi_lokasi_ujian_skb', 'Lokasi Ujian SKD', 'trim|required');

        /*pendidikan*/
        $val->set_rules('pend_universitas', 'Universitas', 'trim|required');
        $val->set_rules('pend_jurusan', 'Jurusan', 'trim|required');
        $val->set_rules('pend_tgl_ijasah', 'Tanggal Ijasah', 'trim|required');
        $val->set_rules('pend_ipk', 'IPK', 'trim|required');
       // $val->set_rules('pend_grade', 'Grade', 'trim|required');
        //$val->set_rules('pend_akreditasi_univ', 'Akreditasi Universitas', 'trim|required');
        $val->set_rules('pend_no_ijasah', 'No Ijasah', 'trim|required');
        $val->set_rules('pend_gelar_sarjana', 'Gelar Sarjana', 'trim|required');

        /*bahasa*/
        $val->set_rules('bhs_jenis_toefl', 'Jenis TOEFL', 'trim|required');
        $val->set_rules('bhs_lembaga_toefl', 'Lembaga Penerbit', 'trim|required');
        $val->set_rules('bhs_nilai_toefl', 'Nilai TOEFL', 'trim|required');
        $val->set_rules('bhs_tanggal_sertifikat', 'Tanggal Sertifikat', 'trim|required');
        $val->set_rules('bhs_no_sertifikat', 'Nomor Sertifikat', 'trim|required');
        $val->set_rules('bhs_tgl_tes_toefl', 'Tanggal Tes', 'trim|required');
        $val->set_rules('bhs_lokasi_tes', 'Lokasi Tes TOEFL', 'trim|required');


        $val->set_message('required', "Silahkan isi field \"%s\"");
        $val->set_message('valid_email', "Format \"%s\" tidak sesuai");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:yellow"><i>', '</i></div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));

        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('dp_id'))?$this->input->post('dp_id'):0;

            /*get no peserta*/
            /*$no_peserta = $this->data_import_model->get_no_peserta();*/

            $dataexc = array(
                /*biodata diri*/
                /*'dp_no_peserta' => $no_peserta,*/
                'dp_nama_lengkap' => $val->set_value('dp_nama_lengkap'),
                'dp_nik' => $val->set_value('dp_nik'),                
                'dp_no_kk' => $val->set_value('dp_no_kk'),
                'dp_tempat_lahir' => $val->set_value('dp_tempat_lahir'),
                'dp_tanggal_lahir' => $val->set_value('dp_tanggal_lahir'),
                'dp_jk' => $val->set_value('dp_jk'),
                'dp_agama' => $val->set_value('dp_agama'),                
                'dp_status_nikah' => $val->set_value('dp_status_nikah'),

                /*informasi kontak*/
                'dp_alamat_lengkap' => $val->set_value('dp_alamat_lengkap'),
                'dp_alamat_org_tua' => $val->set_value('dp_alamat_org_tua'),
                'dp_domisili_negara' => $val->set_value('dp_domisili_negara'),
                'dp_no_telp' => $val->set_value('dp_no_telp'),
                'dp_no_hp' => $val->set_value('dp_no_hp'),
                'dp_email' => $val->set_value('dp_email'),

                /*formasi*/
                /*'formasi_kode' => $val->set_value('formasi_kode'),*/
                'formasi_kategori_jabatan' => $this->data_import_model->get_kfj_id($val->set_value('formasi_jabatan')),
                'formasi_jurusan_pendidikan' => $val->set_value('formasi_jurusan_pendidikan'),
                'formasi_jenis' => $val->set_value('formasi_jenis'),
                'formasi_jabatan' => $val->set_value('formasi_jabatan'),
                'formasi_penempatan' => $val->set_value('formasi_penempatan'),
                'verifikasi_is_ptt' => $val->set_value('verifikasi_is_ptt'),
                

                /*pendidikan*/
                'pend_universitas' => $val->set_value('pend_universitas'),
                'pend_jurusan' => $val->set_value('pend_jurusan'),
                'pend_tgl_ijasah' => $val->set_value('pend_tgl_ijasah'),
                'pend_ipk' => $val->set_value('pend_ipk'),
                'pend_grade' => $val->set_value('pend_grade'),
                'pend_akreditasi_univ' => $val->set_value('pend_akreditasi_univ'),
                'pend_no_ijasah' => $val->set_value('pend_no_ijasah'),
                'pend_gelar_sarjana' => $val->set_value('pend_gelar_sarjana'),

                /*bahasa*/
                'bhs_jenis_toefl' => $val->set_value('bhs_jenis_toefl'),
                'bhs_lembaga_toefl' => $val->set_value('bhs_lembaga_toefl'),
                'bhs_nilai_toefl' => $val->set_value('bhs_nilai_toefl'),
                'bhs_tanggal_sertifikat' => $val->set_value('bhs_tanggal_sertifikat'),
                'bhs_no_sertifikat' => $val->set_value('bhs_no_sertifikat'),
                'bhs_tgl_tes_toefl' => $val->set_value('bhs_tgl_tes_toefl'),
                'bhs_lokasi_tes' => $val->set_value('bhs_lokasi_tes'),

                /*lokasi ujian*/
                'loku_skb_id' => $val->set_value('formasi_lokasi_ujian_skb'),
                'loku_skd_id' => $val->set_value('formasi_lokasi_ujian_skd'),
            );


            
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->data_import_model->save_data_cpns($dataexc);
                /*save logs*/
                $this->logs->save('tr_data_pelamar', $newId, 'insert new record on Data Pelamar module', json_encode($dataexc),'dp_id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->data_import_model->update_data_cpns(array('dp_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('tr_data_pelamar', $newId, 'update record on Data Pelamar module', json_encode($dataexc),'dp_id');
            }

            /*eksekusi file upload*/

            /*excecute upload*/
            if($newId){
                $config = array(
                    'nik' => $val->set_value('dp_nik'),
                    'name' => 'file',
                    'path' => 'uploaded_files/data_pelamar/',
                );
                $this->upload_file->doUploadMultiple($config);
            }


            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                //redirect(base_url().'data_import/form');
            }
            else
            {
                $this->db->trans_commit();
                if($this->session->userdata('user')->level==3){
                    $redirect = base_url().'data_import/success?id='.$newId.'';
                }else{
                    $redirect = base_url().'verifikasi_data_cpns';
                }
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => $redirect));
                //redirect(base_url().'data_import');
            }
        }
    }

     public function delete($id){
        $this->data_import_model->delete_by_id($id);
        redirect(base_url().'data_import');
    }

    public function getDetail($id){
        
        /*get detail data billing*/
        $data = $this->data_import_model->get_by_id($id);
        
        $html = '';
        $html .= '<table class="table">';
        $html .= '<tr>';
        $html .= '<th>Nomor KK</th>';
        $html .= '<th>Tempat/Tanggal Lahir</th>';
        $html .= '<th>Agama</th>';
        $html .= '<th>Tinggi Badan (Cm)</th>';
        $html .= '<th>Status Marital</th>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>'.$data->no_kk.'</td>';
        $html .= '<td>'.$data->tempat_lahir.'/'.$this->tanggal->formatDate($data->tanggal_lahir).'</td>';
        $html .= '<td>'.$data->nama_agama.'</td>';
        $html .= '<td align="center">'.$data->tinggi_badan.' </td>';
        $html .= '<td>'.$data->nama_status_kawin.'</td>';
        $html .= '</tr>';

        $html .= '</table>';

        echo json_encode(array('html' => $html));
    }

    public function process_import(){
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $path = 'uploaded_files/data_import/';
        $filename = $this->upload_file->doUpload('file',$path);


        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load($path.$filename); // Load file yang telah diupload ke folder excel
        
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

        //$variable_inject_akses_level = 'member';

        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = [];

        $numrow = 1;
        foreach($sheet as $row){
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
                // Kita push (add) array data ke variabel data
                array_push($data, [
                    'id_exportdatascnbkn'=>$row['A'], // Ambil data no
                    'id'=>$row['B'], // Insert data id dari kolom A di excel
                    'nik'=>$row['C'], // Ambil data nik
                    'no_kk'=>$row['D'], // Ambil data jenis kelamin
                    'nik_kk'=>$row['E'], // Ambil data alamat
                    'password'=>$row['F'], // Ambil data alamat
                    'nama'=>$row['G'], // Ambil data alamat
                    'nama_ijazah'=>$row['H'], // Ambil data alamat
                    'tempat_lahir'=>$row['I'], // Ambil data alamat
                    'tempat_lahir_ijazah'=>$row['J'], // Ambil data alamat
                    'tanggal_lahir'=>$row['K'], // Ambil data alamat
                    'tanggal_lahir_ijazah'=>$row['L'], // Ambil data alamat
                    'jenis_kelamin'=>$row['M'], // Ambil data alamat
                    'email'=>$row['N'], // Ambil data alamat
                    'pertanyaan_pengaman_1'=>$row['O'], // Ambil data alamat
                    'jawaban_1'=>$row['P'], // Ambil data alamat
                    'pertanyaan_pengaman_2'=>$row['Q'], // Ambil data alamat
                    'jawaban_2'=>$row['R'], // Ambil data alamat
                    'agama'=>$row['S'],// Ambil data alamat
                    'alamat_ktp'=>$row['T'], // Ambil data alamat
                    'alamat_domisili'=>$row['U'], // Ambil data alamat
                    'tinggi_badan'=>$row['V'], // Ambil data alamat
                    'status_kawin'=>$row['W'], // Ambil data alamat
                    'no_telp'=>$row['X'], // Ambil data alamat
                    'no_hp'=>$row['Y'], // Ambil data alamat
                    'nama_ibu'=>$row['Z'], // Ambil data alamat
                    'nama_ayah'=>$row['AA'], // Ambil data alamat
                    'kodepos'=>$row['AB'], // Ambil data alamat
                    'lokasi_kabkota'=>$row['AC'], // Ambil data alamat
                    'tgl_daftar'=>$row['AD'], // Ambil data alamat
                    //'akses_level'=>$variable_inject_akses_level, // ambil data dari injector temporary
                    'remarks'=>$row['AE'], // Ambil data alamat
                ]);
            }

            $numrow++; // Tambah 1 setiap kali looping
        }

        /*echo '<pre>'; print_r($data);die;*/

        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model, disini adalah event handler untuk import kedalam
        // database mapp data dari bkn
        $this->data_import_model->insert_multiple($data);

        redirect("data_import");
    }



}