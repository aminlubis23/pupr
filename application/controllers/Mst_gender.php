<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Mst_gender extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('mst_gender_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Master Data Jenis Kelamin',
            'isi'  	=> 'mst_gender/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Master Data Jenis Kelamin',
            'isi'   => 'mst_gender/form'
            );

        if ($id!='') {
            $data['value'] = $this->mst_gender_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->mst_gender_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = '<div class="center">'.$row_list->gender_id.'</div>';
            $row[] = $row_list->gender_name;
            $row[] = '<div class="center">
            <a href="'.base_url().'mst_gender/form/'.$row_list->gender_id.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'mst_gender/delete/'.$row_list->gender_id.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->mst_gender_model->count_all(),
                        "recordsFiltered" => $this->mst_gender_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_FILES);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('gender_name', 'Nama Jenis Kelamin', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:red">', '</div>');
            $data = array('title' => 'Add Data Master Data Jenis Kelamin',
                'isi' => 'mst_gender/form');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'gender_name' => $val->set_value('gender_name'),
            );
            //print_r($dataexc);die;



            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->mst_gender_model->save($dataexc);
                /*save logs*/
                $this->logs->save('mst_gender', $newId, 'insert new record on Master Data Jenis Kelamin module', json_encode($dataexc),'gender_id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->mst_gender_model->update(array('gender_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('mst_gender', $newId, 'update record on Master Data Jenis Kelamin module', json_encode($dataexc),'gender_id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                redirect(base_url().'mst_gender/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
                redirect(base_url().'mst_gender');
            }
        }
    }

     public function delete($id){
        $this->mst_gender_model->delete_by_id($id);
        redirect(base_url().'mst_gender');
    }

}