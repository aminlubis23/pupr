<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');

         /*load libraries*/
        $this->load->library('bcrypt');
        $this->load->library('logs');
        $this->load->library('Form_validation');
        /*load model*/
        $this->load->model('login_model');

    }

    // Index login
    public function index()
    {
        $site = $this->konfigurasi_model->listing();

        $data = array('title' => 'Admin Login Page ' . $site['namaweb'] . ' | ' . $site['tagline'],
            'keywords' => 'Admin Login Page ' . $site['namaweb'] . ', ' . $site['keywords'],
            'isi' => 'login_view_admin');
        $this->load->view('layout/wrapper_depan', $data);
    }

    public function change_password()
    {
        $site = $this->konfigurasi_model->listing();

        $data = array('title' => 'Admin Login Page ' . $site['namaweb'] . ' | ' . $site['tagline'],
            'keywords' => 'Admin Login Page ' . $site['namaweb'] . ', ' . $site['keywords'],
            'isi' => 'change_password_view');
        $this->load->view('layout/wrapper_depan', $data);
    }

    public function process(){

        /*post username*/
        $username = $this->input->post('username');
        /*hash password bcrypt*/
        $password = $this->input->post('password');

        // form validation
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        // set message error
        $this->form_validation->set_message('required', "Silahkan isi field \"%s\"");
        $this->form_validation->set_message('min_length', "\"%s\" minimal 6 karakter");

        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_error_delimiters('<div style="color:white"><i>', '</i></div>');
            //die(validation_errors());
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            //set session expire time, after that user should login again
            $this->session->sess_expiration = '1800';
            $this->session->sess_expire_on_close = 'true';

            /*check username and password exist*/
            $table = isset($_GET['model'])?'users_cpns':'users';
            $result = $this->login_model->check_account($username,$password, $table);
            if($result){
                /*clear token existing or before*/
                //$this->login_model->clear_token($result->user_id);

                /*save data bellow in session*/
                $sess_data = array(
                    'logged' => TRUE,
                    'user' => $result,
                    'model' => $table,
                    //'token' => $this->login_model->generate_token($result->user_id),
                    //'menus' => $this->login_model->get_sess_menus($result->role_id),
                );
                $this->session->set_userdata($sess_data);
                /*update last logon user*/
                
                $this->db->query("UPDATE ".$table." SET last_logon=now() WHERE username='".$result->username."' AND password='".$result->password."'");
                /*save log activities*/
                //$this->logs->save($table, $result->user_id, 'user loged in', json_encode($sess_data),'user_id');
                redirect(base_url('dashboard'));
                echo json_encode(array('status' => 200, 'message' => 'Login berhasil'));
            }else{
                echo json_encode(array('status' => 301, 'message' => 'Username dan Password tidak sesuai'));
                $table = isset($_GET['model'])?redirect(REDIRECT_FRONT.'login'):redirect(base_url('login'));
            }
        
        }

    }

    public function change_password_process(){

        /*post username*/
        $password = $this->input->post('password');
        /*hash password bcrypt*/
        $confirm_password = $this->input->post('confirm_password');

        // form validation
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'trim|required|matches[password]');

        // set message error
        $this->form_validation->set_message('required', "Silahkan isi field \"%s\"");

        $this->form_validation->set_message('matches', "\"%s\" tidak sesuai dengan password");

        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_error_delimiters('<div style="color:white"><i>', '</i></div>');
            //die(validation_errors());
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            
            /*update password*/

            $dataexc = array(
                'password' => $this->bcrypt->hash_password($this->form_validation->set_value('password')),
                'has_change_password' => 'Y',
                );

            $result = $this->db->update('users_cpns', $dataexc, array('user_id' => $this->session->userdata('user')->user_id));

            if($result){

                echo json_encode(array('status' => 200, 'message' => 'Proses Ubah Password Berhasil Dilakukan', 'redirect' => base_url().'login/logout'));
            
            }else{
                
                echo json_encode(array('status' => 301, 'message' => 'Proses Gagal Dilakukan'));
            
            }
        
        }

    }

    public function logout()
    {   
        $sess_data = array ('user' => NULL,
                            'menus' => NULL,
                            'logged'=>false
                            );
        //$this->login_model->clear_token($this->session->userdata('user')->user_id);
        $this->session->unset_userdata($sess_data);
        $this->session->sess_destroy();
        $base_redirect = ($this->session->userdata('model')=='users_cpns')?REDIRECT_FRONT:base_url();
        redirect($base_redirect.'login');
    }

}
//}