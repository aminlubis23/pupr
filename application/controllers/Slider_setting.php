<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Slider_setting extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('slider_setting_model');
    }

    // Index
    public function index() {
        $data = array(  
            'title' => 'Slider Setting',
            'isi'   => 'slider_setting/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Slider Setting',
            'isi'   => 'slider_setting/form'
            );

        if ($id!='') {
            $data['value'] = $this->slider_setting_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->slider_setting_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $src = isset($row_list->gambar)?'<img src="'.base_url().'assets/upload/image/'.$row_list->gambar.'" style="width:100px">':'-No Image Available-';

            $row[] = '<div class="center">'.$src.'</div>';
            $row[] = $row_list->judul;
            $row[] = $row_list->link_berita;
            $row[] = '<div class="center">
            <a href="'.base_url().'slider_setting/form/'.$row_list->id_slider.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'slider_setting/delete/'.$row_list->id_slider.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->slider_setting_model->count_all(),
                        "recordsFiltered" => $this->slider_setting_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_FILES);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('judul', 'Judul', 'trim|required');
        $val->set_rules('isi_berita', 'Isi Berita', 'trim|required');
        $val->set_rules('link_berita', 'Sumber', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:red">', '</div>');
            $data = array('title' => 'Add Data Slider Setting',
                'isi' => 'slider_setting/form');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'judul' => $val->set_value('judul'),
                'isi_berita' => $val->set_value('isi_berita'),
                'link_berita' => $val->set_value('link_berita'),
            );

            /*upload*/
            if($_FILES['gambar']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $old = $this->slider_setting_model->get_by_id($id);
                    if (file_exists('assets/upload/image/'.$old->gambar.'')) {
                        unlink('assets/upload/image/'.$old->gambar.'');
                    }
                }

                $dataexc['gambar'] = $this->upload_file->doUpload('gambar', 'assets/upload/image/');
            }


            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->slider_setting_model->save($dataexc);
                /*save logs*/
                $this->logs->save('slider', $newId, 'insert new record on Slider Setting module', json_encode($dataexc),'id_slider');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->slider_setting_model->update(array('id_slider' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('slider', $newId, 'update record on Slider Setting module', json_encode($dataexc),'id_slider');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                redirect(base_url().'slider_setting/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
                redirect(base_url().'slider_setting');
            }
        }
    }

     public function delete($id){
        $this->slider_setting_model->delete_by_id($id);
        redirect(base_url().'slider_setting');
    }

}