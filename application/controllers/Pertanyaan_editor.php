<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 11/08/2018
 * Time: 20:43
 */

class Pertanyaan_editor extends CI_Controller
{
// Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('bantuan_model');
        //$this->load->model('kategori_berita_model');
    }

    public function index(){
        $site	= $this->konfigurasi_model->listing();
        $bantuan	= $this->bantuan_model->get_data(); //print_r($bantuan);die;

        $data	= array( 'title'	=> 'Pertanyaan Editor Super Admin '.$site['namaweb'].' | '.$site['tagline'],
            'keywords' => 'Pertanyaan Editor Super Admin '.$site['namaweb'].', '.$site['keywords'],
            'value'	=> $bantuan,
            'isi'		=> 'bantuan_pertanyaan/list');

        $this->load->view('layout/wrapper',$data);
    }

    public function process()
    {

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('mail_layanan_online', 'Email', 'trim');
        $val->set_rules('fb_layanan_online', 'Facebook', 'trim');
        $val->set_rules('twitter_layanan_online', 'Twitter', 'trim');
        $val->set_rules('nama_telp1_administratif', 'nama_telp1_administratif', 'trim');
        $val->set_rules('telp1_administratif', 'telp1_administratif', 'trim');
        $val->set_rules('nama_telp2_administratif', 'nama_telp2_administratif', 'trim');
        $val->set_rules('telp2_administratif', 'telp2_administratif', 'trim');
        $val->set_rules('nama_telp3_administratif', 'nama_telp3_administratif', 'trim');
        $val->set_rules('telp3_administratif', 'telp3_administratif', 'trim');
        $val->set_rules('nama_custcare_administratif', 'nama_custcare_administratif', 'trim');
        $val->set_rules('custcare_administratif', 'custcare_administratif', 'trim');
        $val->set_rules('text_administratif', 'text_administratif', 'trim');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            $data = array('title' => 'Tambah Pertanyaan Editor Super Admin',
                'isi' => 'pertanyaan_editor');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'mail_layanan_online' => $val->set_value('mail_layanan_online'),
                'fb_layanan_online' => $val->set_value('fb_layanan_online'),
                'twitter_layanan_online' => $val->set_value('twitter_layanan_online'),
                'nama_telp1_administratif' => $val->set_value('nama_telp1_administratif'),
                'nama_telp2_administratif' => $val->set_value('nama_telp2_administratif'),
                'nama_telp3_administratif' => $val->set_value('nama_telp3_administratif'),
                'nama_custcare_administratif' => $val->set_value('nama_custcare_administratif'),
                'telp1_administratif' => $val->set_value('telp1_administratif'),
                'telp2_administratif' => $val->set_value('telp2_administratif'),
                'telp3_administratif' => $val->set_value('telp3_administratif'),
                'custcare_administratif' => $val->set_value('custcare_administratif'),
                'text_administratif' => $val->set_value('text_administratif'),
            );

            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->bantuan_model->save($dataexc);
                /*save logs*/
                $this->logs->save('global_parameter', $newId, 'insert new record on Layanan Online module', json_encode($dataexc),'id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->bantuan_model->update(array('id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('global_parameter', $newId, 'update record on Layanan Online module', json_encode($dataexc),'id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                redirect(base_url().'pertanyaan_editor');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
                redirect(base_url().'pertanyaan_editor');
            }
        }
    }

}
