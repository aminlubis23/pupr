<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Verifikasi_data_cpns extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        if($this->session->userdata('logged')!=TRUE){
            redirect(base_url().'Login');exit;
        }
        $this->load->model('konfigurasi_model');
        $this->load->model('verifikasi_data_cpns_model');
        $this->load->model('data_import_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Verifikasi Data CPNS',
            'isi'  	=> 'verifikasi_data_cpns/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Kelengkapan Data CPNS',
            'isi'   => 'verifikasi_data_cpns/form'
            );

        if ($id!='') {
            $data['value'] = $this->verifikasi_data_cpns_model->get_by_nik($id);
            $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);
            //echo '<pre>';print_r($data);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function verifikasi($id='') {

        $data = array();

        if ($id!='') {
            $data['value'] = $this->verifikasi_data_cpns_model->get_by_nik($id);
            $data['file'] = $this->verifikasi_data_cpns_model->get_file_uploaded($id);
            if($data['value']->sv_id==1){
                $data['title'] = 'Verifikasi Kelengkapan Data CPNS';
                $data['isi'] = 'verifikasi_data_cpns/form_verifikasi';
            }else{
                $data['title'] = 'Verifikasi Nilai SKD/SKB';
                $data['isi'] = 'verifikasi_data_cpns/form_verifikasi_nilai';
            }
        }else{
            $data['title'] = 'Verifikasi Kelengkapan Data CPNS';
            $data['isi'] = 'verifikasi_data_cpns/form_verifikasi';
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->verifikasi_data_cpns_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            if($row_list->verifikasi_status==1){
                $form_checkbox = '<input type="checkbox" name="selected_id[]" value="'.$row_list->dp_id.'">';
            }else{
                if(in_array($row_list->verifikasi_status, array(3,4) )){
                    $form_checkbox = '<h3 style="color:green"><i class="fa fa-check green"></i></h3>';
                }else{
                    $form_checkbox = '<h3 style="color:red"><i class="fa fa-times-circle red"></i></h3>';
                }
            }
            $row[] = '<div class="center">'.$form_checkbox.'</div>';
            /*get pas foto pelamar*/
            $pas_foto = $this->verifikasi_data_cpns_model->get_file_pelamar_by_params( array('nik' => $row_list->dp_nik, 'file_category' => 2) );
            $src = isset($pas_foto->file_name)?'<img src="'.base_url().'uploaded_files/data_pelamar/'.$pas_foto->file_name.'" style="width:100px">':'-No Image Available-';

            $text_ver_usia = '';
            if($row_list->verifikasi_is_ptt=='Y'){
                $ver_usia_ptt = $this->konfigurasi_model->verifikasi_umur($row_list->dp_nik,$this->tanggal->hitungUmurShowYearOnly($row_list->dp_tanggal_lahir));
                if($ver_usia_ptt==false){
                    $text_ver_usia .= '<span style="color:red;font-style:italic">Batas Usia PTT melebihi batas maximum</span>';
                }

            }

            $row[] = '<div class="center">'.$src.'<br><a style="color:blue" href="'.base_url().'verifikasi_data_cpns/form/'.$row_list->dp_nik.'">'.$row_list->dp_nik.'</a></div>';
            $ver_ptt = ($row_list->verifikasi_is_ptt=='Y')?'Ya':'Tidak';
            $row[] = strtoupper($row_list->dp_nama_lengkap).'<br>JK : '.$row_list->jk.'<br> TTL : '.$row_list->dp_tempat_lahir.', '.$this->tanggal->formatDate($row_list->dp_tanggal_lahir).'<br>Umur : '.$this->tanggal->hitungUmur($row_list->dp_tanggal_lahir).'<br>PTT : '.$ver_ptt.' <br> '.$text_ver_usia;

            $row[] = $row_list->dp_alamat_lengkap.'<br>Email : '.$row_list->dp_email.'<br>Telp : '.$row_list->dp_no_telp;

            /*pendidikan*/
            /*cek minimum IPK*/
            $ver_ipk = $this->konfigurasi_model->verifikasi_ipk($row_list->pend_ipk);
            $ipk = ( $ver_ipk )?'<span style="font-size:14px; color:green; font-weight:bold">'.$row_list->pend_ipk.' (Memenuhi Syarat)</span>':'<span style="font-size:14px; color:red; font-weight:bold">'.$row_list->pend_ipk.' (Belum Memenuhi Syarat)</span>';
            
            $row[] = 'IPK : '.$ipk.'<br><b>'. $row_list->univ_name.'</b> <br><i>(Perguruan Tinggi <b>"'.$row_list->univ_status.'"</b> Akreditasi <b>"'.$row_list->univ_akreditasi.'"</b>)</i><br> Jurusan <b>"'.$row_list->majors_name.'"</b><br> Lulus Tanggal <b>'.$this->tanggal->formatDate($row_list->pend_tgl_ijasah).'</b>';
            /*end pendidikan*/

            /*kemampuan bahasa inggris*/
            /*cek minimum TOEFL*/
            $ver_toefl = $this->konfigurasi_model->verifikasi_toefl($row_list->bhs_nilai_toefl);
            $toefl = ( $ver_toefl )?'<span style="font-size:14px; color:green; font-weight:bold">'.$row_list->bhs_nilai_toefl.' (Memenuhi Syarat)</span>':'<span style="font-size:14px; color:red; font-weight:bold">'.$row_list->bhs_nilai_toefl.' (Belum Memenuhi Syarat)</span>';

            $row[] = 'TOEFL : '.$toefl.' <br>Lembaga TOEFL <b>'.$row_list->bhs_lembaga_toefl.'</b><br> Tanggal Sertifikat : <b>'.$this->tanggal->formatDate($row_list->bhs_tanggal_sertifikat).'</b>';

            /*kelengkapan dokumen*/
            $item_dok = $this->verifikasi_data_cpns_model->get_item_dok('kf'); 
            $file_upload = $this->verifikasi_data_cpns_model->get_file_uploaded($row_list->dp_nik);
            //print_r($item_dok);die;
            
            $dok = '';
            foreach ($item_dok as $key => $value) {

                if( !isset($file_upload[$value['gp_id']]) ){
                    $icon = '<i style="color:red" class="fa fa-times-circle"></i>';  
                    $dok_name = $value['gp_name'];              
                }else{
                    $icon = '<i style="color:green" class="fa fa-check"></i>';
                    $dok_name = '<a href="'.base_url().'uploaded_files/data_pelamar/'.$file_upload[$value['gp_id']].'" target="blank" style="color:red">'.$value['gp_name'].'</a>'; 
                }

                $dok .= $icon.'&nbsp; '.$dok_name.' <br>';
            }

            $row[] = $row_list->formasi_jenis_name.'<br>'.$row_list->kp_name.'<br>'.$row_list->fj_name;

            $row[] = $dok;

            $status = ($row_list->sv_id==1)?'<div class="center"><span class="label label-danger">'.$row_list->sv_name.'</span></div>':'<b>'.$row_list->sv_name.'</b><br><span style="font-size:11px">Oleh : '.$row_list->verifikasi_petugas.'<br>Tanggal : '.$this->tanggal->formatDateForm($row_list->verifikasi_tanggal).'<br></span>';

            if($row_list->sv_id==1){
                $row[] = '<div class="center"><a href="'.base_url().'verifikasi_data_cpns/verifikasi/'.$row_list->dp_nik.'" class="btn btn-xs btn-primary">Verifikasi<br>Kelengkapan Data</a></div>';
            }else{
                if($row_list->verifikasi_hasil_akhir==NULL){
                    if(in_array($row_list->sv_id, array(3,4))){
                        $row[] = '<div class=" alert alert-success center">'.$status.' <br> <a href="'.base_url().'verifikasi_data_cpns/verifikasi/'.$row_list->dp_nik.'" class="btn btn-xs btn-success">Verifikasi<br>Nilai Ujian SKD/SKB</a> <br><br> <a onclick="reviewUlang('.$row_list->dp_id.')" href="#" class="btn btn-xs btn-warning">Review Ulang</a></div';
                    }else{
                        $row[] = '<div class="alert alert-danger center">'.$status.' <br> <a onclick="reviewUlang('.$row_list->dp_id.')" href="#" class="btn btn-xs btn-danger">Review Ulang</a> </div>';
                    }
                    
                }else{
                    /*view hasil akhir*/
                    $last_view = $this->verifikasi_data_cpns_model->view_hasil_akhir($row_list);
                    $row[] = '<div class="center">'.$last_view.'<a href="#" class="btn btn-xs btn-danger" onclick="reviewUlang('.$row_list->dp_id.')">Review Ulang</a> <br><br> '.$status.' </div> ';
                }
                
            }

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->verifikasi_data_cpns_model->count_all(),
                        "recordsFiltered" => $this->verifikasi_data_cpns_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
         
        //echo '<pre>';print_r($_FILES);die;
        $this->load->library('form_validation');
        $val = $this->form_validation;
        /*biodata diri*/
        $val->set_rules('verifikasi_catatan_hasil_sistem', 'Catatan Hasil Verifikasi Sistem', 'trim|required');
        $val->set_rules('sv_id', 'Hasil Verifikasi', 'trim|required');
        $val->set_rules('verifikasi_catatan_kelengkapan_data', 'Catatan Hasil Verifikasi', 'trim|required');

        if($this->input->post('sv_id')==1){
            $val->set_rules('sv_id', 'Hasil Verifikasi', 'callback_check_status');
        }

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:yellow"><i>', '</i></div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));

        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('dp_id'))?$this->input->post('dp_id'):0;

            $dataexc = array(
                /*biodata diri*/
                'verifikasi_status' => $val->set_value('sv_id'),
                'verifikasi_catatan_hasil_sistem' => $val->set_value('verifikasi_catatan_hasil_sistem'),
                'verifikasi_catatan_kelengkapan_data' => $val->set_value('verifikasi_catatan_kelengkapan_data'),
                'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                'verifikasi_petugas' => $this->session->userdata('user')->nama,
            );

            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->verifikasi_data_cpns_model->save_data_cpns($dataexc);
                /*save logs*/
                $this->logs->save('tr_data_pelamar', $newId, 'insert new record on Data Pelamar module', json_encode($dataexc),'dp_id');
            }else{

                if(in_array($val->set_value('sv_id'), array(3,4))){
                    $no_peserta = $this->data_import_model->get_no_peserta($id);
                    $dataexc['dp_no_peserta'] = $no_peserta;
                }else{
                    $dataexc['dp_no_peserta'] = NULL;
                }

                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->verifikasi_data_cpns_model->update_data_cpns(array('dp_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('tr_data_pelamar', $newId, 'update record on Data Pelamar module', json_encode($dataexc),'dp_id');
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => base_url().'verifikasi_data_cpns'));
            }
        }
    }

    public function processQuickVerified()
    {
         
        //echo '<pre>';print_r($_FILES);die;
        $this->load->library('form_validation');
        $val = $this->form_validation;
        /*biodata diri*/
        $val->set_rules('status', 'Hasil Verifikasi', 'trim|required');
        $val->set_rules('catatan', 'Catatan', 'trim|required');
        //$val->set_rules('catatan_hasil_sistem', 'Catatan Hasil Pada Sistem', 'trim|required');

        if($this->input->post('status')==1){
            $val->set_rules('status', 'Hasil Verifikasi', 'callback_check_status');
        }

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:yellow"><i>', '</i></div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));

        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('arr_id'))?$this->input->post('arr_id'):0;

            foreach ($id as $key => $value) {
                
                $dataexc = array(
                    /*biodata diri*/
                    'verifikasi_status' => $val->set_value('status'),
                    'verifikasi_catatan_hasil_sistem' => $val->set_value('catatan_hasil_sistem'),
                    'verifikasi_catatan_kelengkapan_data' => $val->set_value('catatan'),
                    'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                    'verifikasi_petugas' => $this->session->userdata('user')->nama,
                );

                /*no_peserta*/

                $no_peserta = $this->data_import_model->get_no_peserta($value);
                $dataexc['dp_no_peserta'] = $no_peserta;

                /*lokasi ujian*/
                $lokasi_ujian = $this->verifikasi_data_cpns_model->get_sesi_ujian($value);
                /*SKD*/
                $dataexc['loku_urut_skd'] = $lokasi_ujian['skd']['urut_skd'];
                $dataexc['loku_sesi_skd'] = $lokasi_ujian['skd']['sesi_skd'];
                /*SKB*/
                $dataexc['loku_urut_skb'] = $lokasi_ujian['skb']['urut_skb'];
                $dataexc['loku_sesi_skb'] = $lokasi_ujian['skb']['sesi_skb'];

                /*print_r($dataexc);die;*/

                if($value==0){
                    
                    $dataexc['created_date'] = date('Y-m-d H:i:s');
                    $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                    /*save post data*/
                    $newId = $this->verifikasi_data_cpns_model->save_data_cpns($dataexc);
                    /*save logs*/
                    $this->logs->save('tr_data_pelamar', $newId, 'insert new record on Data Pelamar module', json_encode($dataexc),'dp_id');
                }else{
                    $dataexc['updated_date'] = date('Y-m-d H:i:s');
                    $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                    /*update record*/
                    $this->verifikasi_data_cpns_model->update_data_cpns(array('dp_id' => $value), $dataexc);
                    $newId = $value;
                    /*save logs*/
                    $this->logs->save('tr_data_pelamar', $newId, 'update record on Data Pelamar module', json_encode($dataexc),'dp_id');
                }


            }
            

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => base_url().'verifikasi_data_cpns'));
            }
        }
    }

    public function check_status($str)
    {
        if ($str == 1)
        {
            $this->form_validation->set_message('check_status', 'Status {field} "BELUM DIVERIFIKASI" silahkan ubah hasil verifikasi');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function process_verifikasi_nilai()
    {
         
        $this->load->library('form_validation');
        $val = $this->form_validation;
        /*biodata diri*/
        $val->set_rules('verifikasi_ujian_skd', 'Hasil Ujian SKD', 'trim|required');
        $val->set_rules('verifikasi_ujian_skb', 'Hasil Ujian SKB', 'trim|required');
        $val->set_rules('verifikasi_hasil_akhir', 'Hasil Akhir', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:yellow"><i>', '</i></div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));

        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('dp_id'))?$this->input->post('dp_id'):0;

            $dataexc = array(
                /*biodata diri*/
                'verifikasi_ujian_skd' => $val->set_value('verifikasi_ujian_skd'),
                'verifikasi_ujian_skb' => $val->set_value('verifikasi_ujian_skb'),
                'verifikasi_hasil_akhir' => $val->set_value('verifikasi_hasil_akhir'),
                'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                'verifikasi_petugas' => $this->session->userdata('user')->nama,
            );

            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->verifikasi_data_cpns_model->save_data_cpns($dataexc);
                /*save logs*/
                $this->logs->save('tr_data_pelamar', $newId, 'insert new record on Data Pelamar module', json_encode($dataexc),'dp_id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->verifikasi_data_cpns_model->update_data_cpns(array('dp_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('tr_data_pelamar', $newId, 'update record on Data Pelamar module', json_encode($dataexc),'dp_id');
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => base_url().'verifikasi_data_cpns'));
            }
        }
    }


    public function getDetail($id){
        
        /*get detail data billing*/
        $data = $this->verifikasi_data_cpns_model->get_by_id($id);
        $item_dok = $this->verifikasi_data_cpns_model->get_item_dok('kf');
        $file_upload = $this->verifikasi_data_cpns_model->get_file_uploaded($data->dp_nik);
        //print_r($file_upload);
        
        $html = '';
        $html .= '<h5><i class="fa fa-file"></i> KELENGKAPAN DOKUMEN</h5>';
        $html .= '<table class="table bordered">';
        $html .= '<tr>';
        foreach ($item_dok as $key => $value) {
            $html .= '<th class="center" width="100px" style="background-color:#efb803;color:black">'.$value['gp_name'].'</th>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        foreach ($item_dok as $k => $v) {
            if( !isset($file_upload[$v['gp_id']]) ){
                $html .= '<td align="center"><i style="color:red" class="fa fa-times-circle"></i></td>';                
            }else{
                $html .= '<td align="center"><i style="color:green" class="fa fa-check"></i><br><a href="'.base_url().'uploaded_files/data_pelamar/'.$file_upload[$v['gp_id']].'" target="blank" style="color:red">Download</a></td>';
            }
        }
        $html .= '</tr>';
        $html .= '</table>';

        echo json_encode(array('html' => $html));
    }


    public function getDetailFromSelectedData(){
        /*print_r($_POST);die;*/
        /*get data by dp_id*/
        $id = $_POST['arr_id'];
        $data = $this->verifikasi_data_cpns_model->get_by_id($id);
        
        $html = '';
        $html .= '<h5><i class="fa fa-file"></i> VERIFIKASI DATA PELAMAR</h5>';
        $html .= '<table class="table bordered">';
        $html .= '<tr>';
        $html .= '<th>No</th>';
        $html .= '<th>Nama</th>';
        $html .= '<th>IPK</th>';
        $html .= '<th>TOEFL</th>';
        $html .= '<th>Kekurangan Dokumen</th>';
        $html .= '<th>Jenis Formasi</th>';
        $html .= '</tr>';
        $no = 0;
        foreach ($data as $key => $value) { $no++; 
            /*kelengkapan dokumen*/
            $item_dok = $this->verifikasi_data_cpns_model->get_item_dok('kf'); 
            $file_upload = $this->verifikasi_data_cpns_model->get_file_uploaded($value->dp_nik);
            //print_r($item_dok);die;
            
            $dok = '';
            foreach ($item_dok as $k => $val) {

                if( !isset($file_upload[$val['gp_id']]) ){
                    $icon = '<i style="color:red" class="fa fa-times-circle"></i>';  
                    $dok_name = $val['gp_name'].'<br>'; 
                    $dok .= $icon.'&nbsp; '.$dok_name.'';             
                }

                
            }

            /*ver ipk*/
            $ver_ipk = $this->konfigurasi_model->verifikasi_ipk($value->pend_ipk);
            $ipk = ( $ver_ipk )?'<span style="font-size:12px; color:green; font-weight:bold">'.$value->pend_ipk.'</span>':'<span style="font-size:12px; color:red; font-weight:bold">'.$value->pend_ipk.' </span>';
            /*ver toefl*/
            $ver_toefl = $this->konfigurasi_model->verifikasi_toefl($value->bhs_nilai_toefl);
            $toefl = ( $ver_toefl )?'<span style="font-size:12px; color:green; font-weight:bold">'.$value->bhs_nilai_toefl.' </span>':'<span style="font-size:12px; color:red; font-weight:bold">'.$value->bhs_nilai_toefl.' </span>';

            $html .= '<tr>';
            $html .= '<td>'.$no.'</td>';
            $html .= '<td>'.$value->dp_nama_lengkap.'</td>';
            $html .= '<td>'.$ipk.'</td>';
            $html .= '<td>'.$toefl.'</td>';
            $html .= '<td>'.$dok.'</td>';
            $html .= '<td>'.$value->formasi_jenis_name.'</td>';
            $html .= '</tr>';
        }
        
        $html .= '</table>';

        echo json_encode(array('html' => $html));
    }

    public function find_data()
    {   
        $output = array(
                        "recordsTotal" => $this->verifikasi_data_cpns_model->count_all(),
                        "data" => $_POST,
                        "params_string" => http_build_query($_POST),
                );
        echo json_encode($output);
    }

    public function export_content()
    {   
        $data = $this->verifikasi_data_cpns_model->get_data_for_export();
        $html = '';
        $html .= '<table class="table table-striped table-bordered" >';
        $html .= '<thead>';
        $html .= '<tr>';
        $html .= '<th style=color: #242424; text-align: center; width:30px; > No</th>';
        $html .= '<th>Pas Foto</th>';
        $html .= '<th style=color: #242424;>Biodata Diri</th>';
        $html .= '<th style=color: #242424;>Kontak Informasi</th>';
        $html .= '<th style=color: #242424;>Kemampuan Bahasa Inggris</th>';
        $html .= '<th style=color: #242424;>Status</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $no = 0;
        foreach ($data as $row_list) {
            $no++;
            $html .= '<tr>';
            $html .= '<td>'.$no.'</td>';
            /*get pas foto pelamar*/
            $pas_foto = $this->verifikasi_data_cpns_model->get_file_pelamar_by_params( array('nik' => $row_list->dp_nik, 'file_category' => 1) );
            $src = isset($pas_foto->file_name)?'<img src="'.base_url().'uploaded_files/data_pelamar/'.$pas_foto->file_name.'" style="width:30px">':'-No Image Available-';
            $html .= '<td><div class="center">'.$src.'</div></td>';

            $html .= '<td>'.$row_list->dp_nik.'<br>'.strtoupper($row_list->dp_nama_lengkap).'<br>Jenis Kelamin : '.$row_list->jk.'<br> TTL : '.$row_list->dp_tempat_lahir.', '.$this->tanggal->formatDate($row_list->dp_tanggal_lahir).'</td>';

            $html .= '<td>'.$row_list->dp_alamat_lengkap.'<br>Email : '.$row_list->dp_email.'<br>Telp : '.$row_list->dp_no_telp.'</td>';

            /*pendidikan*/
            /*cek minimum IPK*/
            $ver_ipk = $this->konfigurasi_model->verifikasi_ipk($row_list->pend_ipk);
            $ipk = ( $ver_ipk )?'<span style="font-size:14px; color:green; font-weight:bold">'.$row_list->pend_ipk.' (Memenuhi Syarat)</span>':'<span style="font-size:14px; color:red; font-weight:bold">'.$row_list->pend_ipk.' (Belum Memenuhi Syarat)</span>';
            
            $html .= '<td>'.$row_list->univ_name.' <br><i>(Perguruan Tinggi "'.$row_list->univ_status.'" Akreditasi "'.$row_list->univ_akreditasi.'")</i><br> Jurusan "'.$row_list->majors_name.'"<br> IPK : '.$ipk.'<br> Lulus Tanggal '.$this->tanggal->formatDate($row_list->pend_tgl_ijasah).'</td>';
            /*end pendidikan*/

            /*kemampuan bahasa inggris*/
            /*cek minimum TOEFL*/
            $ver_toefl = $this->konfigurasi_model->verifikasi_toefl($row_list->bhs_nilai_toefl);
            $toefl = ( $ver_toefl )?'<span style="font-size:14px; color:green; font-weight:bold">'.$row_list->bhs_nilai_toefl.' (Memenuhi Syarat)</span>':'<span style="font-size:14px; color:red; font-weight:bold">'.$row_list->bhs_nilai_toefl.' (Belum Memenuhi Syarat)</span>';

            $html .= '<td>Nilai TOEFL : '.$toefl.' <br>Lemabaga TOEFL '.$row_list->bhs_lembaga_toefl.'<br> Tanggal Sertifikat : '.$this->tanggal->formatDate($row_list->bhs_tanggal_sertifikat).'</td>';

            $html .= '<td>'.($row_list->sv_id==1)?'<div class="center"><span class="label label-danger">'.$row_list->sv_name.'</span></div>':$row_list->sv_name.'<br><span style="font-size:11px">Tanggal : '.$this->tanggal->formatDateForm($row_list->verifikasi_tanggal).'<br>Oleh : '.$row_list->verifikasi_petugas.'</span><td>';
            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table>';

        echo $html;

        $this->exportExcelContent($html);

    }

    public function exportExcelContent($html_content){

        $random = rand(1,9999);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=Export-".date('dMY').'-'.$random.".xls");

        echo "<html>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<link rel='stylesheet' href='".base_url()."assets/admin/css/custom.css' />";
        echo "<body>";

        echo $html_content;
        
        echo "</body>";
        echo "</html>";
    }

    public function getKualifikasiPendByFormasiJabatan($fj_id='')
    {
        $query = $this->db->select('tr_formasi_jabatan_has_kualifikasi_pendidikan.kp_id, CONCAT(jenjang,"-",mst_majors.majors_name)as kp_name')->join('mst_kualifikasi_pendidikan','mst_kualifikasi_pendidikan.kp_id=tr_formasi_jabatan_has_kualifikasi_pendidikan.kp_id','left')->join('mst_majors','mst_majors.majors_id=mst_kualifikasi_pendidikan.majors_id','left')->where('fj_id', $fj_id)
                          ->group_by('tr_formasi_jabatan_has_kualifikasi_pendidikan.kp_id')
                          ->order_by('mst_kualifikasi_pendidikan.jenjang', 'ASC')
                          ->get('tr_formasi_jabatan_has_kualifikasi_pendidikan');
        
        echo json_encode($query->result());
    }

    public function getPenempatanByFormasiJabatan($fj_id='')
    {
        $query = $this->db->select('tr_formasi_jabatan_has_kualifikasi_pendidikan.up_id, mst_unit_penempatan.up_name')->join('mst_unit_penempatan','mst_unit_penempatan.up_id=tr_formasi_jabatan_has_kualifikasi_pendidikan.up_id','left')->where('fj_id', $fj_id)
                          ->group_by('mst_unit_penempatan.up_id')
                          ->order_by('mst_unit_penempatan.up_name', 'ASC')
                          ->get('tr_formasi_jabatan_has_kualifikasi_pendidikan');
        
        echo json_encode($query->result());
    }

    public function processReviewUlang()
    {
        $id = $this->input->post('ID');
        /*update status verifikasi*/
        $update_data = $this->verifikasi_data_cpns_model->update(array('dp_id'=>$id), array('verifikasi_status' => 1));
        if($update_data){
            echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan' ));
        }else{
            echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
        }

    }


}