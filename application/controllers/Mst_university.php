<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Mst_university extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('mst_university_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Master Data Universitas',
            'isi'  	=> 'mst_university/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Master Data Universitas',
            'isi'   => 'mst_university/form'
            );

        if ($id!='') {
            $data['value'] = $this->mst_university_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->mst_university_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = $row_list->univ_name;
            $row[] = $row_list->univ_akreditasi;
            $row[] = $row_list->univ_status;
            $row[] = '<div class="center">
            <a href="'.base_url().'mst_university/form/'.$row_list->univ_id.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'mst_university/delete/'.$row_list->univ_id.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->mst_university_model->count_all(),
                        "recordsFiltered" => $this->mst_university_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_FILES);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('univ_name', 'Nama Universitas', 'trim|required');
        $val->set_rules('univ_akreditasi', 'Akreditasi', 'trim|required');
        $val->set_rules('univ_status', 'Kategori', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            $data = array('title' => 'Add Data Master Data Universitas',
                'isi' => 'mst_university/form');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'univ_name' => $val->set_value('univ_name'),
                'univ_akreditasi' => $val->set_value('univ_akreditasi'),
                'univ_status' => $val->set_value('univ_status'),
            );
            //print_r($dataexc);die;



            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->mst_university_model->save($dataexc);
                /*save logs*/
                $this->logs->save('mst_university', $newId, 'insert new record on Master Data Universitas module', json_encode($dataexc),'univ_id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->mst_university_model->update(array('univ_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('mst_university', $newId, 'update record on Master Data Universitas module', json_encode($dataexc),'univ_id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                redirect(base_url().'mst_university/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
                redirect(base_url().'mst_university');
            }
        }
    }

    public function delete($id){
        $this->mst_university_model->delete_by_id($id);
        redirect(base_url().'mst_university');
    }

    public function get_detail_univ($id){
        $data = $this->mst_university_model->get_by_id($id);
        echo json_encode($data);
    }

    public function getMajorsByUniversity($univ_id='')
    {
        $query = $this->db->join('mst_majors','mst_majors.majors_id=tr_university_has_majors.majors_id','left')->where('univ_id', $univ_id)
                          ->group_by('tr_university_has_majors.majors_id', 'ASC')
                          ->order_by('mst_majors.majors_name', 'ASC')
                          ->get('tr_university_has_majors');
        
        echo json_encode($query->result());
    }

    public function getUniversityByKeyword(){
        
        $query = $this->db->where('univ_name like "%'.$_POST['keyword'].'%"')
                          ->order_by('mst_university.univ_name', 'ASC')
                          ->get('mst_university')->result();
        foreach ($query as $key => $value) {
                $arrResult[] = $value->univ_id.' : '.$value->univ_name;
        }
        
        echo json_encode($arrResult);

    }

    public function getDetailAkreditasiUniv($univ_id){
        
        $query = $this->db->where('univ_id', $univ_id)
                          ->order_by('mst_university.univ_name', 'ASC')
                          ->get('mst_university')->row();
        
        echo json_encode($query);

    }


}