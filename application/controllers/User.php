<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
	}
	
	// Index
	public function index() {
		$user = $this->user_model->listing();
		
		$data = array( 	'title' => 'Manajemen User',
						'user'	=> 	$user,
						'isi'  	=> 'user/list');
		$this->load->view('layout/wrapper',$data);
	}
	
	// Tambah User
	public function tambah() {
		
		// Validasi
		$valid = $this->form_validation;
		$valid->set_rules('nama','Nama','required',
		array( 'required' => 'Nama harus diisi'));
		
		$valid->set_rules('email','Email','required',
		array( 'required' => 'email harus diisi'));
		
		$valid->set_rules('usernama','Username','required|is_unique[users.usernama]',
		array( 	'required' 	=> 'Username harus diisi',
				'is_unique'	=> 'Username: <strong>'.$this->input->post('usernama').
							   '</strong> sudah digunakan. Buat username baru!'));
		
		$valid->set_rules('kuncimasuk','Password','required',
		array( 'required' => 'Password harus diisi'));
		
		if($valid->run()===FALSE) {
		// End validasi
		
		$data = array( 'title' => 'Tambah User',
						'isi'  => 'user/tambah');
		$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$i = $this->input;
			$data = array( 	'nama'			=> 	$i->post('nama'),
							'email'			=>	$i->post('email'),
							'usernama'		=>	$i->post('usernama'),
							'kuncimasuk'	=>	$i->post('kuncimasuk'),
							'akses_level'	=> $i->post('akses_level'));
			$this->user_model->tambah($data);
			$this->session->set_flashdata('sukses','user telah ditambah');
			redirect(base_url('user'));
		}
		// End masuk database
	}
	
	// Edit User
	public function edit($id_user) {
		$user = $this->user_model->detail($id_user);
		
		// Validasi
		$valid = $this->form_validation;
		$valid->set_rules('nama','Nama','required',
		array( 'required' => 'Nama harus diisi'));
		
		$valid->set_rules('email','Email','required',
		array( 'required' => 'email harus diisi'));
		
		
		$valid->set_rules('kuncimasuk','Password','required',
		array( 'required' => 'Password harus diisi'));
		
		if($valid->run()===FALSE) {
		// End validasi
		
		$data = array( 'title' 	=> 'Edit User',
						'user'	=> $user,
						'isi' 	=> 'user/edit');
		$this->load->view('layout/wrapper',$data);
		// masuk database
		}else{
			$i = $this->input;
			$data = array( 	'nama'			=> 	$i->post('nama'),
							'email'			=>	$i->post('email'),
							'usernama'		=>	$i->post('usernama'),
							'kuncimasuk'		=>	$i->post('kuncimasuk'),
							'akses_level'	=> $i->post('akses_level'));
			$this->user_model->edit($data,$id_user);
			$this->session->set_flashdata('sukses','user telah ditambah');
			redirect(base_url('user'));
		}
		// End masuk database
	}
	
	// Delete User
	public function delete($id_user) {
		$this->simple_login->cek_login();
		$data = array('id_user'=> $id_user);
		$this->user_model->delete($data);
		$this->session->set_flashdata('sukses','user telah dihapus');
		redirect (base_url('user'));
	}
}