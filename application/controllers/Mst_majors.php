<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Mst_majors extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('mst_majors_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Master Data Jurusan',
            'isi'  	=> 'mst_majors/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Master Data Jurusan',
            'isi'   => 'mst_majors/form'
            );

        if ($id!='') {
            $data['value'] = $this->mst_majors_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->mst_majors_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = $row_list->majors_name;
            $row[] = '<div class="center">
            <a href="'.base_url().'mst_majors/form/'.$row_list->majors_id.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'mst_majors/delete/'.$row_list->majors_id.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->mst_majors_model->count_all(),
                        "recordsFiltered" => $this->mst_majors_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_FILES);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('majors_name', 'Nama Jurusan', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            $data = array('title' => 'Add Data Master Data Jurusan',
                'isi' => 'mst_majors/form');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'majors_name' => $val->set_value('majors_name'),
            );
            //print_r($dataexc);die;



            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->mst_majors_model->save($dataexc);
                /*save logs*/
                $this->logs->save('mst_majors', $newId, 'insert new record on Master Data Jurusan module', json_encode($dataexc),'majors_id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->mst_majors_model->update(array('majors_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('mst_majors', $newId, 'update record on Master Data Jurusan module', json_encode($dataexc),'majors_id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
                redirect(base_url().'mst_majors/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
                redirect(base_url().'mst_majors');
            } 
        }
    }

     public function delete($id){
        $this->mst_majors_model->delete_by_id($id);
        redirect(base_url().'mst_majors');
    }

    public function get_detail_majors($majors_id, $univ_id){
        $data = $this->mst_majors_model->get_majors_by_id_and_univ($majors_id, $univ_id);
        echo json_encode($data);
    }

    public function getMajorsByKeyword(){
        $univ_id = $_POST['univ_id'];
        $query = $this->db->where('univ_id', $univ_id)->where('majors_name like "%'.$_POST['keyword'].'%"')->join('mst_majors','mst_majors.majors_id=tr_university_has_majors.majors_id','left')
                          ->group_by('tr_university_has_majors.majors_id')
                          ->order_by('mst_majors.majors_name', 'ASC')
                          ->get('tr_university_has_majors')->result();                          
        $arrResult = array();
        foreach ($query as $key => $value) {
                $arrResult[] = (int)$value->majors_id.':'.$value->majors_name;
        }
        
        echo json_encode($arrResult);

    }

}