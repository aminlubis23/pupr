<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Uploadscnbkn extends CI_Controller {
	private $filename = "import_data"; // Kita tentukan nama filenya

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('uploadscnbkn_model');
        $this->load->model('user_model');
	}

	public function index(){
		$site	= $this->konfigurasi_model->listing();
        $user	= $this->user_model->listing();
        //$uploadscnbkn = $this->uploadscnbkn_model->listing();
        $uploadscnbkn = $this->uploadscnbkn_model->uploadscnbkn();

        //$data['siswa'] = $this->SiswaModel->view();

        $data	= array( 'title'	=> 'Summary Data Pelamar Page || '.$site['namaweb'].' | '.$site['tagline'],
            'keywords' => 'Summary Data Pelamar Page '.$site['namaweb'].', '.$site['keywords'],
            'uploadscnbkn'	=> $uploadscnbkn,
            'isi'		=> 'master/uploadscnbkn');
        $this->load->view('layout/wrapper',$data);
	}

	public function form(){
		$data = array(); // Buat variabel $data sebagai array

		if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di UploadscnbknModel.php
			//$upload = $this->UploadscnbknModel->upload_file($this->filename);
            $upload = $this->uploadscnbkn_model->upload_file($this->filename);

			if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			}else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/master/form_upload', $data);
	}

	public function import(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = [];

		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, [
          'id'=>$row['A'], // Insert data id dari kolom A di excel
			    'nik'=>$row['B'], // Ambil data nik
			    'no_kk'=>$row['C'], // Ambil data jenis kelamin
			    'nik_kk'=>$row['D'], // Ambil data alamat
			    'password'=>$row['E'], // Ambil data alamat
			    'nama'=>$row['F'], // Ambil data alamat
			    'nama_ijazah'=>$row['G'], // Ambil data alamat
			    'tempat_lahir'=>$row['H'], // Ambil data alamat
			    'tempat_lahir_ijazah'=>$row['I'], // Ambil data alamat
			    'tanggal_lahir'=>$row['J'], // Ambil data alamat
			    'tanggal_lahir_ijazah'=>$row['K'], // Ambil data alamat
          'jenis_kelamin'=>$row['L'], // Ambil data alamat
			    'email'=>$row['M'], // Ambil data alamat
			    'pertanyaan_pengaman_1'=>$row['N'], // Ambil data alamat
			    'jawaban_1'=>$row['O'], // Ambil data alamat
			    'pertanyaan_pengaman_2'=>$row['P'], // Ambil data alamat
			    'jawaban_2'=>$row['Q'], // Ambil data alamat
			    'agama'=>$row['R'],// Ambil data alamat
			    'alamat_ktp'=>$row['S'], // Ambil data alamat
			    'alamat_domisili'=>$row['T'], // Ambil data alamat
			    'tinggi_badan'=>$row['U'], // Ambil data alamat
			    'status_kawin'=>$row['V'], // Ambil data alamat
			    'no_telp'=>$row['W'], // Ambil data alamat
			    'no_hp'=>$row['X'], // Ambil data alamat
			    'nama_ibu'=>$row['Y'], // Ambil data alamat
			    'nama_ayah'=>$row['Z'], // Ambil data alamat
			    'kodepos'=>$row['AA'], // Ambil data alamat
			    'lokasi_kabkota'=>$row['AB'], // Ambil data alamat
			    'tgl_daftar'=>$row['AC'], // Ambil data alamat
			    'remarks'=>$row['AD'], // Ambil data alamat
				]);
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Uploadscnbkn_model->insert_multiple($data);

		//redirect("Uploadscnbkn"); // Redirect ke halaman awal (ke controller exportdatascnbkn fungsi index)
    redirect("/admin/Uploadscnbkn");
	}
}
