<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Set_parameter extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('set_parameter_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'value' => $this->set_parameter_model->get_by_id(1),
            'title' => 'Konfigurasi Global Parameter',
            'isi'  	=> 'set_parameter/form'
            );
        $this->load->view('layout/wrapper',$data);
    }

    public function process()
    {

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('min_toefl', 'Nilai Minimal TOEFL', 'trim|required');
        $val->set_rules('min_ipk', 'Nilai Minimal IPK', 'trim|required');
        $val->set_rules('max_usia_ptt', 'Usia Maximal PTT', 'trim|required');
        $val->set_rules('max_usia_s1', 'Usia Maximal S1', 'trim|required');
        $val->set_rules('max_usia_s2', 'Usia Maximal S2', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            $data = array('title' => 'Add Data Konfigurasi Global Parameter',
                'isi' => 'set_parameter/tambah');
            $this->load->view('layout/wrapper', $data);
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'min_toefl' => $val->set_value('min_toefl'),
                'min_ipk' => $val->set_value('min_ipk'),
                'max_usia_ptt' => $val->set_value('max_usia_ptt'),
                'max_usia_s1' => $val->set_value('max_usia_s1'),
                'max_usia_s2' => $val->set_value('max_usia_s2'),
            );

            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->set_parameter_model->save($dataexc);
                /*save logs*/
                $this->logs->save('global_parameter', $newId, 'insert new record on Konfigurasi Global Parameter module', json_encode($dataexc),'id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->set_parameter_model->update(array('id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('global_parameter', $newId, 'update record on Konfigurasi Global Parameter module', json_encode($dataexc),'id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan', 'redirect' => base_url().'set_parameter'));
                //redirect(base_url().'set_parameter/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => base_url().'set_parameter'));
                //redirect(base_url().'set_parameter');
            }
        }
    }

    public function getById() {
        $data = $this->set_parameter_model->get_by_id(1);
        echo json_encode($data);
    }


}