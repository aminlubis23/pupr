<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Faq_admin extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('faq_admin_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Bantuan dan FAQ',
            'isi'  	=> 'faq_admin/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Bantuan dan FAQ',
            'isi'   => 'faq_admin/form'
            );

        if ($id!='') {
            $data['value'] = $this->faq_admin_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->faq_admin_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = $row_list->question;
            $row[] = $row_list->answer;
            $row[] = ($row_list->category==1)?'Kategori Pelamar':'Persyaratan';
            $row[] = $this->tanggal->formatDate($row_list->created_date);
            $row[] = '<div class="center">
            <a href="'.base_url().'faq_admin/form/'.$row_list->faq_id.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'faq_admin/delete/'.$row_list->faq_id.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->faq_admin_model->count_all(),
                        "recordsFiltered" => $this->faq_admin_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_FILES);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('question', 'Pertanyaan', 'trim|required');
        $val->set_rules('answer', 'Jawaban', 'trim|required');
        $val->set_rules('category', 'Kategori', 'trim');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'question' => $val->set_value('question'),
                'answer' => $val->set_value('answer'),
                'category' => $val->set_value('category'),
            );

            /*upload*/
            if($_FILES['file']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $old = $this->faq_admin_model->get_by_id($id);
                    if (file_exists('assets/upload/image/'.$old->file.'')) {
                        unlink('assets/upload/image/'.$old->file.'');
                    }
                }

                $dataexc['file'] = $this->upload_file->doUpload('file', 'assets/upload/image/');
            }


            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->faq_admin_model->save($dataexc);
                /*save logs*/
                $this->logs->save('faq', $newId, 'insert new record on Tata Cara module', json_encode($dataexc),'faq_id');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->faq_admin_model->update(array('faq_id' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('faq', $newId, 'update record on Tata Cara module', json_encode($dataexc),'faq_id');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan', 'redirect' => base_url().'faq_admin'));
                //redirect(base_url().'faq_admin/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'redirect' => base_url().'faq_admin'));
                //redirect(base_url().'faq_admin');
            }
        }
    }

     public function delete($id){
        $this->faq_admin_model->delete_by_id($id);
        redirect(base_url().'faq_admin');
    }

}