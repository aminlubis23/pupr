<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 29/07/2018
 * Time: 17:55
 */

class User_profile extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('user_model');
        //$this->load->model('video_model');
        $this->load->model('userprofile_model');
        //$this->load->model('')
        //$this->load->model('berita_model');
        //$this->load->model('produk_model');
        //$this->load->model('kategori_produk_model');
        //$this->load->model('kategori_berita_model');
    }
    // Index
    public function index() {
        $site	= $this->konfigurasi_model->listing();
        $user	= $this->user_model->listing();
        //$userprofile = $this->userprofile_model->listing();
        //$video				= $this->video_model->listing();
        //$pengumuman				= $this->userprofile_model->listing();
        //$berita				= $this->berita_model->listing();
        //$produk				= $this->produk_model->listing();
        //$kategori_produk	= $this->kategori_produk_model->listing();
        //$kategori_berita	= $this->kategori_berita_model->listing();

        $data = array('title'	=> 'User Dashboard Page - '.$site['namaweb'],
            'keywords' => 'User Dashboard Page - ' . $site['namaweb'] . ', ' . $site['keywords'],
            'user'				=> $user,
            //'video'				=> $video,
            //'berita'			=> $berita,
            //'produk'			=> $produk,
            //'kategori_produk'	=> $kategori_produk,
            //'kategori_berita'	=> $kategori_berita,
            'isi'				=> 'admin/user_profile/list');
        //$this->load->view('admin/layout/wrapper',$data);
        $this->load->view('admin/layout/wrapper_user',$data);
    }

    public function kartupesertaSKD(){
        $site	= $this->konfigurasi_model->listing();
        $user	= $this->user_model->listing();


        $data = array('title'	=> 'Kartu Peserta Seleksi Kompetensi Dasar Priview Page - '.$site['namaweb'],
            'keywords' => 'Kartu Peserta Seleksi Kompetensi Dasar Priview Page - ' . $site['namaweb'] . ', ' . $site['keywords'],
            'user'				=> $user,
            //'video'				=> $video,
            //'berita'			=> $berita,
            //'produk'			=> $produk,
            //'kategori_produk'	=> $kategori_produk,
            //'kategori_berita'	=> $kategori_berita,
            'isi'				=> 'admin/user_profile/ksksd');
        //$this->load->view('admin/layout/wrapper',$data);
        $this->load->view('admin/layout/wrapper_user',$data);
    }
}