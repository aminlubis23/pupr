<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Pengumuman_admin extends CI_Controller
{   // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('pengumuman_admin_model');
    }

    // Index
    public function index() {
        $data = array( 	
            'title' => 'Pengumuman',
            'isi'  	=> 'pengumuman_admin/index'
            );
        $this->load->view('layout/wrapper',$data);
    }

    // Index
    public function form($id='') {

        $data = array(  
            'title' => 'Pengumuman',
            'isi'   => 'pengumuman_admin/form'
            );

        if ($id!='') {
            $data['value'] = $this->pengumuman_admin_model->get_by_id($id);
        }

        $this->load->view('layout/wrapper',$data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->pengumuman_admin_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">'.$no.'</div>';
            $row[] = $this->tanggal->formatDate($row_list->tanggal_pengumuman);
            $row[] = $row_list->judul_pengumuman;
            $row[] = $row_list->nama_kategori_pengumuman;
            $row[] = $row_list->deskripsi_pengumuman;
            $row[] = ($row_list->status_pengumuman == 'NEW') ? '<div class="center"><span class="label label-sm label-success">Newest</span></div>' : '<div class="center"><span class="label label-sm label-danger">Expired</span></div>';
            $row[] = '<div class="center">
            <a href="'.base_url().'pengumuman_admin/form/'.$row_list->id_pengumuman.'" class="btn btn-xs btn-success" ><i class="ace-icon fa fa-edit bigger-50"></i></a>
            <a href="'.base_url().'pengumuman_admin/delete/'.$row_list->id_pengumuman.'" class="btn btn-xs btn-danger"><i class="ace-icon fa fa-times bigger-50"></i></a>

            </div>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->pengumuman_admin_model->count_all(),
                        "recordsFiltered" => $this->pengumuman_admin_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        //print_r($_POST);die;

        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('no_pengumuman', 'Nomor SK Pengumuman', 'trim');
        $val->set_rules('judul_pengumuman', 'Judul', 'trim|required');
        $val->set_rules('tanggal_pengumuman', 'Tanggal', 'trim|required');
        $val->set_rules('deskripsi_pengumuman', 'Deskripsi', 'trim');
        $val->set_rules('id_kategori_pengumuman', 'Kategori Pengumuman', 'trim');
        $val->set_rules('status_pengumuman', 'Kategori Pengumuman', 'trim');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'no_pengumuman' => $val->set_value('no_pengumuman'),
                'judul_pengumuman' => $val->set_value('judul_pengumuman'),
                'tanggal_pengumuman' => $val->set_value('tanggal_pengumuman'),
                'deskripsi_pengumuman' => $val->set_value('deskripsi_pengumuman'),
                'id_kategori_pengumuman' => $val->set_value('id_kategori_pengumuman'),
                'status_pengumuman' => $val->set_value('status_pengumuman'),
            );
            //print_r($dataexc);die;

            /*upload*/
            if($_FILES['file_pengumuman']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $old = $this->pengumuman_admin_model->get_by_id($id);
                    if (file_exists('assets/upload/image/'.$old->image.'')) {
                        unlink('assets/upload/image/'.$old->image.'');
                    }
                }

                $dataexc['file_pengumuman'] = $this->upload_file->doUpload('file_pengumuman', 'assets/upload/image/');
            }


            //print_r($dataexc);die;
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*save post data*/
                $newId = $this->pengumuman_admin_model->save($dataexc);
                /*save logs*/
                $this->logs->save('pengumuman', $newId, 'insert new record on Pengumuman module', json_encode($dataexc),'id_pengumuman');
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>'', 'fullname' => $this->session->userdata('nama')));
                /*update record*/
                $this->pengumuman_admin_model->update(array('id_pengumuman' => $id), $dataexc);
                $newId = $id;
                /*save logs*/
                $this->logs->save('pengumuman', $newId, 'update record on Pengumuman module', json_encode($dataexc),'id_pengumuman');
            }
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan','redirect' => base_url().'pengumuman_admin'));
                //redirect(base_url().'pengumuman_admin/create');
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan','redirect' => base_url().'pengumuman_admin'));
                //redirect(base_url().'pengumuman_admin');
            }
        }
    }

     public function delete($id){
        $this->pengumuman_admin_model->delete_by_id($id);
        redirect(base_url().'pengumuman_admin');
    }

}