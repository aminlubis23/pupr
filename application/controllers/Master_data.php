<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 16/08/2018
 * Time: 19:36
 */

class Master_data extends CI_Controller
{
    // Load database
    public function __construct(){
        parent::__construct();
        $this->load->model('konfigurasi_model');
        $this->load->model('master_data_model');
        $this->load->model('user_model');
    }

    // ini indexnya jenis kelamin
    public function indexjeniskelamin() {
        $site	= $this->konfigurasi_model->listing();
        //$user	= $this->user_model->listing();
        $jengkel = $this->master_data_model->listingjengkel();

        $data	= array( 'title'	=> 'Master Data Jenis Kelamin Admin Side || '.$site['namaweb'].' | '.$site['tagline'],
            'keywords' => 'Master Data Jenis Kelamin Admin Side '.$site['namaweb'].', '.$site['keywords'],
            'jengkel'	=> $jengkel,
            'isi'		=> 'jengkel/list');
        $this->load->view('layout/wrapper',$data);
    }

    // Tambah Jenis Kelamin
    public function tambahjeniskelamin() {

        // Validasi
        $valid = $this->form_validation;
        $valid->set_rules('kode_jengkel','Kode','required|is_unique[dtl_jengkel.kode]',
            array( 	'required' 	=> 'Kode harus diisi',
                'is_unique'	=> 'Kode: <strong>'.$this->input->post('kode').
                    '</strong> sudah digunakan. Buat Kode baru!'));

        $valid->set_rules('nama_jengkel','Nama','required|is_unique[dtl_jengkel.nama]',
            array( 	'required' 	=> 'Nama Jenis Kelamin harus diisi',
                'is_unique'	=> 'Nama: <strong>'.$this->input->post('nama').
                    '</strong> sudah digunakan. Buat Nama baru!'));

        $valid->set_rules('desk_jengkel','Deskripsi','required',
            array( 'required' => 'Deskripsi harus diisi'));

        if($valid->run()===FALSE) {
            // End validasi

            $data = array( 'title' => 'Tambah Jenis Kelamin',
                'isi'  => 'jengkel/list');
            $this->load->view('layout/wrapper',$data);
            // masuk database
        }else{
            $i = $this->input;
            $insertuser = $this->session->nama;
            $data = array( 	'kode'			=> 	$i->post('kode_jengkel'),
                'nama'			=>	$i->post('nama_jengkel'),
                'deskripsi'		=>	$i->post('desk_jengkel'),
                'create_date'	=> date('Y-m-d H:i:s'),
                'create_by'	=> $insertuser,
                'remarks' => $i->post('desk_jengkel'));
            $this->master_data_model->tambahjengkel($data);
            $this->session->set_flashdata('sukses','Jenis Kelamin telah ditambah');
            redirect(base_url('master_data/indexjeniskelamin'));
        }
        // End masuk database
    }

    // Delete Jenis Kelamin
    public function deletejeniskelamin($id_jengkel) {
        $this->simple_login->cek_login();
        $data = array('id_jengkel'=> $id_jengkel);
        //$this->master_data_model->deletejeniskelamin($data);
        $this->master_data_model->deletejengkel($data);
        $this->session->set_flashdata('sukses','Jenis Kelamin telah dihapus');
        redirect (base_url('master_data/indexjeniskelamin'));
    }

    // ini indexnya jenis kelamin
    public function indexstatus() {
        $site	= $this->konfigurasi_model->listing();
        //$user	= $this->user_model->listing();
        $jengkel = $this->master_data_model->listingjengkel();

        $data	= array( 'title'	=> 'Master Data Jenis Kelamin Admin Side || '.$site['namaweb'].' | '.$site['tagline'],
            'keywords' => 'Master Data Jenis Kelamin Admin Side '.$site['namaweb'].', '.$site['keywords'],
            'jengkel'	=> $jengkel,
            'isi'		=> 'jengkel/list');
        $this->load->view('layout/wrapper',$data);
    }

    // Tambah Jenis Status
    public function tambahstatus() {

        // Validasi
        $valid = $this->form_validation;
        $valid->set_rules('kode_jengkel','Kode','required|is_unique[dtl_jengkel.kode]',
            array( 	'required' 	=> 'Kode harus diisi',
                'is_unique'	=> 'Kode: <strong>'.$this->input->post('kode').
                    '</strong> sudah digunakan. Buat Kode baru!'));

        $valid->set_rules('nama_jengkel','Nama','required|is_unique[dtl_jengkel.nama]',
            array( 	'required' 	=> 'Nama Jenis Kelamin harus diisi',
                'is_unique'	=> 'Nama: <strong>'.$this->input->post('nama').
                    '</strong> sudah digunakan. Buat Nama baru!'));

        $valid->set_rules('desk_jengkel','Deskripsi','required',
            array( 'required' => 'Deskripsi harus diisi'));

        if($valid->run()===FALSE) {
            // End validasi

            $data = array( 'title' => 'Tambah Jenis Kelamin',
                'isi'  => 'jengkel/list');
            $this->load->view('layout/wrapper',$data);
            // masuk database
        }else{
            $i = $this->input;
            $insertuser = $this->session->nama;
            $data = array( 	'kode'			=> 	$i->post('kode_jengkel'),
                'nama'			=>	$i->post('nama_jengkel'),
                'deskripsi'		=>	$i->post('desk_jengkel'),
                'create_date'	=> date('Y-m-d H:i:s'),
                'create_by'	=> $insertuser,
                'remarks' => $i->post('desk_jengkel'));
            $this->master_data_model->tambahjengkel($data);
            $this->session->set_flashdata('sukses','Jenis Kelamin telah ditambah');
            redirect(base_url('master_data/indexjeniskelamin'));
        }
        // End masuk database
    }

    // Delete Jenis Kelamin
    public function deletestatus($id_jengkel) {
        $this->simple_login->cek_login();
        $data = array('id_jengkel'=> $id_jengkel);
        //$this->master_data_model->deletejeniskelamin($data);
        $this->master_data_model->deletejengkel($data);
        $this->session->set_flashdata('sukses','Jenis Kelamin telah dihapus');
        redirect (base_url('master_data/indexjeniskelamin'));
    }
}