<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class upload_file {

    function doUpload($inputname, $path)
    {
        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);

        $random = rand(1,99);
        $unique_filename = str_replace(' ','_', $random.preg_replace('/\s+/', '', $_FILES[''.$inputname.'']['name']));
        $vfile_upload = $path . $unique_filename;
        $type_file = $_FILES[''.$inputname.'']['type'];
        if(move_uploaded_file($_FILES[$inputname]["tmp_name"], $vfile_upload)){
            return $unique_filename;
        }else{
            return false;
        }

    }

    function doUploadCallbackFullResponse($inputname, $path)
    {
        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);

        $random = rand(1,99);
        $unique_filename = str_replace(' ','_', $random.preg_replace('/\s+/', '', $_FILES[''.$inputname.'']['name']));
        $vfile_upload = $path . $unique_filename;
        $type_file = $_FILES[''.$inputname.'']['type'];
        if(move_uploaded_file($_FILES[$inputname]["tmp_name"], $vfile_upload)){
            $response = array(
                'name' => $_FILES[''.$inputname.'']['name'],
                'path' => $unique_filename,
                'fullpath' => $path.$unique_filename,
                'type' => $type_file,
                'size' => $_FILES[''.$inputname.'']['size']
            );
            return $response;
        }else{
            return false;
        }

    }


    function getUploadedFile($params){

        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);
        $html = '';
        $db->order_by('attc_id', 'ASC');
        if (is_array($params['ref_id'])) {
            $db->where_in('ref_id', $params['ref_id']);
            # code...
        }else{
            $db->where('ref_id', $params['ref_id']);
        }
        
        $files = $db->get('tr_attachment')->result();

        $html .= '<ul class="ace-thumbnails clearfix">';
        foreach ($files as $key => $value) {
            $html .= '<li id="file_'.$value->attc_id.'">';
                if (in_array($value->attc_type, array('image/jpeg','image/png')) ) {
                    $link_src = base_url().'assets/img/img2.png';
                    $link_href = base_url().$value->attc_fullpath;
                }else{
                    $link_src = base_url().'assets/img/doc.png';
                    $link_href = base_url().'assets/img/doc.png';
                }

                $html .= '<a href="'.$link_href.'" data-rel="colorbox">
                        <img width="150" height="150" alt="150x150" src="'.$link_src.'" />
                        <div class="text"><div class="inner">'.$value->attc_type.'</div></div>
                      </a>';

                          $html .= '<div class="tools tools-bottom"><a href="'.base_url().$value->attc_fullpath.'" target="blank"><i class="ace-icon fa fa-link"></i></a>
                            <a href="'.base_url().$value->attc_fullpath.'"><i class="ace-icon fa fa-download" target="blank"></i></a>
                            <a href="#" onclick="delete_attachment('.$value->attc_id.')"><i class="ace-icon fa fa-times red"></i></a>
                          </div>
                        </li>';
        }
        $html .= '</ul>';
        return $html;

    }

    function doUploadMultiple($params)
    {
        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);
        $CI->load->library('upload');
        //$CI->load->library('image_lib'); 
        $getData = array();

        foreach ($_FILES[''.$params['name'].'']['name'] as $i=>$values) {

            if(!empty($values)){

                $_FILES['userfile']['name']     = $_FILES[''.$params['name'].'']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES[''.$params['name'].'']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES[''.$params['name'].'']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES[''.$params['name'].'']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES[''.$params['name'].'']['size'][$i];

                $random = $params['nik'].'_'.$i.'_';
                $nama_file_unik = str_replace(' ','_', $random.$_FILES[''.$params['name'].'']['name'][$i]);
                $type_file = $_FILES[''.$params['name'].'']['type'][$i];

                $config = array(
                'allowed_types' => '*',
                'file_name'     => $nama_file_unik,
                'max_size'      => '999999',
                'overwrite'     => TRUE,
                'remove_spaces' => TRUE,
                'upload_path'   => $params['path']
                );

                $CI->upload->initialize($config);

                if ($_FILES['userfile']['tmp_name']) {

                  if ( ! $CI->upload->do_upload()) :
                    $error = array('error' => $CI->upload->display_errors());
                  else :

                    $data = array( 'upload_data' => $CI->upload->data() );
                    /*cek attchment exist*/
                    
                    $datainsertattc = array(
                        'file_category' => $i,
                        'file_name' => $nama_file_unik,
                        'file_size' => $_FILES[''.$params['name'].'']['size'][$i],
                        'file_type' => $type_file,
                        'file_path' => $params['path'].$nama_file_unik,
                        'nik' => $params['nik'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_by' => 'CPNS',
                    );
                    $CI->db->insert('tr_file_pelamar', $datainsertattc);

                    $getData[] = $datainsertattc;

                  endif;

                }
            }
            
                
        }

            return $getData;
    } 

    function check_existing($params){

        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);

        $files = $this->getUploadedFile($params, 'data');
        /*if exist file*/
        if(count($files) > 0 ){
           foreach ($files as $key => $value) {
                if(file_exists($value->attc_fullpath)){
                    unlink($value->attc_fullpath);
                }
                $CI->db->delete('tr_attachment', array('attc_id' => $value->attc_id));
            }
        }

        return true;

    }

	   
}

?>
