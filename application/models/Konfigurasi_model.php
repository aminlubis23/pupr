<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Konfigurasi_model extends CI_Model {
	
	public function __construct() {
		$this->load->database();
	}
	
	// Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('konfigurasi');
		$this->db->order_by('id_konfigurasi','DESC');
		$query = $this->db->get();
		return $query->row_array();
	}

	
	
	// Detail
	public function detail($id_konfigurasi) {
		$this->db->select('*');
		$this->db->from('konfigurasi');
		$this->db->where('id_konfigurasi',$id_konfigurasi);
		$this->db->order_by('id_konfigurasi','DESC');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function get_config_web() {
		$this->db->select('*');
		$this->db->from('global_parameter');
		$this->db->where('id',1);
		$query = $this->db->get();
		return $query->row();
	}

	public function verifikasi_ipk($ipk) {
		$config = $this->get_config_web();
		if( $config->min_ipk < $ipk ){
			return true;
		}else{
			return false;
		}

	}

	public function verifikasi_toefl($toefl) {
		$config = $this->get_config_web();
		if( $config->min_toefl < $toefl ){
			return true;
		}else{
			return false;
		}

	}

	public function verifikasi_umur($nik, $umur) {
		$config = $this->get_config_web();
		$data_cpns = $this->verifikasi_data_cpns_model->get_by_nik($nik);
		/*cek apakah PTT*/
		$is_ptt = $data_cpns->verifikasi_is_ptt;
		if($is_ptt=='Y'){
			if( $config->max_usia_ptt > $umur ){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
	public function get_menu_level($level) {

		$menu = [];

		$menu[0] = array(
				'name' => 'Dashboard',
				'link' => 'dashboard',
				'submenu' => array(),
				);

		/*superadmin*/
		if($level==0){

			$menu[1] = array(
				'name' => 'Data CPNS',
				'link' => '#',
				'submenu' => array(
						array('name' => 'Verifikasi Data CPNS', 'link' => 'verifikasi_data_cpns'),
						array('name' => 'Statistik Data', 'link' => 'statistik_data'),
					),
				);

			$menu[2] = array(
				'name' => 'Web Content',
				'link' => '#',
				'submenu' => array(
					array('name' => 'Pengumuman', 'link' => 'pengumuman_admin'),
					array('name' => 'Tata Cara', 'link' => 'tatacara_admin'),
					array('name' => 'Bantuan dan FAQ', 'link' => 'faq_admin'),
					array('name' => 'Upload Dokumen', 'link' => 'dokumen_admin'),
					array('name' => 'Slider', 'link' => 'slider_setting'),
					array('name' => 'Login CPNS 1 Setting', 'link' => 'katalogincpnskiri'),
					array('name' => 'Layanan Online Setting', 'link' => 'pertanyaan_editor'),
					array('name' => 'Home Content Setting', 'link' => 'home_setting'),
					array('name' => 'Web Template', 'link' => 'template'),
					),
				);
			$menu[3] = array(
				'name' => 'Master Data',
				'link' => '#',
				'submenu' => array(
					array('name' => 'Jenis Kelamin', 'link' => 'mst_gender'),
					array('name' => 'Agama', 'link' => 'mst_religion'),
					array('name' => 'Status Nikah', 'link' => 'mst_marital_status'),
					array('name' => 'Universitas', 'link' => 'mst_university'),
					array('name' => 'Jurusan', 'link' => 'mst_majors'),
					array('name' => 'Kategori Formasi Jabatan', 'link' => 'mst_kategori_formasi_jabatan'),
					array('name' => 'Formasi Jabatan', 'link' => 'mst_formasi_jabatan'),
					),
				);

			$menu[4] = array(
				'name' => 'Konfigurasi',
				'link' => '#',
				'submenu' => array(
					array('name' => 'Global Parameter', 'link' => 'set_parameter'),
					array('name' => 'Akreditasi Jurusan', 'link' => 'tr_akreditasi_jurusan'),
					array('name' => 'Kualifikasi Pendidikan', 'link' => 'mst_kualifikasi_pendidikan'),
					array('name' => 'Jabatan Formasi Pendidikan', 'link' => 'tr_jabatan_has_kualifikasi_pendidikan'),
					array('name' => 'Lokasi Ujian', 'link' => 'mst_lokasi_ujian'),
					),
				);

			$menu[5] = array(
				'name' => 'Manajemen',
				'link' => '#',
				'submenu' => array(
					/*array('name' => 'Statistik Data', 'link' => 'statistik_data'),*/
					array('name' => 'Import Data BKN', 'link' => 'data_import'),
					array('name' => 'Import Hasil Ujian SKD', 'link' => 'import_skd'),
					array('name' => 'Import Hasil Ujian SKB', 'link' => 'import_skb'),
					array('name' => 'User Internal', 'link' => 'user_internal'),
					array('name' => 'User CPNS', 'link' => 'user_cpns'),
					array('name' => 'Logout', 'link' => 'login/logout'),
					),
				);
		}

		/*admin content*/
		if($level==1){

			/*$menu[1] = array(
				'name' => 'Web Content',
				'link' => '#',
				'submenu' => array(
					array('name' => 'Pengumuman', 'link' => 'pengumuman_admin'),
					array('name' => 'Tata Cara', 'link' => 'tatacara_admin'),
					array('name' => 'Bantuan dan FAQ', 'link' => 'faq_admin'),
					array('name' => 'Upload Dokumen', 'link' => 'dokumen_admin'),
					array('name' => 'Slider Admin', 'link' => 'slider_setting'),
					),
				);*/

			$menu[1] = array(
				'name' => 'Web Content',
				'link' => '#',
				'submenu' => array(
					array('name' => 'Pengumuman', 'link' => 'pengumuman_admin'),
					array('name' => 'Tata Cara', 'link' => 'tatacara_admin'),
					array('name' => 'Bantuan dan FAQ', 'link' => 'faq_admin'),
					array('name' => 'Upload Dokumen', 'link' => 'dokumen_admin'),
					array('name' => 'Slider', 'link' => 'slider_setting'),
					array('name' => 'Login CPNS 1 Setting', 'link' => 'katalogincpnskiri'),
					array('name' => 'Login CPNS 2 Setting', 'link' => '#'),
					array('name' => 'Layanan Online Setting', 'link' => 'pertanyaan_editor'),
					),
				);


			$menu[2] = array(
				'name' => 'Logout',
				'link' => 'login/logout',
				'submenu' => array(),
				);

		}

		/*cpns*/
		elseif ($level==3) {
			$menu[1] = array(
				'name' => 'Kelengkapan Data CPNS',
				'link' => 'data_import/form/'.$this->session->userdata('user')->username.'',
				'submenu' => array(),
				);

			$menu[2] = array(
				'name' => 'Setting',
				'link' => '#',
				'submenu' => array(
					array('name' => 'Ubah Password', 'link' => 'login/change_password'),
					array('name' => 'Logout', 'link' => 'login/logout'),
					),
				);

		/*verifikator*/
		}elseif ($level==2) {

			$menu[1] = array(
				'name' => 'Verifikasi',
				'link' => 'verifikasi_data_cpns',
				'submenu' => array(
					array('name' => 'Data BKN', 'link' => 'data_import'),
					array('name' => 'Verifikasi Data CPNS', 'link' => 'verifikasi_data_cpns'),
					),
				);

			$menu[2] = array(
				'name' => 'Statistik',
				'link' => 'statistik_data',
				'submenu' => array(),
				);

			/*$menu[3] = array(
				'name' => 'Pelaporan',
				'link' => '#',
				'submenu' => array(
						array('name' => 'Data Pelamar', 'link' => '#'),
						array('name' => 'Hasil Verifikasi', 'link' => '#'),
						array('name' => 'Hasil Ujian SKD', 'link' => '#'),
						array('name' => 'Hasil Ujian SKB', 'link' => '#'),
					),
				);*/
			$menu[4] = array(
				'name' => 'Logout',
				'link' => 'login/logout',
				'submenu' => array(),
				);

		}

		return $menu;

		
	}

}