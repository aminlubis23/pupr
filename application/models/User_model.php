<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->order_by('user_id','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	
	// detail peruser
	public function detail($user_id){
		$query = $this->db->get_where('users',array('user_id'  => $user_id));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('users',$data);
	}
	
	// Edit 
	public function edit ($data,$user_id) {
		$this->db->where('user_id',$user_id);
		$this->db->update('users',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('user_id',$data['user_id']);
		$this->db->delete('users',$data);
	}
}