<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_import_model extends CI_Model {

    var $table = 'exportdatascnbkn';
    var $column = array('exportdatascnbkn.nik','exportdatascnbkn.nama','exportdatascnbkn.email','exportdatascnbkn.alamat_ktp','exportdatascnbkn.no_telp');
    var $select = 'exportdatascnbkn.*,mst_religion.religion_name as nama_agama, mst_marital_status.ms_name as nama_status_kawin, tr_data_pelamar.*';

    var $order = array('exportdatascnbkn.id_exportdatascnbkn' => 'DESC');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _main_query(){
        $this->db->select($this->select);
        $this->db->from($this->table);
        $this->db->join('tr_data_pelamar',$this->table.'.nik=tr_data_pelamar.dp_nik','left');
        $this->db->join('mst_university','tr_data_pelamar.pend_universitas=mst_university.univ_id','left');
        $this->db->join('mst_majors','tr_data_pelamar.pend_jurusan=mst_majors.majors_id','left');
        $this->db->join('mst_religion',$this->table.'.agama=mst_religion.religion_id','left');
        $this->db->join('mst_marital_status',$this->table.'.status_kawin=mst_marital_status.ms_id','left');

    }

    private function _get_datatables_query()
    {
        
        $this->_main_query();

        $i = 0;
    
        foreach ($this->column as $item) 
        {
            if($_POST['search']['value'])
                ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
            $column[$i] = $item;
            $i++;
        }
        
        if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->_main_query();
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->_main_query();
        if(is_array($id)){
            $this->db->where_in(''.$this->table.'.id_exportdatascnbkn',$id);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->where(''.$this->table.'.id_exportdatascnbkn',$id);
            $query = $this->db->get();
            return $query->row();
        }
        
    }

    public function get_by_nik($id)
    {
        $this->db->select("mst_university.*, CONCAT(mst_university.univ_id,' : ',mst_university.univ_name) AS concat_univ");
        $this->db->select("CONCAT(mst_majors.majors_id,' : ',mst_majors.majors_name) AS concat_majors");
        $this->_main_query();
        $this->db->where(''.$this->table.'.nik',$id);
        $query = $this->db->get();
        return $query->row();
        
    }

    public function get_by_dp_id($id)
    {
        $this->_main_query();
        $this->db->where('tr_data_pelamar.dp_id',$id);
        $query = $this->db->get();
        return $query->row();
        
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function save_data_cpns($data)
    {
        $this->db->insert('tr_data_pelamar', $data);
        return $this->db->insert_id();
    }

    public function update_data_cpns($where, $data)
    {
        $this->db->update('tr_data_pelamar', $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        return $this->db->delete($this->table, array('id' => $id));
    }

    public function get_no_peserta($dp_id)
    {
        $data = $this->get_by_dp_id($dp_id);
        /*kode instansi 4 digit*/
        $kode_instansi = '3021';

        /*jenis formasi 1 digit*/
        $jf = $this->db->get_where('mst_formasi_jenis', array('formasi_jenis_id' => $data->formasi_jenis))->row();
        $jenis_formasi = $jf->formasi_jenis_kode;

        /*jenis kelamin 1 digit*/
        $jk = $data->dp_jk;

        /*jenjang pendidikan 1 digit*/
        $jp = '3';

        /*formasi_jabatan 1 digit*/
        $formasi_jabatan = $data->formasi_jabatan;

        /*jurusan pendidikan 2 digit*/
        $jur_pend = $this->getDigit(2, $data->pend_jurusan);

        /*no_urut 4 digit*/
        $qry = $this->db->query("SELECT COUNT(dp_id)AS total FROM tr_data_pelamar WHERE verifikasi_status in (3,4)")->row();
        $urut = $qry->total + 1;
        $no_urut = $this->getDigit(4, $urut);

        $no_peserta = $kode_instansi.'-'.$jenis_formasi.$jk.$jp.'-'.$formasi_jabatan.$jur_pend.'-'.$no_urut;


        return $no_peserta;
    }

    public function getDigit($max_digit, $number){
        /*count length number*/
        $dgt = '';
        $length = strlen((string)$number);
        for ($i=1; $i < $max_digit; $i++) { 
            $dgt .= 0;
        }
        return $dgt.$number;
    }

    public function get_kfj_id($fj_id){
        $data = $this->db->get_where('mst_formasi_jabatan', array('fj_id' => $fj_id))->row();
        return $data->kfj_id;
    }

    public function insert_multiple($data){
        $this->db->insert_batch('exportdatascnbkn', $data);
    }


}
