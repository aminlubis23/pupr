<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function check_account($usr, $pass, $table) {
        /*get hash password*/
        $data = $this->get_hash_password($usr, $table); //print_r($this->db->last_query());die;
        /*validate account*/
        if($data){
            if($this->bcrypt->check_password($pass,$data->password)){
                return $data;
            }else{
                return false;
            }
        }else{
            return false;
        }
        
    }

    public function get_hash_password($usr, $table){
        $query = $this->db->select(''.$table.'.*')
                          ->get_where($table, array('username' => $usr, $table.'.is_active' => 'Y'))->row();
        if($query){
            return $query;
        }else{
            return false;
        }
    }

    /*public function get_sess_menus($role_id){

        $getData = [];
        $find_menus = $this->find_menus($role_id, 0);  
        foreach($find_menus as $row){
            $find_sub_menus = $this->find_menus($role_id, $row->menu_id);
            $row->sub_menus = $find_sub_menus;
            $getData[] = $row;
        }
        return $getData;
    }

    function find_menus($role_id, $parent) {

        $this->db->from('role_has_menu');
        $this->db->join('menu','menu.menu_id=role_has_menu.menu_id');
        $this->db->where(array('role_id' => $role_id, 'is_active' => 'Y', 'parent' => $parent));
        $this->db->order_by('counter', 'ASC');

        return $this->db->get()->result();
    }

    public function generate_token($user_id){

        $static_str='Login';
        $currenttimeseconds = date("mdY_His");
        $token_id=$static_str.$user_id.$currenttimeseconds;
        $data = array(
                 'token' => md5($token_id),
                 'type' => $static_str,
                 'created_date' => date('Y-m-d H:i:s'),
                 'user_id' => $user_id,
                 );
        $this->db->insert('token', $data);
        return md5($token_id);
    }*/

    public function clear_token($user_id){
        return $this->db->delete('token', array('user_id' => $user_id));
    }


}
