<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bantuan_model extends CI_Model{

	var $table = 'global_parameter';
    var $column = array('global_parameter.mail_layanan_online');
    var $select = 'global_parameter.*';

    var $order = array('global_parameter.id' => 'DESC');

  public function __construct()
  {
    parent::__construct();
		$this->load->database();
  }

  public function get_data(){
    $qry = $this->db->get_where('global_parameter', array('id' => 1));
    return $qry->row_array();
  }

  public function get_by_id($id)
    {
        $this->_main_query();
        if(is_array($id)){
            $this->db->where_in(''.$this->table.'.id',$id);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->where(''.$this->table.'.id',$id);
            $query = $this->db->get();
            return $query->row();
        }
        
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

}
