<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 30/07/2018
 * Time: 14:33
 */

class Slideradmin_model extends CI_Model
{
    // Load database
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Slideradmin_model');
        $this->load->model('user_model');
    }

    //Listing
    public function listing() {
        $this->db->select('*');
        $this->db->from('slider');
        $this->db->order_by('id_slider','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // detail peruser
    public function detail($id_user){
        $query = $this->db->get_where('slider',array('id_slider'  => $id_user));
        return $query->row();
    }

    // Tambah
    public function tambah ($data) {
        $this->db->insert('slider',$data);
    }

    // Edit
    public function edit ($data,$id_user) {
        $this->db->where('id_slider',$id_user);
        $this->db->update('slider',$data);
    }

    // Delete
    public function delete ($data){
        $this->db->where('id_slider',$data['id_slider']);
        $this->db->delete('slider',$data);
    }
}