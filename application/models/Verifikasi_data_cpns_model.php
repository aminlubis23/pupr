<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_data_cpns_model extends CI_Model {

    var $table = 'tr_data_pelamar';
    var $column = array('tr_data_pelamar.dp_nik','tr_data_pelamar.dp_nama_lengkap','tr_data_pelamar.dp_no_peserta','tr_data_pelamar.dp_alamat_lengkap','tr_data_pelamar.dp_tempat_lahir',);
    var $select = 'tr_data_pelamar.*,mst_religion.religion_name as nama_agama, mst_marital_status.ms_name as nama_status_kawin, exportdatascnbkn.*, mst_gender.gender_name as jk, mst_university.univ_name, mst_university.univ_akreditasi, mst_university.univ_status, mst_majors.majors_name, mst_status_verifikasi.sv_name, mst_status_verifikasi.sv_id, CONCAT(mst_kualifikasi_pendidikan.jenjang,"-",majors_a.majors_name) as kp_name, mst_formasi_jenis.formasi_jenis_name, mst_formasi_jabatan.fj_name, mst_unit_penempatan.up_name, mst_jenis_toefl.jt_name, tbl_lokasi_ujian_skd.lokasi_ujian_alamat as alamat_skd, tbl_lokasi_ujian_skd.tanggal_ujian_skd,, tbl_lokasi_ujian_skb.lokasi_ujian_alamat as alamat_skb, tbl_lokasi_ujian_skb.tanggal_ujian_skb, kota_skd.name as kota_skd_name, kota_skb.name as kota_skb_name, tr_nilai_ujian.*';

    var $order = array('tr_data_pelamar.dp_id' => 'DESC');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _main_query(){
        $this->db->select($this->select);
        $this->db->from($this->table);
        $this->db->join('exportdatascnbkn',$this->table.'.dp_nik=exportdatascnbkn.nik','left');
        $this->db->join('mst_religion',$this->table.'.dp_agama=mst_religion.religion_id','left');
        $this->db->join('mst_marital_status',$this->table.'.dp_status_nikah=mst_marital_status.ms_id','left');
        $this->db->join('mst_gender',$this->table.'.dp_jk=mst_gender.gender_id','left');
        $this->db->join('mst_university',$this->table.'.pend_universitas=mst_university.univ_id','left');
        $this->db->join('mst_majors',$this->table.'.pend_jurusan=mst_majors.majors_id','left');
        $this->db->join('mst_majors as majors_a',$this->table.'.pend_jurusan=majors_a.majors_id','left');
        $this->db->join('mst_formasi_jenis',$this->table.'.formasi_jenis=mst_formasi_jenis.formasi_jenis_id','left');
        $this->db->join('mst_formasi_jabatan',$this->table.'.formasi_jabatan=mst_formasi_jabatan.fj_id','left');
        $this->db->join('mst_unit_penempatan',$this->table.'.formasi_penempatan=mst_unit_penempatan.up_id','left');
        $this->db->join('mst_status_verifikasi',$this->table.'.verifikasi_status=mst_status_verifikasi.sv_id','left');
        $this->db->join('mst_jenis_toefl',$this->table.'.bhs_jenis_toefl=mst_jenis_toefl.jt_id','left');
        $this->db->join('mst_kualifikasi_pendidikan',$this->table.'.formasi_jurusan_pendidikan=mst_kualifikasi_pendidikan.kp_id','left');
        $this->db->join('mst_lokasi_ujian as tbl_lokasi_ujian_skd',$this->table.'.loku_skd_id=tbl_lokasi_ujian_skd.lokasi_ujian_id','left');
        $this->db->join('mst_lokasi_ujian as tbl_lokasi_ujian_skb',$this->table.'.loku_skb_id=tbl_lokasi_ujian_skb.lokasi_ujian_id','left');
        $this->db->join('regencies as kota_skd','kota_skd.id=tbl_lokasi_ujian_skd.regency_id','left');
        $this->db->join('regencies as kota_skb','kota_skb.id=tbl_lokasi_ujian_skb.regency_id','left');
        $this->db->join('tr_nilai_ujian','tr_nilai_ujian.nik=tr_data_pelamar.dp_nik','left');

        /*search by formasi*/
        if(isset($_GET['search_by']) AND $_GET['search_by']==1){
            $this->db->where('formasi_jenis', $_GET['formasi_jenis']);
        }

        /*search by lokasi penempatan*/
        if(isset($_GET['search_by']) AND $_GET['search_by']==2){
            $this->db->where('formasi_penempatan', $_GET['up_id']);
        }

        /*search by status_pelamar*/
        if(isset($_GET['search_by']) AND $_GET['search_by']==3){
            $this->db->where('verifikasi_status', $_GET['sv_id']);
        }

        /*search by universitas*/
        if(isset($_GET['search_by']) AND $_GET['search_by']==4){
            if($_GET['pend_universitas']!=''){
                $this->db->where('pend_universitas', $_GET['pend_universitas']);
            }
            
            if(isset($_GET['pend_jurusan']) AND $_GET['pend_jurusan'] !=''){
                $this->db->where('pend_jurusan', $_GET['pend_jurusan']);
            }
        }

        /*search by field*/
        if(isset($_GET['search_by']) AND $_GET['search_by']==5){
            /*search by field agama*/
            if($_GET['select_field']=='dp_agama'){
                $keyword = $_GET['dp_agama'];
            }elseif ($_GET['select_field']=='dp_status_nikah') {
                $keyword = $_GET['dp_status_nikah'];
            }else{
                $keyword = $_GET['keyword'];
            }
            $this->db->where($_GET['select_field'], $keyword);
        }

    }

    private function _get_datatables_query()
    {
        
        $this->_main_query();

        $i = 0;
    
        foreach ($this->column as $item) 
        {
            if($_POST['search']['value'])
                ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
            $column[$i] = $item;
            $i++;
        }
        
        if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($this->db->last_query());die;
        return $query->result();
    }

    function get_data_for_export()
    {
        $this->_main_query();
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->_main_query();
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->_main_query();
        if(is_array($id)){
            $this->db->where_in(''.$this->table.'.dp_id',$id);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->where(''.$this->table.'.dp_id',$id);
            $query = $this->db->get();
            return $query->row();
        }
        
    }

    public function get_by_nik($id)
    {
        $this->_main_query();
        $this->db->where(''.$this->table.'.dp_nik',$id);
        $query = $this->db->get();
        return $query->row();
        
    }

    public function get_item_dok($flag)
    {
        $this->db->where('flag', $flag);
        $this->db->order_by('gp_id', 'ASC');
        $query = $this->db->get('mst_global_params');
        return $query->result_array();
        
    }

    public function get_file_uploaded($nik)
    {
        $this->db->where('nik', $nik);
        $query = $this->db->get('tr_file_pelamar');
        $result = $query->result_array();
        $files = [];
        foreach ($result as $key => $value) {
            $files[$value['file_category']] = $value['file_name'];
        }
        return $files;
    }


    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function save_data_cpns($data)
    {
        $this->db->insert('tr_data_pelamar', $data);
        return $this->db->insert_id();
    }

    public function update_data_cpns($where, $data)
    {
        $this->db->update('tr_data_pelamar', $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        return $this->db->delete($this->table, array('dp_id' => $id));
    }

    public function get_file_pelamar_by_params($where)
    {
        return $this->db->get_where('tr_file_pelamar', $where)->row();
    }

     public function view_hasil_akhir($array)
    {
        if($array->verifikasi_hasil_akhir=='L'){
            $html = '';
            $html .= '<div class="alert alert-success">';
            $html .= '<b><i class="fa fa-thumbs-up"></i><br>Lulus </b>';
            $html .= '<br> <small style="font-size:10px !important"> ~ '.$array->verifikasi_petugas.' ~ <br> '.$this->tanggal->formatDateForm($array->verifikasi_tanggal).' </small>';
            $html .= '</div>';
        }else{
             $html = '';
            $html .= '<div class="alert alert-danger">';
            $html .= '<b><i class="fa fa-thumbs-down"></i><br>Tidak Lulus </b>';
            $html .= '<br> <small style="font-size:10px !important"> ~ '.$array->verifikasi_petugas.' ~ <br> '.$this->tanggal->formatDateForm($array->verifikasi_tanggal).' </small>';
            $html .= '</div>';
        }
        return $html;
    }

    public function get_sesi_ujian($dp_id)
    {
        /*get detail data by dp_id*/
        $data = $this->get_by_id($dp_id);

        /*==========SKD========*/
        $loku_skd_id = $data->loku_skd_id;
        $data_lokasi_skd = $this->db->get_where('mst_lokasi_ujian', array('lokasi_ujian_id' => $data->loku_skd_id))->row();
        $total_skd = $this->db->query("SELECT MAX(loku_urut_skd)AS max_urut FROM tr_data_pelamar WHERE verifikasi_status in (3,4) AND loku_skd_id=".$data->loku_skd_id."")->row();
        $default_urut_skd = ($total_skd->max_urut==NULL)?1:$total_skd->max_urut;
        $next_urut_skd = $default_urut_skd + 1;
        /*get_sesi skd*/
        $sesi_skd = $this->get_sesi($data_lokasi_skd->max_per_sesi_skd, $next_urut_skd);
        $data_skd = array('urut_skd' => $next_urut_skd, 'sesi_skd' => $sesi_skd);

        /*=========SKB=========*/
        $loku_skb_id = $data->loku_skb_id;
        $data_lokasi_skb = $this->db->get_where('mst_lokasi_ujian', array('lokasi_ujian_id' => $data->loku_skb_id))->row();
        $total_skb = $this->db->query("SELECT MAX(loku_urut_skb)AS max_urut FROM tr_data_pelamar WHERE verifikasi_status in (3,4) AND loku_skb_id=".$data->loku_skb_id."")->row();
        $default_urut_skb = ($total_skb->max_urut==NULL)?1:$total_skb->max_urut;
        $next_urut_skb = $default_urut_skb + 1;
        /*get_sesi skb*/
        $sesi_skb = $this->get_sesi($data_lokasi_skb->max_per_sesi_skb, $next_urut_skb);
        $data_skb = array('urut_skb' => $next_urut_skb, 'sesi_skb' => $sesi_skb);

        $getData = array('skd' => $data_skd, 'skb' => $data_skb);

        return $getData;


    }

    public function get_sesi($max, $current){
        /*modulus*/
        $sesi = ceil($current / $max);

        return $sesi;

    }

    public function get_nilai($nik){

        $data = $this->db->get_where('tr_nilai_ujian', array('nik' => $nik))->row();
        return $data;
    }


}
