<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik_data_model extends CI_Model {


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_data(){
        /*get data kategori formasi jabatan*/
        $query = "SELECT d.`kfj_id`, d.`kfj_name`, b.`kfj_id`, b.`fj_id`, b.`fj_name`, CONCAT(c.jenjang,'  ',e.majors_name) as kp_name,a.`kp_id`, a.`jumlah_formasi`, f.up_name as penempatan
            FROM tr_formasi_jabatan_has_kualifikasi_pendidikan AS a
            LEFT JOIN mst_formasi_jabatan b ON b.`fj_id`= a.`fj_id`
            LEFT JOIN mst_kualifikasi_pendidikan c ON c.`kp_id`=a.`kp_id`
            LEFT JOIN mst_majors e ON e.`majors_id`=c.`majors_id`
            LEFT JOIN mst_unit_penempatan f ON f.`up_id`=a.`up_id`
            LEFT JOIN mst_kategori_formasi_jabatan d ON d.`kfj_id`=b.`kfj_id`";

        $result = $this->db->query($query)->result();

        /*get count result*/

        foreach ($result as $key => $value) {
            $group[$value->kfj_name][] = array('kfj_id'=>$value->kfj_id, 'fj_id' => $value->fj_id, 'kp_id'=>$value->kp_id,'fj_name' => $value->fj_name, 'kp_name' => $value->kp_name, 'kp_jumlah_formasi' => $value->jumlah_formasi, 'penempatan' => $value->penempatan, 'statistik_data' => $this->get_statistik_data($value->kfj_id, $value->fj_id, $value->kp_id) );
        }

        /*grouping by formasi jabatan*/
        foreach ($group as $k => $v) {
            foreach ($group[$k] as $ka => $va) {
                $group_jf[$va['fj_name']][] = $va;
            }
            $getData[$k] = $group_jf;
        }
        //echo '<pre>';print_r($getData);die;
        return $getData;

    }

    public function get_statistik_data($kfj_id, $fj_id, $kp_id){
        $qry = "SELECT formasi_jenis_id, COUNT(id) AS total FROM log_count_data_formasi WHERE kfj_id=".$kfj_id." AND fj_id=".$fj_id." AND kp_id=".$kp_id." GROUP BY formasi_jenis_id";
        $data = $this->db->query($qry)->result();
        $getData = array();
        foreach($data as $row){
            $getData[$kfj_id][$fj_id][$kp_id][$row->formasi_jenis_id] = $row->total;
        }

        return $getData;
    }

    public function count_global_data(){
        $all = $this->db->get('exportdatascnbkn')->num_rows();
        $pelamar = $this->db->get('tr_data_pelamar')->num_rows();
        $memenuhi_syarat = $this->db->get_where('tr_data_pelamar', array('verifikasi_status' => 4) )->num_rows();
        $lulus = $this->db->get_where('tr_data_pelamar', array('verifikasi_hasil_akhir' => 'L') )->num_rows();

        $getData = array('total_all' => $all, 'ttl_pelamar' => $pelamar, 'memenuhi_syarat' => $memenuhi_syarat, 'lulus' => $lulus );

        return $getData;
    }

    public function grafik(){
        /*chart by jk*/
        $by_jk = $this->db->query("SELECT a.gender_name, (SELECT COUNT(b.dp_id) AS total FROM tr_data_pelamar b WHERE b.dp_jk=a.gender_id) AS total FROM mst_gender a")->result();

        $by_formasi = $this->db->query("SELECT a.formasi_jenis_name, (SELECT COUNT(b.dp_id) AS total FROM tr_data_pelamar b WHERE b.formasi_jenis=a.formasi_jenis_id) AS total FROM mst_formasi_jenis a")->result();

        $by_status_marital = $this->db->query("SELECT a.ms_name, (SELECT COUNT(b.dp_id) AS total FROM tr_data_pelamar b WHERE b.dp_status_nikah=a.ms_id) AS total FROM mst_marital_status a")->result();

        $by_religion = $this->db->query("SELECT a.religion_name, (SELECT COUNT(b.dp_id) AS total FROM tr_data_pelamar b WHERE b.dp_agama=a.religion_id) AS total FROM mst_religion a")->result();

        $by_formasi_pendidikan = $this->db->query("SELECT CONCAT(a.jenjang,'-',c.majors_name)as kp_name, (SELECT COUNT(b.dp_id) AS total FROM tr_data_pelamar b WHERE b.formasi_jurusan_pendidikan=a.kp_id) AS total FROM mst_kualifikasi_pendidikan a LEFT JOIN mst_majors c on c.majors_id=a.majors_id")->result();

        $by_status_verifikasi = $this->db->query("SELECT a.sv_name, (SELECT COUNT(b.dp_id) AS total FROM tr_data_pelamar b WHERE b.verifikasi_status=a.sv_id) AS total FROM mst_status_verifikasi a")->result();

        /*nilai toefl*/
        $lulus_toefl = $this->db->query("SELECT COUNT(a.dp_id) AS total FROM tr_data_pelamar a WHERE bhs_nilai_toefl >=(SELECT min_toefl FROM global_parameter WHERE id=1)")->row();
        $tidak_lulus_toefl = $this->db->query("SELECT COUNT(a.dp_id) AS total FROM tr_data_pelamar a WHERE bhs_nilai_toefl < (SELECT min_toefl FROM global_parameter WHERE id=1)")->row();

         /*nilai toefl*/
        $lulus_ipk = $this->db->query("SELECT COUNT(a.dp_id) AS total FROM tr_data_pelamar a WHERE pend_ipk >=(SELECT min_ipk FROM global_parameter WHERE id=1)")->row();
        $tidak_lulus_ipk = $this->db->query("SELECT COUNT(a.dp_id) AS total FROM tr_data_pelamar a WHERE pend_ipk < (SELECT min_ipk FROM global_parameter WHERE id=1)")->row();

        $data = array(
            'graph_by_jk' => $by_jk,
            'graph_by_formasi' => $by_formasi,
            'graph_by_status_marital' => $by_status_marital,
            'graph_by_religion' => $by_religion,
            'graph_by_formasi_pendidikan' => $by_formasi_pendidikan,
            'graph_by_status_verifikasi' => $by_status_verifikasi,
            'graph_by_toefl' => array('L' => $lulus_toefl->total, 'TL' => $tidak_lulus_toefl->total),
            'graph_by_ipk' => array('L' => $lulus_ipk->total, 'TL' => $tidak_lulus_ipk->total ),
            );

        return $data;
    }




}
