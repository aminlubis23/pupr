<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 16/08/2018
 * Time: 19:32
 */

class Master_data_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Master_data_model');
        $this->load->model('user_model');
    }

    //  pertama data modeling master jenis kelamin
    //Listing
    public function listingjengkel() {
        $this->db->select('*');
        $this->db->from('dtl_jengkel');
        $this->db->order_by('id_jengkel','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // detail peruser
    public function detailjengkel($id_user){
        $query = $this->db->get_where('dtl_jengkel',array('id_jengkel'  => $id_user));
        return $query->row();
    }

    // Tambah
    public function tambahjengkel($data) {
        $this->db->insert('dtl_jengkel',$data);
    }

    // Delete
    public function deletejengkel($data){
        $this->db->where('id_jengkel',$data['id_jengkel']);
        $this->db->delete('dtl_jengkel',$data);
    }

    // kedua data modeling master status
    //Listing
    public function listingstatus() {
        $this->db->select('*');
        $this->db->from('slider');
        $this->db->order_by('id_slider','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // detail peruser
    public function detailstatus($id_user){
        $query = $this->db->get_where('slider',array('id_slider'  => $id_user));
        return $query->row();
    }

    // Tambah
    public function tambahstatus ($data) {
        $this->db->insert('slider',$data);
    }

    // Edit
    public function editstatus ($data,$id_user) {
        $this->db->where('id_slider',$id_user);
        $this->db->update('slider',$data);
    }

    // Delete
    public function deletestatus ($data){
        $this->db->where('id_slider',$data['id_slider']);
        $this->db->delete('slider',$data);
    }

    // lanjutnya data modeling master agama
    //Listing
    public function listingagama() {
        $this->db->select('*');
        $this->db->from('slider');
        $this->db->order_by('id_slider','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // detail peruser
    public function detailagama($id_user){
        $query = $this->db->get_where('slider',array('id_slider'  => $id_user));
        return $query->row();
    }

    // Tambah
    public function tambahagama ($data) {
        $this->db->insert('slider',$data);
    }

    // Edit
    public function editagama ($data,$id_user) {
        $this->db->where('id_slider',$id_user);
        $this->db->update('slider',$data);
    }

    // Delete
    public function deleteagama ($data){
        $this->db->where('id_slider',$data['id_slider']);
        $this->db->delete('slider',$data);
    }
}