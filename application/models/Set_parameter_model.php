<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_parameter_model extends CI_Model {

    var $table = 'global_parameter';
    var $column = array('global_parameter.min_toefl');
    var $select = 'global_parameter.*';

    var $order = array('global_parameter.id' => 'DESC');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _main_query(){
        $this->db->select($this->select);
        $this->db->from($this->table);

    }

    public function get_by_id($id)
    {
        $this->_main_query();
        if(is_array($id)){
            $this->db->where_in(''.$this->table.'.id',$id);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->where(''.$this->table.'.id',$id);
            $query = $this->db->get();
            return $query->row();
        }
        
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        return $this->db->delete($this->table, array('id' => $id));
    }


}
