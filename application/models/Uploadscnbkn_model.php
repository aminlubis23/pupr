<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 13/08/2018
 * Time: 9:46
 */

class Uploadscnbkn_model extends CI_Model
{
    // Load database
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('uploadscnbkn_model');
        $this->load->model('user_model');
    }

    //Listing
    public function listing() {
        $this->db->select('*');
        $this->db->from('exportdatascnbkn');
        $this->db->order_by('id_exportdatascnbkn','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // detail per data user
    public function detail($id_user){
        $query = $this->db->get_where('exportdatascnbkn',array('id_exportdatascnbkn'  => $id_user));
        return $query->row();
    }

    public function uploadscnbkn(){
        return $this->db->get('exportdatascnbkn')->result(); // Tampilkan semua data yang ada di tabel Uploadernya
    }
    // Fungsi untuk melakukan proses upload file
    public function upload_file($filename){
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size']	= '10000';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    public function insert_multiple($data){
        $this->db->insert_batch('exportdatascnbkn', $data);
    }
}