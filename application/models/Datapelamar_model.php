<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: NB-HaritzIPM
 * Date: 27/07/2018
 * Time: 10:57
 */

class Datapelamar_model extends CI_Model{
    // Load database
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Datapelamar_model');
        $this->load->model('user_model');
    }

    //Listing
    public function listing() {
        $this->db->select('*');
        //$this->db->from('data_pelamar_dummy');
        $this->db->from('exportdatascnbkn');
        $this->db->order_by('id_exportdatascnbkn','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    //Listing data join
    public function listingjoin(){
        $this->db->select('exportdatascnbkn.id_exportdatascnbkn AS id_exportdatascnbkn, exportdatascnbkn.id AS id, exportdatascnbkn.nik AS nik, exportdatascnbkn.no_kk AS no_kk, exportdatascnbkn.nik_kk AS nik_kk, exportdatascnbkn.password AS PASSWORD, exportdatascnbkn.nama AS nama, exportdatascnbkn.nama_ijazah AS nama_ijazah, exportdatascnbkn.tempat_lahir AS tempat_lahir, exportdatascnbkn.tempat_lahir_ijazah AS tempat_lahir_ijazah, exportdatascnbkn.tanggal_lahir AS tanggal_lahir, exportdatascnbkn.tanggal_lahir_ijazah AS tanggal_lahir_ijazah, dtl_jengkel.nama AS nama_kelamin, exportdatascnbkn.email AS email, exportdatascnbkn.pertanyaan_pengaman_1 AS pertanyaan_pengaman_1, exportdatascnbkn.jawaban_1 AS jawaban_1, exportdatascnbkn.pertanyaan_pengaman_2 AS pertanyaan_pengaman_2, exportdatascnbkn.jawaban_2 AS jawaban_2, dtl_agama.nama AS agama, exportdatascnbkn.alamat_ktp AS alamat_ktp, exportdatascnbkn.alamat_domisili AS alamat_domisili, exportdatascnbkn.tinggi_badan AS tinggi_badan, dtl_status.nama AS status_kawin, exportdatascnbkn.no_telp AS no_telp, exportdatascnbkn.no_hp AS no_hp, exportdatascnbkn.nama_ibu AS nama_ibu, exportdatascnbkn.nama_ayah AS nama_ayah, exportdatascnbkn.kodepos AS kodepos, exportdatascnbkn.lokasi_kabkota AS lokasi_kabkota, exportdatascnbkn.tgl_daftar AS tgl_daftar, exportdatascnbkn.recorded_time AS recorded_time, exportdatascnbkn.create_by AS create_by, exportdatascnbkn.remarks AS remarks');
        $this->db->from('exportdatascnbkn');
        $this->db->join('dtl_jengkel', 'exportdatascnbkn.jenis_kelamin = dtl_jengkel.id_jengkel');
        $this->db->join('dtl_status', 'exportdatascnbkn.status_kawin = dtl_status.id_status');
        $this->db->join('dtl_agama', 'exportdatascnbkn.agama = dtl_agama.id_agama');
        //$this->db->order_by('ORDER BY exportdatascnbkn.id_exportdatascnbkn','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // detail peruser
    public function detail($id_user){
        //$query = $this->db->get_where('data_pelamar_dummy',array('id_daper'  => $id_user));
        $query = $this->db->get_where('exportdatascnbkn',array('id_exportdatascnbkn'  => $id_user));
        return $query->row();
    }
}